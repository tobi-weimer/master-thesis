import os

from absl import app
from absl import flags

FLAGS = flags.FLAGS
flags.DEFINE_string(
    'dir', None,
    'The directory to flatten.')


def main(argv):
    dir_to_flatten = FLAGS.dir
    for subdir, dirs, files in os.walk(dir_to_flatten):
        for file in files:
            src_file = os.path.join(subdir, file)
            dest_file = os.path.join(dir_to_flatten, file)
            os.rename(src_file, dest_file)
        if subdir != dir_to_flatten:
            os.rmdir(subdir)


if __name__ == '__main__':
    flags.mark_flag_as_required('dir')
    app.run(main)
