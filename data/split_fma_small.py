from fma_utils import load as load_fma_metadata
from pathlib import Path

import os

from absl import app
from absl import flags

FLAGS = flags.FLAGS
flags.DEFINE_string(
    'audio_dir', None,
    'The directory where the FMA SMALL tracks can be found.')
flags.DEFINE_string(
    'metadata_dir', None,
    'The directory where the FMA metadata files can be found.')
flags.DEFINE_bool(
    'dry_run', False,
    'If true, only logs what whould be done')
flags.DEFINE_string(
    'src_file_ending', '.tfrecord-00000-of-00001',
    'The file ending of the source file')
flags.DEFINE_string(
    'dest_file_ending', '.tfrecord',
    'The file ending of the destination file')



def main(argv):
    tracks = load_fma_metadata(os.path.join(FLAGS.metadata_dir, 'tracks.csv'))
    tracks = tracks[tracks['set', 'subset'] <= 'small']

    audio_dir = FLAGS.audio_dir
    for split in ['training', 'validation', 'test']:
        missing_tracks = []
        missing_tracks_count = 0
        split_dir = os.path.join(audio_dir, split)
        Path(split_dir).mkdir(parents=False, exist_ok=True)
        split_df = tracks[tracks['set', 'split'] == split]
        print(f'{split}: {len(split_df)}')
        for track_id in split_df.index.values:
            track_id = str(track_id).zfill(6)
            src_file = os.path.join(audio_dir, f'{track_id}{FLAGS.src_file_ending}')
            dest_file = os.path.join(split_dir, f'{track_id}{FLAGS.dest_file_ending}')
            if not os.path.isfile(src_file):
                missing_tracks.append(track_id)
                missing_tracks_count += 1
                continue
            if FLAGS.dry_run:
                print(f'mv {src_file} {dest_file}')
            else:
                os.rename(src_file, dest_file)
        print(f'Finished moving files in {split} split. {len(missing_tracks)} tracks could not be found!"')
        print(f'Missing tracks in {split} split: {missing_tracks}')


if __name__ == '__main__':
    flags.mark_flags_as_required(['audio_dir', 'metadata_dir'])
    app.run(main)
