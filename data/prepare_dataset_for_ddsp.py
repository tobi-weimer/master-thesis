# Call as:
#   nohup python data/prepare_dataset_for_ddsp.py \
#   --wav_input_root_dir=/scratch/wtp-music-embedding/datasets/fma_small_wav/ \
#   --tfrecord_output_root_dir=/scratch/wtp-music-embedding/datasets/fma_small_ddsp \
#   --sliding_window_hop_secs=1 \
#   > log/prepare_dataset_fma_small.log &

import os
import subprocess
from pathlib import Path

from absl import app
from absl import flags

# Converts all *.wav files in a directory structure into tfrecord files that are suitable to be ingested by
# the ddsp model. The directory structure present in the input directory is preserved in the output directory.

FLAGS = flags.FLAGS
flags.DEFINE_string(
    'wav_input_root_dir', None,
    'The directory containing the wav files to process.')
flags.DEFINE_string(
    'tfrecord_output_root_dir', None,
    'The directory to which the resulting TFRecord files should be written.')
flags.DEFINE_float(
    'sliding_window_hop_secs', None,
    'The hop size in seconds to use when splitting audio into constant-length examples.')


# noinspection PyUnusedLocal
def main(argv):
    Path(FLAGS.tfrecord_output_root_dir).mkdir(parents=True, exist_ok=True)

    wav_root_input_dir = FLAGS.wav_input_root_dir
    for root, _, wav_files in os.walk(wav_root_input_dir):
        for wav_file in wav_files:
            wav_file_abs = os.path.join(root, wav_file)
            rel_path = os.path.relpath(wav_file_abs, os.path.commonpath([wav_file_abs, wav_root_input_dir]))
            src_file = os.path.join(root, wav_file)
            dest_file = os.path.join(FLAGS.tfrecord_output_root_dir, rel_path.replace(".wav", '.tfrecord'))
            os.makedirs(os.path.dirname(dest_file), exist_ok=True)

            if os.path.isfile(dest_file + "-00000-of-00001"):  # All TFRecord files get this postfix
                print(f"File {wav_file} has already been converted. Skipping!")
                continue
            else:
                command = [
                    'ddsp_prepare_tfrecord',
                    f"--input_audio_filepatterns={src_file}",
                    f"--output_tfrecord_path={dest_file}",
                    '--num_shards=1',  # Each audio file is written to a single TFRecord file
                    f'--hop_secs={FLAGS.sliding_window_hop_secs}',
                    '--alsologtostderr'
                ]
                print(f"Executing command: {' '.join(command)}")
                subprocess.call(command)
                pass
    print(f"Finished processing all audio files")


if __name__ == '__main__':
    flags.mark_flags_as_required([
        'wav_input_root_dir',
        'tfrecord_output_root_dir',
        'sliding_window_hop_secs'])
    app.run(main)
