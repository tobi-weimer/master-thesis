from absl import app
from absl import flags
import os

FLAGS = flags.FLAGS
flags.DEFINE_string(
    'dataset_dir', None,
    'The dataset root directory.')


def get_split_dir(root_dir, split_name):
    split_dir = os.path.join(root_dir, split_name)
    os.makedirs(split_dir, exist_ok=True)
    return split_dir


def main(argv):
    root_dir = FLAGS.dataset_dir
    training_dir = get_split_dir(root_dir, 'training')
    validation_dir = get_split_dir(root_dir, 'validation')
    test_dir = get_split_dir(root_dir, 'test')

    for root, _, files in os.walk(os.path.join(root_dir, 'genres_original')):
        for f in files:
            idx = int(f.split('.')[1])
            src_file = os.path.join(root, f)
            split_dir = training_dir if idx < 80 else validation_dir if idx < 90 else test_dir
            dest_file = os.path.join(split_dir, f)
            os.rename(src_file, dest_file)


if __name__ == '__main__':
    flags.mark_flags_as_required(['dataset_dir'])
    app.run(main)
