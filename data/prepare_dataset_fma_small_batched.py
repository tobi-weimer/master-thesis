import subprocess
from os import scandir
from os.path import normpath, basename, join, isdir


def extsis_and_is_not_empty(directroy):
    return isdir(directroy) and len([f for f in scandir(directroy)]) > 0


source_dir = '/scratch/wtp-music-embedding/datasets/fma_small_wav'
target_dir = '/scratch/wtp-music-embedding/datasets/fma_small_ddsp'

wav_dirs = sorted([f.path for f in scandir(source_dir) if f.is_dir()])
for wav_dir in wav_dirs:
    batch_name = basename(normpath(wav_dir))
    tfrecord_dir = join(target_dir, batch_name)

    print(f"Processing {wav_dir}")
    if extsis_and_is_not_empty(tfrecord_dir):
        print(f"Found non empty directory {tfrecord_dir}. Assuming conversion already happened. Skipping!\n")
        continue

    input_audio_filepattern = join(wav_dir, '*.wav')
    output_tfrecord_path = join(tfrecord_dir, f"fma_small_{batch_name}.tfrecord")
    command = [
        'ddsp_prepare_tfrecord',
        f"--input_audio_filepatterns={input_audio_filepattern}",
        f"--output_tfrecord_path={output_tfrecord_path}",
        '--num_shards=4',  # yields files of approx. 100mb
        '--alsologtostderr'
    ]

    print(f"Executing command: {' '.join(command)}")
    subprocess.call(command)
    print(f"Finished processing {wav_dir}\n")
