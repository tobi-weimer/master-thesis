# Master-Thesis von Tobias Weimer

# Working with this project

## Creating a python environment that manages the project's dependencies
This section discusses how to setup a virtual environment both locally and on the cluster.

This project uses venv to manage its environment. On the cluster venv is already installed.  
Locally, see [these instructions](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/) in
order to install venv.

Create a new virtual environment using the following command:  
```
# On Unix
python3 -m venv env

# On Windows
python -m venv env
```
Activate the new environment:
```
# On Unix
source env/bin/activate

# On Windows
.\env\Scripts\activate
```
Update pip and setuptools:
```
python -m pip install --upgrade pip setuptools
```

Only the first time - or if the dependencies have been updated - run the following:
```
python -m pip install -r requirements.txt 
```
Remember to update the `requirements.txt` if you have added/updated the project's dependencies.

To make sure you can always reproduce your results, it makes sense to freeze your requirements in a file:
```
python -m pip freeze > requirements-fixed.txt 
```
Now your virtual environment has been activated and all the project's dependencies have been installed.  
You should thus be able to run the code in this repository. 

