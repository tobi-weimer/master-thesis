import os
import uuid
from pathlib import Path, PurePath

import ddsp.training
import gin
import tensorflow as tf
import yaml
from absl import app
from absl import flags

from audio_utils import save_audio, save_specplot
from vq_vae import QuantizingAutoencoder

FLAGS = flags.FLAGS
flags.DEFINE_string("config", None, "Path to a report config yaml file.")


class Report:
    def __init__(self, dir, name):
        Path(dir).mkdir(parents=True, exist_ok=True)
        self.file = open(Path(dir).joinpath('report.html'), 'w+')
        self.data_dir = Path(dir).joinpath("data")
        self.data_dir.mkdir(parents=False, exist_ok=True)
        self.name = name
        self.num_samples = 0

    def __enter__(self):
        self.file.write('<html><body>')
        self.file.write(f'<h1 style="width:100%; text-align: center">{self.name}</h1>')
        self.file.write('<table style="margin: 0 auto">')
        self.file.write('<tr>')
        self.file.write('<th style="font-size: x-large">#</th>')
        self.file.write('<th style="font-size: x-large">Original Audio</th>')
        self.file.write('<th style="font-size: x-large">Resynthesis</th>')
        self.file.write('</tr>')
        return self

    def add_row(self, original_audio, resynthesis):
        if len(original_audio.shape) > 1 or len(resynthesis.shape) > 1:
            raise ValueError("Parameters original_audio and resynthesis must be 1D arrays. Unbatch them if necessary!")

        self.num_samples += 1
        sample_id = uuid.uuid4()

        original_audio_wav_file = self.data_dir.joinpath(f"{sample_id}-original_audio.wav")
        save_audio(original_audio_wav_file, original_audio)

        resynthesis_wav_file = self.data_dir.joinpath(f"{sample_id}-resynthesis.wav")
        save_audio(resynthesis_wav_file, resynthesis)

        original_audio_specplot_file = self.data_dir.joinpath(f"{sample_id}-original_audio.png")
        save_specplot(original_audio_specplot_file, original_audio)

        resynthesis_specplot_file = self.data_dir.joinpath(f"{sample_id}-resynthesis.png")
        save_specplot(resynthesis_specplot_file, resynthesis)

        self.file.write(f'<tr><td rowspan="2" style="font-size: x-large">{self.num_samples}</td><td>')
        self.write_specplot(original_audio_specplot_file)
        self.file.write('</td><td>')
        self.write_specplot(resynthesis_specplot_file)
        self.file.write('</td></tr>')
        self.file.write('<tr><td>')
        self.write_audio_control(original_audio_wav_file)
        self.file.write('</td><td>')
        self.write_audio_control(resynthesis_wav_file)
        self.file.write('</td></tr>')

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.write('</table></body></html>')
        self.file.close()

    def write_audio_control(self, audio_file):
        self.file.write('<div align="center"><audio controls>')
        self.file.write(f'<source src="data/{PurePath(audio_file).name}" type="audio/wav">')
        self.file.write('</audio></div>')

    def write_specplot(self, image_file):
        src = f'data/{PurePath(image_file).name}'
        self.file.write(f'<img src="{src}" alt="{src}" style="max-width:400px">')


# noinspection PyUnusedLocal
def main(argv):
    with open(FLAGS.config, 'r') as config_file:
        config = yaml.safe_load(config_file)
    trained_model_dir = config['trained_model_dir']

    # Parse the gin config.
    gin_file = os.path.join(trained_model_dir, 'operative_config-0.gin')
    gin.parse_config_file(gin_file)

    # Load model
    model = QuantizingAutoencoder() if config['quantize'] else ddsp.training.models.Autoencoder()
    model.restore(trained_model_dir)

    data_provider = ddsp.training.data.TFRecordProvider(config['test_tfrecord_filepattern'])
    dataset = data_provider.get_batch(batch_size=16, shuffle=False)

    i = 0
    with Report(config['report_dir'], config['report_name']) as report:
        for batch in dataset:
            # if random.random() > 0.2:
            #     continue
            # Resynthesize audio.
            audio_gen = model(batch, training=False)[0]  # Only take the first example from the batch
            audio = batch['audio'][0]  # Only take the first example from the batch
            report.add_row(tf.reshape(audio, (-1,)), tf.reshape(audio_gen, (-1,)))

            num_samples = config['num_samples']
            i += 1
            print(f"Sample {i}/{num_samples}")
            if i == num_samples:
                break


if __name__ == '__main__':
    app.run(main)
