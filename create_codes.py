import os

import ddsp.training
import gin
import tensorflow as tf
from absl import app
from absl import flags
from tqdm import tqdm

import vq_vae
from fma_utils import FmaTracks
from gtzan_utils import GtzanTracks

FLAGS = flags.FLAGS
flags.DEFINE_string(
    'tfrecord_dir', None,
    'The directory where the FMA SMALL tracks can be found The tracks need to be in the DDSP TFRecord format. '
    'It is expected that the tracks are encoded in non-overlaping windows!')
flags.DEFINE_string(
    'tracks_metadata_file', None,
    'The tracks metadata file.')
flags.DEFINE_string(
    'trained_model_dir', None,
    'The directory where the trained VQ-VAE DDSP model can be found.')
flags.DEFINE_string(
    'tfrecord_output_file', None,
    'The name of the TFRecord file into which the codes should be written.')
flags.DEFINE_enum(
    'dataset', None, ['fma_small', 'gtzan'],
    'The name of the dataset for which the codes should be generated.')


def create_codes(tfrecord_dir, tracks_metadata_file, trained_model_dir, tfrecord_output_file, dataset_name):
    # Parse the gin config.
    gin_file = os.path.join(trained_model_dir, 'operative_config-0.gin')
    gin.parse_config_file(gin_file)

    # Load model
    model = vq_vae.QuantizingAutoencoder()
    model.restore(trained_model_dir)

    if dataset_name == 'fma_small':
        tracks = FmaTracks(tracks_metadata_file)
    elif dataset_name == 'gtzan':
        tracks = GtzanTracks(tracks_metadata_file)
    else:
        raise ValueError(f'Undefined dataset: {dataset_name}')

    os.makedirs(os.path.dirname(tfrecord_output_file), exist_ok=True)

    with tf.io.TFRecordWriter(tfrecord_output_file, options=None) as writer:
        for filename in tqdm(os.listdir(tfrecord_dir)):
            record = os.path.join(tfrecord_dir, filename)
            data_provider = ddsp.training.data.TFRecordProvider(record)
            # Important: batch_size=7! We need a batch size of 7 because we start out with tracks with a length of 30s.
            # Dividing those 30s up into windows of length 4s with no overlap yields 8 windows, where the last window is
            # partially zero padded. To avoid falsifying the results, only the first 7 windows are used. This is, because
            # the encoder output is kind of undefined for zero padded input. By setting drop_remainder=True we get only the
            # first 7 windows and discard the last window that contains 2s of real audio and 2s of zero padding.
            dataset = data_provider.get_dataset(shuffle=False).batch(7, drop_remainder=True)
            track_id = f'{os.path.splitext(filename)[0]}.wav' if dataset_name == 'gtzan' else filename.split(".")[0]
            genre_idx = tracks.get_genre_idx(track_id)

            for i, batch in enumerate(dataset):
                if i > 0:
                    raise Exception("There should only ever be one batch!")
                z_batch = model.quantizer(model.encoder(model.preprocessor(batch, training=False)))["z_indices"]
                code_indices = tf.reshape(z_batch, (-1,))  # Flatten the batch

            try:
                code_indices = code_indices.numpy().tolist()
                example = tf.train.Example(features=tf.train.Features(feature={
                    'codes': tf.train.Feature(int64_list=tf.train.Int64List(value=code_indices)),
                    'track_id': tf.train.Feature(bytes_list=tf.train.BytesList(value=[track_id.encode('utf-8')])),
                    'genre_idx': tf.train.Feature(int64_list=tf.train.Int64List(value=[genre_idx]))
                })).SerializeToString()
                writer.write(example)
            except AttributeError:
                print(f"Cannot create codes for track {track_id}: variable code_indices does not seem to be a tensor")


# noinspection PyUnusedLocal
def main(argv):
    create_codes(
        tfrecord_dir=FLAGS.tfrecord_dir,
        tracks_metadata_file=FLAGS.tracks_metadata_file,
        trained_model_dir=FLAGS.trained_model_dir,
        tfrecord_output_file=FLAGS.tfrecord_output_file,
        dataset_name=FLAGS.dataset
    )


if __name__ == '__main__':
    flags.mark_flags_as_required(
        ['tfrecord_dir', 'tracks_metadata_file', 'trained_model_dir', 'tfrecord_output_file', 'dataset'])
    app.run(main)
