import os
from collections import namedtuple
from pathlib import Path

import numpy as np
import tensorflow as tf

import codes


def find_track_files(dataset_name):
    ds_path = f'D:/devel/datasets/{dataset_name}/wav'
    tracks = {}
    for root, _, files in os.walk(ds_path):
        for file in files:
            track_id = file if dataset_name == 'gtzan' else os.path.splitext(file)[0]
            track_path = os.path.join(root, file)
            tracks[track_id] = track_path
    return tracks


def get_track_features(dataset_name):
    ds = codes.get_codes_dataset("training", "validation", "test", dataset_name=dataset_name) \
        .map(codes.get_parse_codes_example_map_fn()) \
        .map(codes.get_calc_frequencies_map_fn())

    all_code_frequencies, all_track_ids = [], []
    for example in ds:
        code_frequencies, track_id = example['code_frequencies'], example['track_id'].numpy().decode('utf-8')
        all_code_frequencies.append(code_frequencies), all_track_ids.append(track_id)
    all_code_frequencies = tf.stack(all_code_frequencies, axis=0)
    all_track_ids = np.array(all_track_ids)
    return all_code_frequencies, all_track_ids


def get_track_code_frequencies(track_id, all_track_ids, code_frequencies):
    track_idx = np.where(all_track_ids == track_id)[0].item()
    return code_frequencies[track_idx]


def calc_dist_l1(code_freqs, track_freqs):
    return np.linalg.norm(code_freqs - track_freqs, axis=1, ord=1)


def calc_dist_l2(code_freqs, track_freqs):
    return np.linalg.norm(code_freqs - track_freqs, axis=1, ord=2)


distance_measures = {
    'L1': calc_dist_l1,
    'L2': calc_dist_l2,
}


def get_clossest_tracks(track_id, measure, all_track_ids, code_frequencies):
    track_freqs = get_track_code_frequencies(track_id, all_track_ids, code_frequencies)
    dist = distance_measures[measure](code_frequencies, track_freqs)
    dist_sorted = np.sort(dist)[1:]  # Remove the search track itself
    closest_tracks_idxs = np.argsort(dist)[1:]  # Remove the search track itself
    closest_tracks_ids = all_track_ids[closest_tracks_idxs]

    return closest_tracks_ids, dist_sorted, np.mean(dist), np.median(dist)


def calc_average_mean_distance(code_frequencies, track_ids, measure):
    print("Start calculating AMD")
    dists = []
    for track_id in track_ids:
        track_freqs = get_track_code_frequencies(track_id, track_ids, code_frequencies)
        dist = distance_measures[measure](code_frequencies, track_freqs)
        dist = np.sort(dist)[1:]  # Remove the distance to the track itself
        dists.append(dist)

    print("Finished calculating AMD")
    return np.mean(dists)


QueryTrack = namedtuple(
    "QueryTrack",
    ["name", "genre", "mean_distance", "median_distance", "audio_file"]
)
SimilarTrack = namedtuple(
    "SimilarTrack",
    ["name", "genre", "distance", "audio_file"]
)


class Report:
    def __init__(self, report_file, report_name, average_mean_distance):
        self.report_dir = Path(report_file).parent
        self.report_dir.mkdir(parents=True, exist_ok=True)
        self.file = open(report_file, 'w+', encoding="utf-8-sig")
        self.name = report_name
        self.average_mean_distance = average_mean_distance

    def __enter__(self):
        self.file.write('<!DOCTYPE html>')
        self.file.write('<html>')
        self.write_head(self.name)
        self.file.write('<body><div class="root">')
        self.file.write(f'<h1>{self.name}</h1>')
        self.file.write('<div class="main">')
        self.file.write(f'<div>Average mean distance of all tracks: {self.average_mean_distance}</div>')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.write('</div></div></body></html>')
        self.file.close()

    def add_audio(self, audio_file):
        rel_path = Path(audio_file).relative_to(self.report_dir)
        self.file.write('<audio controls preload="metadata">')
        self.file.write(f'<source src="{rel_path}" type="audio/wav">')
        self.file.write('</audio>')

    def add_track(self, query_track: QueryTrack, similar_tracks):
        self.file.write('<div class="row"><div class="column trackinfo">')
        self.file.write(f'<strong>{query_track.name}</strong> ({query_track.genre})<br>')
        self.file.write(f'Mean distance: {query_track.mean_distance:.4f}<br>')
        self.file.write(f'Median distance: {query_track.median_distance:.4f}')
        self.file.write('</div></div>')

        self.file.write('<div class="row">')
        self.add_audio(query_track.audio_file)
        self.file.write('</div>')

        self.file.write('<div class="row"><br><strong>Close tracks:</strong></div>')
        self.file.write('<div class="row">')
        for sim_track in similar_tracks:
            self.file.write('<div class="column">')
            self.file.write('<div>')
            self.add_audio(sim_track.audio_file)
            self.file.write('</div>')
            self.file.write(f'<div><strong>{sim_track.name}</strong> ({sim_track.genre})</div>')
            self.file.write(f'<div>Distance: {sim_track.distance:.4f}</div>')
            self.file.write('</div>')
        self.file.write('</div>')
        self.file.write('<br><hr><br>')

    def write_head(self, title):
        self.file.write("""<head>
    <meta charset="utf-8"/>
    <title>{title}</title>
    <style>
        html, body {
            height: 100%;
        }

        .row {
            display: table;
            border-spacing: 10px 0; /*Optional*/
        }

        .column {
            display: table-cell;
            text-align: left;
        }

        .trackinfo {
            vertical-align: middle;
            text-align: left;
        }

        .root {
            width: 100%;
            display: block;
            text-align: center;
        }

        .main {
            display: inline-block;
        }
    </style>
    <script>
    // Taken from https://stackoverflow.com/a/19792168
    document.addEventListener('play', function(e){
        var audios = document.getElementsByTagName('audio');
        for(var i = 0, len = audios.length; i < len;i++){
            if(audios[i] != e.target){
                audios[i].pause();
            }
        }
    }, true);
    </script>
</head>""".replace('{title}', title))
