import mir.mir_util as mir
import shutil
from fma_utils import FmaTracks
from pathlib import Path

distance_measure = 'L2'
dataset_name = 'fma_small'

fma_track_ids = "000140 000005 000182 098203 062191 148584 103518 038450 154307 152261 010675".split()

code_frequencies, track_ids = mir.get_track_features("fma_small")
average_mean_distance = mir.calc_average_mean_distance(code_frequencies, track_ids, distance_measure)

track_files_by_id = mir.find_track_files(dataset_name)
fma_tracks = FmaTracks("D:/devel/datasets/fma_small/metadata/tracks.csv")

root_dir = "D:/devel/reports/mir"
tracks_dir = f"{root_dir}/fma_small_tracks"
Path(tracks_dir).mkdir(parents=True, exist_ok=True)
report_file = f"{root_dir}/mir.html"
with mir.Report(report_file, "MIR examples", average_mean_distance) as report:
    for track_id in fma_track_ids:
        print(f"Adding track {track_id}")
        query_track_file = f'{tracks_dir}/{track_id}.wav'
        shutil.copyfile(track_files_by_id[track_id], query_track_file)
        closest_track_ids, dists, mean_dist, median_dist = mir.get_clossest_tracks(track_id, distance_measure,
                                                                                   track_ids, code_frequencies)
        sim_tracks = []
        for sim_track_id, dist in zip(closest_track_ids[:5], dists[:5]):
            genre = fma_tracks.get_genre(sim_track_id)
            sim_track_file = f'{tracks_dir}/{sim_track_id}.wav'
            shutil.copyfile(track_files_by_id[sim_track_id], sim_track_file)
            sim_track = mir.SimilarTrack(sim_track_id, genre, dist, sim_track_file)
            sim_tracks.append(sim_track)
        query_track = mir.QueryTrack(track_id, fma_tracks.get_genre(track_id), mean_dist, median_dist,
                                     query_track_file)
        report.add_track(query_track, sim_tracks)
