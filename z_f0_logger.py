from absl import logging as logger
import gin
from ddsp.training.models import Autoencoder


@gin.configurable
class LoggingAutoencoder(Autoencoder):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.call_counter: int = 0
        logger.info("init LoggingAutoEncoder")

    def call(self, features, training=True):
        """Run the core of the network, get predictions and loss."""

        if not training:
            self.call_counter = self.call_counter + 1
            logger.info("LoggingAutoencoder called %d times.", self.call_counter)
            for feature in features:
                logger.info("value of feature %s is\n%d", feature.__self__, feature)

        conditioning = self.encode(features, training=training)
        audio_gen = self.decode(conditioning, training=training)
        if training:
            for loss_obj in self.loss_objs:
                loss = loss_obj(features['audio'], audio_gen)
                self._losses_dict[loss_obj.name] = loss
        return audio_gen
