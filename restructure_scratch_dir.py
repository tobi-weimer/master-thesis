import sys
from absl import flags, app
from pathlib import Path

"""
example call:
nohup python generate_spectograms.py \
--BASE_DIR=/home/tobi/MA-Thesis/audios_generated/AimeeNorwich_Child \
--AUDIO_FILE_PATTERN=**/*.wav --FILE_STEM_EXTENSION=_specplot \
> /home/tweimer/logs/resynthesizeAudio.`date +"%Y-%m-%d_%H-%M"`.info.log \
2>/home/tweimer/logs/resynthesizeAudio.`date +"%Y-%m-%d_%H-%M"`.error.log &
"""

FLAGS = flags.FLAGS
flags.DEFINE_string(
    'from_dir', None,
    'The directory where files are searched and moved from')
flags.DEFINE_string(
    'audio_dir', None,
    'The director in which the audio files are stored')
flags.DEFINE_string(
    'audio_file_name_pattern', '*',
    'A pattern to filter which audio files the file movement shuold be based on')
flags.DEFINE_boolean(
    'change_name', False,
    'shall file names be changed?')
flags.DEFINE_string(
    'suffix', None,
    'optional new suffix for all new files')


def main(argv):
    from_dir = FLAGS.from_dir
    audio_dir = FLAGS.audio_dir
    audio_file_name_pattern = FLAGS.audio_file_name_pattern
    change_name = FLAGS.change_name
    suffix = FLAGS.suffix

    print(argv[0] + ' running with flags:',
          '\n--from_dir', from_dir,
          '\n--audio_dir', audio_dir,
          '\n--audio_file_name_pattern', audio_file_name_pattern,
          '\n--change_suffix', change_name,
          '\n--suffix', suffix,
          '\n', sep=None, flush=True)

    if from_dir is None or audio_dir is None:
        print('parameters --from_dir and --audio_dir are required!', file=sys.stderr)
        return
    if change_name and suffix is None:
        print('parameter --suffix is mandatory when --change_suffix is set true!', file=sys.stderr)
        return

    from_dir_path = Path(from_dir)
    audio_dir_path = Path(audio_dir)
    for audio_file in audio_dir_path.glob('**/' + audio_file_name_pattern + '.wav'):
        print('\nmoving files based on ' + str(audio_file))

        for file_to_move in from_dir_path.glob('**/' + audio_file.stem + '*'):
            if file_to_move.samefile(audio_dir_path):
                continue

            if change_name:
                new_path = Path(audio_file.parent, audio_file.stem + '_' + suffix + file_to_move.suffix)
            else:
                new_path = Path(audio_file.parent, file_to_move.stem + file_to_move.suffix)

            print('move file ' + str(file_to_move) + ' to ' + str(new_path))

            file_to_move.rename(new_path)


if __name__ == '__main__':
    app.run(main)
