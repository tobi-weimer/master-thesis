from absl import flags, app, logging
from pathlib import Path

from create_plot_from_csvs import create_plot, create_property_plot_with_instruments, FOLDER_PATH_HELP


FLAGS = flags.FLAGS

FILE_SAVE_PATH_HELP = 'The folder where the graphs will be saved.'
flags.DEFINE_string('FILE_SAVE_PATH', None, FILE_SAVE_PATH_HELP)
GRAPH_MODES: [str] = [
    'instrument_groups',
    'one_frequency_styles',
    'two_frequency_styles',
    'z'
]
GRAPHS_MODES_HELP = ('The modes of the graphs to be plotted. ' +
                     'Multiple values have to be separated with commas ("<MODE1>,<MODE2>"). ' +
                     'Available modes are ') + ', '.join(GRAPH_MODES)
flags.DEFINE_string('GRAPH_MODES', None, GRAPHS_MODES_HELP)
FILE_NAME_PREFIX_HELP = 'The prefix of the save file for the z graph.'
flags.DEFINE_string('FILE_NAME_PREFIX', None, FILE_NAME_PREFIX_HELP)


def create_plot_group(folder: Path, file_pattern: str, title_prefix: str, file_save_folder: str = None) -> None:
    save_file_f0_hz: str = None
    save_file_loudness_db: str = None
    save_file_f0_scaled: str = None
    save_file_ld_scaled: str = None
    save_file_f0_scaled_and_ld_scaled: str = None
    show_graph = True

    if file_save_folder is not None:
        show_graph = False
        save_file_f0_hz = file_save_folder + title_prefix + '_f0_hz.svg'
        save_file_loudness_db = file_save_folder + title_prefix + '_loudness_db.svg'
        save_file_f0_scaled = file_save_folder + title_prefix + '_f0_scaled.svg'
        save_file_ld_scaled = file_save_folder + title_prefix + '_ld_scaled.svg'
        save_file_f0_scaled_and_ld_scaled = file_save_folder + title_prefix + '_f0_scaled_and_ld_scaled.svg'

    create_plot(folder=folder,
                file_pattern=file_pattern,
                columns=['f0_hz'],
                y_axis_title='Hz',
                title=title_prefix + ' f0_hz',
                show_graph=show_graph,
                save_file=save_file_f0_hz)
    create_plot(folder=folder,
                file_pattern=file_pattern,
                columns=['loudness_db'],
                y_axis_title='Loudness (dB)',
                title=title_prefix + ' loudness_db',
                show_graph=show_graph,
                save_file=save_file_loudness_db)
    create_plot(folder=folder,
                file_pattern=file_pattern,
                columns=['f0_scaled'],
                y_axis_title='f0 scaled',
                title=title_prefix + ' f0_scaled',
                show_graph=show_graph,
                save_file=save_file_f0_scaled)
    create_plot(folder=folder,
                file_pattern=file_pattern,
                columns=['ld_scaled'],
                y_axis_title='ld scaled',
                title=title_prefix + ' ld_scaled',
                show_graph=show_graph,
                save_file=save_file_ld_scaled)
    create_plot(folder=folder,
                file_pattern=file_pattern,
                columns=['f0_scaled', 'ld_scaled'],
                y_axis_title='f0 scaled',
                title=title_prefix + ' f0_scaled and ld_scaled',
                show_graph=show_graph,
                save_file=save_file_f0_scaled_and_ld_scaled)


def create_property_plot_group(folder: Path, file_pattern: str, title_prefix: str, file_save_folder: str = None) -> None:
    save_file_f0_hz: str = None
    save_file_loudness_db: str = None
    save_file_f0_scaled: str = None
    save_file_ld_scaled: str = None
    save_file_f0_scaled_and_ld_scaled: str = None
    show_graph = True

    if file_save_folder is not None:
        show_graph = False
        save_file_f0_hz = file_save_folder + title_prefix + '_f0_hz.svg'
        save_file_loudness_db = file_save_folder + title_prefix + '_loudness_db.svg'
        save_file_f0_scaled = file_save_folder + title_prefix + '_f0_scaled.svg'
        save_file_ld_scaled = file_save_folder + title_prefix + '_ld_scaled.svg'
        save_file_f0_scaled_and_ld_scaled = file_save_folder + title_prefix + '_f0_scaled_and_ld_scaled.svg'

    create_property_plot_with_instruments(
        folder=folder,
        file_pattern=file_pattern,
        columns=['f0_hz'],
        y_axis_title='f0 frequency (Hz)',
        title='f0_hz ' + title_prefix,
        show_graph=show_graph,
        save_file=save_file_f0_hz)
    create_property_plot_with_instruments(
        folder=folder,
        file_pattern=file_pattern,
        columns=['loudness_db'],
        y_axis_title='Loudness (dB)',
        title='loudness_db ' + title_prefix,
        show_graph=show_graph,
        save_file=save_file_loudness_db)
    create_property_plot_with_instruments(
        folder=folder,
        file_pattern=file_pattern,
        columns=['f0_scaled'],
        y_axis_title='f0 frequency (scaled)',
        title='f0_scaled ' + title_prefix,
        show_graph=show_graph,
        save_file=save_file_f0_scaled)
    create_property_plot_with_instruments(
        folder=folder,
        file_pattern=file_pattern,
        columns=['ld_scaled'],
        y_axis_title='Loudness (ld_scaled)',
        title='ld_scaled ' + title_prefix,
        show_graph=show_graph,
        save_file=save_file_ld_scaled)
    create_property_plot_with_instruments(
        folder=folder,
        file_pattern=file_pattern,
        columns=['f0_scaled', 'ld_scaled'],
        y_axis_title='f0 frequency (scaled) and Loudness (ld\_scaled)',
        title='f0_scaled and ld_scaled ' + title_prefix,
        show_graph=show_graph,
        save_file=save_file_f0_scaled_and_ld_scaled)


def create_instrument_group_plots(folder: Path, file_save_folder: str = None) -> None:
    create_plot_group(folder=folder,
                      file_pattern='*viola_section*.csv',
                      title_prefix='viola_section',
                      file_save_folder=file_save_folder)
    create_plot_group(folder=folder,
                      file_pattern='*female_singer*.csv',
                      title_prefix='female singer',
                      file_save_folder=file_save_folder)
    create_plot_group(folder=folder,
                      file_pattern='*flute*.csv',
                      title_prefix='flute',
                      file_save_folder=file_save_folder)
    create_plot_group(folder=folder,
                      file_pattern='*_male_singer*.csv',
                      title_prefix='male singer',
                      file_save_folder=file_save_folder)
    create_plot_group(folder=folder,
                      file_pattern='*violin*.csv',
                      title_prefix='violin',
                      file_save_folder=file_save_folder)


def create_one_frequency_stype_plots(folder: Path, file_save_folder: str = None) -> None:
    create_property_plot_group(
        folder=folder,
        file_pattern='*one-constant-frequency_constant-loudness*.csv',
        title_prefix='constant frequency constant loudness',
        file_save_folder=file_save_folder)
    create_property_plot_group(
        folder=folder,
        file_pattern='*one-constant-frequency_increasing-loudness*.csv',
        title_prefix='constant frequency increasing loudness',
        file_save_folder=file_save_folder)
    create_property_plot_group(
        folder=folder,
        file_pattern='*one-constant-frequency_decreasing-loudness*.csv',
        title_prefix='constant frequency decreasing loudness',
        file_save_folder=file_save_folder)
    create_property_plot_group(
        folder=folder,
        file_pattern='*one-increasing-frequency_constant-loudness*.csv',
        title_prefix='increasing frequency constant loudness',
        file_save_folder=file_save_folder)
    create_property_plot_group(
        folder=folder,
        file_pattern='*one-decreasing-frequency_constant-loudness*.csv',
        title_prefix='decreasing frequency const loudness',
        file_save_folder=file_save_folder)


def create_two_frequencies_stype_plots(folder: Path, file_save_folder: str = None) -> None:
    create_property_plot_group(
        folder=folder,
        file_pattern='*two-constant-frequencies_constant-loudness*.csv',
        title_prefix='two constant frequency constant loudness.',
        file_save_folder=file_save_folder)
    create_property_plot_group(
        folder=folder,
        file_pattern='*two-constant-frequencies_increasing-loudness*.csv',
        title_prefix='two constant frequency increasing loudness',
        file_save_folder=file_save_folder)
    create_property_plot_group(
        folder=folder,
        file_pattern='*two-constant-frequencies_decreasing-loudness*.csv',
        title_prefix='two constant frequency decreasing loudness',
        file_save_folder=file_save_folder)
    create_property_plot_group(
        folder=folder,
        file_pattern='*two-increasing-frequencies_constant-loudness*.csv',
        title_prefix='two increasing frequency constant loudness',
        file_save_folder=file_save_folder)
    create_property_plot_group(
        folder=folder,
        file_pattern='*two-decreasing-frequencies_constant-loudness*.csv',
        title_prefix='two decreasing frequency const loudness',
        file_save_folder=file_save_folder)


def create_z_plots(folder: Path, file_save_folder: str, file_name_prefix: str, title: str) -> None:
    file_ids: [str] = ['011', '012', '013', '014', '015',
                  '021', '022', '023', '024', '025',
                  '031', '032', '033', '034', '035',
                  '041', '042', '043', '044', '045',
                  '051', '052', '053', '054', '055',
                  '061', '062', '063', '064', '065',
                  '071', '072', '073', '074', '075',
                  '081', '082', '083', '084', '085',
                  '091', '092', '093', '094', '095',
                  '101', '102', '103', '104', '105']
    for file_id in file_ids:
        create_z_plot(folder=folder,
                      file_save_folder=file_save_folder,
                      title=title,
                      file_name=file_name_prefix + file_id + '.csv',
                      file_pattern='*' + file_id + '*')


def create_z_plot(folder: Path, file_save_folder: str, file_name: str, title: str, file_pattern: str = None) -> None:
    save_file: str = None
    show_graph = True

    if file_save_folder is not None:
        show_graph = False
        if not file_save_folder.endswith('/'):
            file_save_folder += '/'
        if not file_name.endswith('.svg'):
            file_name += '.svg'
        save_file = file_save_folder + file_name

    logging.info('Creating plot for file pattern "%s" ...', file_pattern)

    create_plot(folder=folder,
                file_pattern=file_pattern,
                columns=['z_col_0', 'z_col_1', 'z_col_2', 'z_col_3',
                         'z_col_4', 'z_col_5', 'z_col_6', 'z_col_7',
                         'z_col_8', 'z_col_9', 'z_col_10', 'z_col_11',
                         'z_col_12', 'z_col_13', 'z_col_14', 'z_col_15'],
                y_axis_title='Z',
                title=title,
                show_graph=show_graph,
                save_file=save_file)

def main(argv):
    logging.info('Running %s with flags', argv[0])
    my_flags = FLAGS.get_flags_for_module(argv[0])
    for flag in my_flags:
        if flag.value is not None:
            logging.info('--%s %s', flag.name, str(flag.value))

    if FLAGS.FOLDER_PATH is None:
        message: str = ('Please provide parameter "--FOLDER_PATH <PATH>" with path being %s'
                        % FOLDER_PATH_HELP)
        logging.error(message)
        raise ValueError(message)
    folder: Path = Path(FLAGS.FOLDER_PATH)

    my_graph_modes_array: str = FLAGS.GRAPH_MODES
    if my_graph_modes_array is None:
        message: str = ('Please provide parameter "--GRAPH_MODES <MODE1>[,MODE2>...]"\n%s'
                        % GRAPHS_MODES_HELP)
        logging.error(message)
        raise ValueError(message)

    my_graph_modes: [str] = my_graph_modes_array.split(',')

    file_save_folder: str = FLAGS.FILE_SAVE_PATH
    if not file_save_folder.endswith('/'):
        file_save_folder += '/'

    Path(file_save_folder).mkdir(parents=True, exist_ok=True)

    # if file_save_folder is not None:
    #     file_save_path = file_save_folder + '/graph.svg'
    #     create_plot(folder=folder,
    #                 file_pattern='011_violas_one-constant-frequency_constant-loudness.csv',
    #                 columns=['f0_hz', 'f0_scaled', 'loudness_db', 'ld_scaled'],
    #                 y_axis_title='Hz',
    #                 show_graph=False,
    #                 save_file=file_save_path)

    if 'instrument_groups' in my_graph_modes:
        create_instrument_group_plots(folder, file_save_folder=file_save_folder)

    if 'one_frequency_styles' in my_graph_modes:
        create_one_frequency_stype_plots(folder, file_save_folder=file_save_folder)

    if 'two_frequency_styles' in my_graph_modes:
        create_two_frequencies_stype_plots(folder, file_save_folder=file_save_folder)

    if 'z' in my_graph_modes:
        if FLAGS.FILE_NAME_PREFIX is None:
            message = ('Please provide parameter "--FILE_NAME_PREFIX <VALUE". Help Message: '
                       + FILE_NAME_PREFIX_HELP)
            logging.error(message)
            raise ValueError(message)

        create_z_plots(folder=folder,
                       file_name_prefix=FLAGS.FILE_NAME_PREFIX,
                       file_save_folder=file_save_folder,
                       title=FLAGS.TITLE)


if __name__ == '__main__':
    app.run(main)
