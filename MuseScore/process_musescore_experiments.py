import os.path

from absl import flags, app, logging
from pathlib import Path
import time
import subprocess
import sys

FLAGS = flags.FLAGS
model_dir_help: str = 'The directory where the models can be found.'
flags.DEFINE_string('model_dir', None, model_dir_help)
source_dir_help: str = 'The directory where the source files can be found.'
flags.DEFINE_string('source_dir', None, source_dir_help)
target_dir_help: str = 'The directory where the created files will be saved.'
flags.DEFINE_string('target_dir', None, target_dir_help)
mode_help: str = 'encode (WAV to CSV), decode (CSV to WAV) or edit (change CSV)'
flags.DEFINE_string('mode', None, mode_help)
with_parameters_help: str = 'Whether or not to decode CSVs with edited parameters'
flags.DEFINE_bool('with_parameters', False, with_parameters_help)
edit_both_f0_help: str = 'edit both f0 parameters to keep their relation intact?'
flags.DEFINE_boolean('edit_both_f0', False, edit_both_f0_help)
overwrite_help: str = 'overwrite existing files?'
flags.DEFINE_bool('overwrite', False, overwrite_help)

instruments = [
    dict({'id': '1', 'instrument': 'viola_section'}),
    dict({'id': '2', 'instrument': 'female_singer'}),
    dict({'id': '3', 'instrument': 'flute'}),
    dict({'id': '4', 'instrument': 'violin'}),
    dict({'id': '5', 'instrument': 'male_singer'})]

file_name_parts_array = [
    dict({'prefix': '01', 'postfix': '_one-constant-frequency_constant-loudness'}),
    dict({'prefix': '02', 'postfix': '_one-constant-frequency_increasing-loudness'}),
    dict({'prefix': '03', 'postfix': '_one-constant-frequency_decreasing-loudness'}),
    dict({'prefix': '04', 'postfix': '_one-increasing-frequency_constant-loudness'}),
    dict({'prefix': '05', 'postfix': '_one-decreasing-frequency_constant-loudness'}),
    dict({'prefix': '06', 'postfix': '_two-constant-frequencies_constant-loudness'}),
    dict({'prefix': '07', 'postfix': '_two-constant-frequencies_increasing-loudness'}),
    dict({'prefix': '08', 'postfix': '_two-constant-frequencies_decreasing-loudness'}),
    dict({'prefix': '09', 'postfix': '_two-increasing-frequencies_constant-loudness'}),
    dict({'prefix': '10', 'postfix': '_two-decreasing-frequencies_constant-loudness'})]


parameters: [str] = [
    'f0_both',
    'f0_hz',
    'f0_scaled',
    'ld_scaled',
    'z'
]


edit_functions: [str] = [
    'null_value',
    'zero_value',
    'max_value',
    'min_value',
    'jump_min_max',
    'jump_low_high',
    'sin_min_max',
    'sin_low_high',
    'invert_value'
]


edit_functions_parameters: dict = {
    'jump_min_max': dict({
        '--jump_length': 250
    }),
    'jump_low_high': dict({
        '--jump_length': 250,
        '--low_value': 20,
        '--high_value': 8_000
    }),
    'sin_min_max': dict({
        '--wave_length': 250
    }),
    'sin_low_high': dict({
        '--wave_length': 250,
        '--low_value': 20,
        '--high_value': 8_000
    })
}


def encode(this_scripts_location: str, models_dir: str, source_dir: str, target_dir: str):

    if not models_dir.endswith("/"):
        models_dir += "/"

    if not source_dir.endswith('/'):
        source_dir += '/'

    if not target_dir.endswith('/'):
        target_dir += '/'

    execute_script_name = this_scripts_location.replace(
        'process_musescore_experiments.py',
        '../ddsp_scripts/audio_worker.py')

    for instrument in instruments:
        for file_name_parts in file_name_parts_array:
            file_name = (file_name_parts['prefix'] +
                         instrument['id'] +
                         '_' +
                         instrument['instrument'] +
                         file_name_parts['postfix'])
            logging.info('encoding file %s with %s model', file_name, instrument['instrument'])

            command = [
                sys.executable, execute_script_name,
                '--MODE', 'encode',
                '--MODEL_DIR', f"{models_dir}{instrument['instrument']}/",
                '--SOURCE_AUDIO_FILE', f'{source_dir}{file_name}.wav',
                '--TARGET_CSV_FILE', f'{target_dir}{file_name}.csv',
                '--OVERWRITE', 'True'
            ]
            execute_command(command)

    logging.info('all files encoded.')


def decode(this_scripts_location: str,
           models_dir: str,
           source_dir: str,
           target_dir: str,
           with_parameters: bool = False,
           overwrite: bool = False):

    if not models_dir.endswith("/"):
        models_dir += "/"

    if not source_dir.endswith('/'):
        source_dir += '/'

    if not target_dir.endswith('/'):
        target_dir += '/'

    execute_script_name = this_scripts_location.replace(
        'process_musescore_experiments.py',
        '../ddsp_scripts/audio_worker.py')

    for instrument in instruments:
        for file_name_parts in file_name_parts_array:
            file_name = (file_name_parts['prefix'] +
                         instrument['id'] +
                         '_' +
                         instrument['instrument'] +
                         file_name_parts['postfix'])
            if with_parameters:
                if 'two' not in file_name_parts['postfix']:
                    for parameter in parameters:
                        target_file_name = parameter + '_' + file_name
                        decode_execute(execute_script_name,
                                       models_dir + instrument['instrument'] + '/',
                                       source_dir + target_file_name + '.csv',
                                       target_dir + target_file_name + '.wav',
                                       overwrite)
            else:
                decode_execute(execute_script_name,
                               models_dir + instrument['instrument'] + '/',
                               source_dir + file_name + '.csv',
                               target_dir + file_name + '.wav',
                               overwrite)

    logging.info('all files decoded.')


def decode_execute(execute_script_name: str,
                   model_dir: str,
                   source_file: str,
                   target_file: str,
                   overwrite: bool = False):
    logging.info('decoding file %s with model %s', source_file, model_dir)

    if not os.path.exists(source_file):
        logging.error('source file %s does not exist', source_file)
        return

    if os.path.exists(target_file):
        logging.info('target file %s already exists. Do not overwrite.', target_file)
        return

    command = [
        sys.executable, execute_script_name,
        '--MODE', 'decode',
        '--MODEL_DIR', model_dir,
        '--SOURCE_CSV_FILE', source_file,
        '--TARGET_AUDIO_FILE', target_file,
        '--OVERWRITE', str(overwrite)
    ]
    execute_command(command)


def edit(this_scripts_location: str, source_dir: str, target_dir: str):
    if not source_dir.endswith('/'):
        source_dir += '/'

    if not target_dir.endswith('/'):
        target_dir += '/'

    for edit_function in edit_functions:
        edit_function_target_dir = target_dir + edit_function + '/'
        Path(edit_function_target_dir).expanduser().mkdir(parents=True, exist_ok=True)

    execute_script_name = this_scripts_location.replace(
                    'process_musescore_experiments.py',
                    '../ddsp_scripts/csv_editor.py')

    for instrument in instruments:
        for file_name_parts in file_name_parts_array:
            if 'two' not in file_name_parts['postfix']:
                file_name = (file_name_parts['prefix'] +
                             instrument['id'] +
                             '_' +
                             instrument['instrument'] +
                             file_name_parts['postfix'])
                edit_one_file(execute_script_name, file_name, source_dir, target_dir)

    logging.info('all files edited.')


def edit_one_file(execute_script_name, file_name, source_dir, target_dir):
    logging.info('editing file %s', file_name)
    base_command = [
        sys.executable, execute_script_name,
        '--source_csv_file', source_dir + file_name + '.csv',
        '--edit_both_f0', str(FLAGS.edit_both_f0)
    ]
    for parameter in parameters:
        for edit_function in edit_functions:
            command = base_command.copy()
            command += [
                '--target_csv_file', f'{target_dir}/{edit_function}/{parameter}_{file_name}.csv',
                '--edit_function', edit_function,
                '--parameter_name', parameter
            ]

            if edit_function in edit_functions_parameters:
                edit_function_parameters: dict = edit_functions_parameters.get(edit_function)

                for key, value in edit_function_parameters.items():
                    command.append(key)
                    command.append(str(value))

            execute_command(command)


def execute_command(command: [str]):
    logging.info('executing command: %s', ' '.join(command))

    subprocess.call(command)
    time.sleep(2)


def main(argv):
    logging.info('Running %s with flags', argv[0])
    my_flags = FLAGS.get_flags_for_module(argv[0])
    for flag in my_flags:
        if flag.value is not None:
            logging.info('--%s %s', flag.name, str(flag.value))

    if FLAGS.source_dir is None:
        message: str = f'Please specify parameter "--source_dir <PATH>": {source_dir_help}'
        logging.error(message)
        raise ValueError(message)
    if FLAGS.target_dir is None:
        message: str = f'Please specify parameter "--target_dir <PATH>": {target_dir_help}'
        logging.error(message)
        raise ValueError(message)
    if FLAGS.mode is None:
        message: str = f'Please specify parameter "--mode <encode/decode>": {mode_help}'
        logging.error(message)
        raise ValueError(message)

    mode = FLAGS.mode.lower()
    if not (mode == 'encode' or mode == 'decode' or mode == 'edit'):
        message: str = 'Parameter "--mode <value>" must have either of values: "encode", "decode", "edit"'
        logging.error(message)
        raise ValueError(message)

    if mode == 'encode':
        if FLAGS.model_dir is None:
            message: str = f'Please specify parameter "--model_dir <PATH>": {model_dir_help}'
            logging.error(message)
            raise ValueError(message)

        encode(str(argv[0]), FLAGS.model_dir, FLAGS.source_dir, FLAGS.target_dir)
    elif mode == 'decode':
        if FLAGS.model_dir is None:
            message: str = f'Please specify parameter "--model_dir <PATH>": {model_dir_help}'
            logging.error(message)
            raise ValueError(message)

        decode(str(argv[0]),
               FLAGS.model_dir,
               FLAGS.source_dir,
               FLAGS.target_dir,
               FLAGS.with_parameters,
               FLAGS.overwrite)
    elif mode == 'edit':
        edit(str(argv[0]), FLAGS.source_dir, FLAGS.target_dir)
    else:
        message: str = 'Parameter "--mode <value>" must have either of values: "encode", "decode", "edit"'
        logging.error(message)
        raise ValueError(message)


if __name__ == '__main__':
    app.run(main)
