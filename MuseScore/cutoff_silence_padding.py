import logging
import subprocess

from absl import flags, app
from pathlib import Path


FLAGS = flags.FLAGS
absolute_folder_path_help = 'The absolute path of the folder containing the wav files'
flags.DEFINE_string(
    'absolute_folder_path', None,
    absolute_folder_path_help)
file_pattern_help = 'The pattern of the wav files'
flags.DEFINE_string(
    'file_pattern', None,
    file_pattern_help
)
target_files_prefix_help = 'The prefix of the resulting files'
flags.DEFINE_string('target_files_prefix', '',
                    target_files_prefix_help)
target_files_postfix_help = 'The postfix of the resulting files'
flags.DEFINE_string('target_files_postfix', '',
                    target_files_postfix_help)


# noinspection PyUnusedLocal
def main(argv):
    logging.info('Running %s with flags', argv[0])
    my_flags = FLAGS.get_flags_for_module(argv[0])
    for flag in my_flags:
        if flag.value is not None:
            logging.info('--%s %s', flag.name, str(flag.value))

    if FLAGS.absolute_folder_path is None:
        message: str = ('Please provide additional parameter "--absolute_folder_path <PATH>" - %s'
                        % absolute_folder_path_help)
        logging.error(message)
        raise ValueError(message)

    if FLAGS.file_pattern is None:
        message: str = ('Please provide additional parameter "--file_pattern <PATH>" - %s'
                        % file_pattern_help)
        logging.error(message)
        raise ValueError(message)
    file_pattern: str = FLAGS.file_pattern
    if not file_pattern.endswith('.wav'):
        file_pattern += '.wav'

    if FLAGS.target_files_prefix == '' and FLAGS.target_files_postfix == '':
        message: str = ('Please provide at least one of the additional parameters ' +
                        '"--target_files_prefix <PREFIX> or "--target_files_postfix <POSTFIX>"')
        logging.error(message)
        raise ValueError(message)

    # all parameters are checked now

    absolute_folder_path: Path = Path(FLAGS.absolute_folder_path)
    logging.info('searching files with pattern %s in folder %s',
                 FLAGS.file_pattern,
                 FLAGS.absolute_folder_path)
    for wav_file in absolute_folder_path.glob(file_pattern):
        logging.info('shortening file: %s',
                     wav_file)
        command = [
            'sox',
            str(wav_file),
            str(wav_file.parent) + '/'
            + FLAGS.target_files_prefix + wav_file.stem + FLAGS.target_files_postfix + wav_file.suffix,
            'trim',
            '0',
            '4.25'
        ]
        logging.debug('Executing command: %s',
                     ' '.join(command))
        subprocess.call(command)
    logging.info('Finished processing all files.')


if __name__ == '__main__':
    app.run(main)
