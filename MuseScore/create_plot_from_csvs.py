from absl import flags, app, logging
from pathlib import Path
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go


FLAGS = flags.FLAGS

FOLDER_PATH_HELP: str = 'the folder containing the CSV files.'
flags.DEFINE_string('FOLDER_PATH', None, FOLDER_PATH_HELP)

FILE_PATTERN_HELP: str = 'the name pattern of the CSV files to include in the graph'
flags.DEFINE_string('FILE_PATTERN', None, FILE_PATTERN_HELP)

COLUMNS_HELP: str = 'the name of the columns to include in the graph'
flags.DEFINE_string('COLUMNS', None, COLUMNS_HELP)

Y_AXIS_TITLE_HELP: str = 'the name of the y-axis title'
flags.DEFINE_string('Y_AXIS_TITLE', None, Y_AXIS_TITLE_HELP)

TITLE_HELP: str = 'the title of the graph'
flags.DEFINE_string('TITLE', None, TITLE_HELP)

SHOW_GRAPH_HELP: str = 'show the graph?'
flags.DEFINE_bool('SHOW_GRAPH', True, SHOW_GRAPH_HELP)

SAVE_FILE_HELP: str = 'the path and name of the optional save file'
flags.DEFINE_string('SAVE_FILE', None, SAVE_FILE_HELP)

def create_plot(
        folder: Path,
        file_pattern: str,
        columns: [str],
        y_axis_title: str,
        title: str = None,
        show_graph: bool = True,
        save_file: str = None):

    if not folder.exists():
        message: str = 'The provided source folder %s does not exist' % FLAGS.FOLDER_PATH
        logging.error(message)
        raise ValueError(message)

    if save_file is not None and not Path(save_file).parent.exists():
        message = 'The folder to save the plot does not exists!'
        logging.error(message)
        raise ValueError(message)

    # all checks successful

    layout = go.Layout(
        yaxis_title=y_axis_title,
        xaxis_title='timesteps',
        height=400,  # px
        width=760  # px
    )
    if title is not None:
        layout.title = title

    figure = go.Figure(layout=layout)
    for csv in folder.glob(file_pattern):
        csv_data = pd.read_csv(csv)
        for column in columns:
            line = px.line(csv_data, x="Unnamed: 0", y=column)
            line_updated = line.update_traces(line_color=None, showlegend=True, name=csv.stem + " " + column)
            line_data = line_updated.data
            figure.add_trace(line_data[0])

    if show_graph:
        figure.show()

    if save_file is not None:
        figure.write_image(save_file)


def create_property_plot_with_instruments(
        folder: Path,
        file_pattern: str,
        columns: [str],
        y_axis_title: str,
        title: str = None,
        show_graph: bool = True,
        save_file: str = None):

    if not folder.exists():
        message: str = 'The provided source folder %s does not exist' % FLAGS.FOLDER_PATH
        logging.error(message)
        raise ValueError(message)

    if save_file is not None and not Path(save_file).parent.exists():
        message = 'The folder to save the plot does not exists!'
        logging.error(message)
        raise ValueError(message)

    # all checks successful

    layout = go.Layout(
        yaxis_title=y_axis_title,
        xaxis_title='time steps',
        height=400,  # px
        width=510  # px
    )
    if title is not None:
        layout.title = title

    figure = go.Figure(layout=layout)
    for csv in folder.glob(file_pattern):
        csv_data = pd.read_csv(csv)
        instrument_name = get_instrument_name(str(csv))
        for column in columns:
            line = px.line(csv_data, x="Unnamed: 0", y=column)
            line_updated = line.update_traces(
                line_color=None,
                showlegend=True,
                name=instrument_name)
            line_data = line_updated.data
            figure.add_trace(line_data[0])

    if show_graph:
        figure.show()

    if save_file is not None:
        figure.write_image(save_file)



instruments: [str] = [
    'female_singer',
    'flute',
    'male_singer',
    'viola_section',
    'violin'
]


def get_instrument_name(filename: str) -> str:
    for instrument in instruments:
        if instrument in filename:
            return instrument.replace('_', ' ')


# noinspection PyUnusedLocal
def main(argv):
    logging.info('Running %s with flags', argv[0])
    my_flags = FLAGS.get_flags_for_module(argv[0])
    for flag in my_flags:
        if flag.value is not None:
            logging.info('--%s %s', flag.name, str(flag.value))

    folder: Path = Path(FLAGS.FOLDER_PATH)
    if folder is None:
        message: str = ('Please provide parameter "--FOLDER_PATH <PATH>" with path being %s'
                        % FOLDER_PATH_HELP)
        logging.error(message)
        raise ValueError(message)

    file_pattern: str = FLAGS.FILE_PATTERN
    if file_pattern is None:
        message: str = \
            ('Please provide a pattern for the CSV files to include in the graph as parameter '
             + '"-FILE_PATTERN <PATTERN>". The minimal pattern is "*" (all)')
        logging.error(message)
        raise ValueError(message)
    if not file_pattern.endswith('.csv'):
        file_pattern = file_pattern + '.csv'

    columns: str = FLAGS.COLUMNS
    if columns is None:
        message: str = \
            'Please provide the name of the column to include in the graph as parameter '\
            + '"--COLUMNS <COLUMN1[,COLUMN2,...]>".'
        logging.error(message)
        raise ValueError(message)
    columns = columns.split(',')

    y_axis_title: str = FLAGS.Y_AXIS_TITLE
    if y_axis_title is None:
        message: str = 'Please provide an y axis title as parameter "--Y_AXIS_TITLE <TITLE>".'
        logging.error(message)
        raise ValueError(message)

    show_graph: bool = FLAGS.SHOW_GRAPH
    save_file: str = FLAGS.SAVE_FILE

    create_plot(folder=folder,
                file_pattern=file_pattern,
                columns=columns,
                y_axis_title=y_axis_title,
                show_graph=show_graph,
                save_file=save_file)


if __name__ == "__main__":
    app.run(main)
