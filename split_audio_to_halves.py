import subprocess
import librosa
import sys
from os import path, makedirs
from pathlib import Path


def main():
    """split all music tracks in a directory tree into two sub-tracks of equal length by calling ffmpeg from the
    shell. They are stored in base_target_folder, keeping the original subdirectory structure. """

    # check command line for original file and track list file
    if len(sys.argv) != 3:
        print('usage: split_audio_to_halves <base_source_folder> <base_target_folder>')
        exit(1)

    # record command line args
    base_source_folder: str = sys.argv[1]
    base_target_folder: str = sys.argv[2]

    # check existence of folders
    if not path.exists(base_source_folder) & path.isdir(base_source_folder):
        print("base source folder does not exist or is not a folder!\nfolder name: {}"
              .format(base_source_folder), file=sys.stderr)
        exit(1)

    if not path.exists(base_target_folder) & path.isdir(base_target_folder):
        print('creating target directory {}'.format(base_target_folder))
        try:
            makedirs(base_target_folder)
        except OSError:
            print("Directory '{}}' can not be created".format(base_target_folder))

    # begin actual program
    for file_path in Path(base_source_folder).glob('**/*.wav'):
        print('processing {}'.format(file_path))
        split_audio_in_halves(file_path, base_source_folder, base_target_folder)

    return None


def split_audio_in_halves(audio_file: Path, base_source_folder: str, base_target_folder: str):
    """split audio_file in two halves of equal length, store them relative to base_target_folder"""

    # define input and output file names with path
    # source audio file
    track_original: Path = audio_file

    # target audio files
    relative_path = path.relpath(audio_file.parent, base_source_folder)
    target_folder = path.join(base_target_folder, relative_path)
    track_half1: str = '{}/{}_half1'.format(target_folder, audio_file.stem)
    track_half2: str = '{}/{}_half2'.format(target_folder, audio_file.stem)

    # create target folder if not exists
    if not path.exists(target_folder) & path.isdir(target_folder):
        print('creating target directory {}'.format(target_folder))
        try:
            makedirs(target_folder)
        except OSError:
            print("Directory '{}' can not be created".format(target_folder))

    # get duration of original track
    end: float = librosa.get_duration(filename=str(track_original))
    # define further time steps in track
    start: float = 0.0
    middle: float = end / 2

    # create a template of the ffmpeg call in advance
    cmd_string: str = 'ffmpeg -i {tr} -acodec copy -ss {st} -to {en} {nm}{ext} -v error'

    # create first half using subprocess to execute the command in the shell
    command: str = cmd_string.format(tr=track_original, st=start, en=middle, nm=track_half1, ext=audio_file.suffix)
    subprocess.call(command, shell=True)

    # create second half using subprocess to execute the command in the shell
    command = cmd_string.format(tr=track_original, st=middle, en=end, nm=track_half2, ext=audio_file.suffix)
    subprocess.call(command, shell=True)

    return None


if __name__ == '__main__':
    main()
