import yaml
import sys
from pathlib import Path

# noinspection PyPackageRequirements
from absl import flags, app

FLAGS = flags.FLAGS
flags.DEFINE_string(
    name='yaml_files_folder', required=True, default=None,
    help='the folder where all yaml files per song can be found')
flags.DEFINE_string(
    name='files_root_dir', default=None,
    help='the root folder of the files to rename'
)
flags.DEFINE_boolean(
    name='include_files_in_subdir', default=True,
    help='include files in subfolders'
)
flags.DEFINE_string(
    name='folders_root_dir', default=None,
    help='the root folder of the folders to rename'
)


def main(argv):
    # noinspection PyTypeChecker
    folders_root_dir: Path = None
    # noinspection PyTypeChecker
    files_root_dir: Path = None
    include_subdir_files: bool = False

    yaml_files_folder = Path(FLAGS.yaml_files_folder)
    if FLAGS.files_root_dir is not None:
        files_root_dir = Path(FLAGS.files_root_dir)

    if FLAGS.include_files_in_subdir is not None:
        include_subdir_files = True

    if FLAGS.folders_root_dir is not None:
        folders_root_dir = Path(FLAGS.folders_root_dir)

    if folders_root_dir is None and files_root_dir is None:
        print(
            "Require at least one parameter of '--files_root_dir' of '--folders_root_dir'.",
            file=sys.stderr
        )
        exit(0)

    counter_folders: int = 0
    counter_files: int = 0

    if folders_root_dir is not None:
        for folder_to_rename in folders_root_dir.glob("*_STEM_*"):
            if folder_to_rename.is_file():
                continue

            counter_folders += rename_item(folder_to_rename, yaml_files_folder, folders_root_dir)

    if files_root_dir is not None:
        counter_files += rename_file(files_root_dir, include_subdir_files, yaml_files_folder)

    print("renamed {counter_files} files and {counter_folders} folders.".format(
            counter_files=counter_files, counter_folders=counter_folders))


def rename_file(files_root_dir, include_subdir_files, yaml_files_folder):
    counter_files: int = 0

    if include_subdir_files:
        for item in files_root_dir.iterdir():
            if item.is_dir():
                counter_files += rename_file(item, include_subdir_files, yaml_files_folder)

    for file_to_rename in files_root_dir.glob("*_STEM_*"):
        if file_to_rename.is_dir():
            continue

        if not include_subdir_files and file_to_rename.parent is not files_root_dir:
            continue

        counter_files += rename_item(file_to_rename, yaml_files_folder, files_root_dir)

    return counter_files


def rename_item(item_to_rename: Path, yaml_files_folder: Path, root_dir: Path):
    original_name = str(item_to_rename.name)
    substring_end = original_name.find("_STEM_")
    name_base = original_name[0:substring_end]
    stem_number = original_name[substring_end + 6:substring_end + 8]
    name_post = original_name[substring_end + 9:len(original_name)]
    stem_entry = "S" + stem_number

    yaml_file = yaml_files_folder.joinpath(name_base + "_METADATA.yaml")

    with yaml_file.open() as my_yaml_file:
        content = yaml.safe_load(my_yaml_file)

        instrument = content["stems"][stem_entry]["instrument"]
        instrument = str(instrument).replace(" ", "_")
        instrument = str(instrument).replace("/", "_")

        if item_to_rename.is_dir():
            new_file_dir = str(root_dir) + "/" + name_base + "/STEM_" + stem_number + "_" + instrument
            new_file_name = new_file_dir + "/" + name_post

            Path(new_file_dir).mkdir(parents=True, exist_ok=True)
        elif item_to_rename.is_file():
            if 'half' in name_post.lower():
                my_name_post = '_' + name_post
            else:
                my_name_post = '.' + name_post

            new_file_name = str(root_dir) + "/STEM_" + stem_number + "_" + instrument + my_name_post

        item_to_rename.rename(new_file_name)

        return 1


if __name__ == '__main__':
    app.run(main)
