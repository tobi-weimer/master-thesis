import os.path

import pandas as pd


def load(filepath):
    filename = os.path.basename(filepath)
    return pd.read_csv(filepath, index_col=0, header=[0])


class GtzanTracks:
    def __init__(self, gtzan_tracks_file):
        self.tracks_df = load(gtzan_tracks_file)

    def get_genre(self, track_id):
        return self.tracks_df.loc[track_id]['label']

    def get_genre_idx(self, track_id):
        return gtzan_genres.index(self.get_genre(track_id))

    @property
    def genres(self):
        return gtzan_genres


gtzan_genres = ['blues', 'classical', 'country', 'disco', 'hiphop', 'jazz', 'metal', 'pop', 'reggae', 'rock']
