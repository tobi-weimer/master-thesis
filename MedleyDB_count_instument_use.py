from pathlib import Path

import yaml

from absl import flags, app

FLAGS = flags.FLAGS
flags.DEFINE_string(
    name='yaml_files_folder', required=True, default=None,
    help='the folder where all yaml files per song can be found')


def main(argv):
    yaml_files_folder = Path(FLAGS.yaml_files_folder)

    counter = {}

    for yaml_file in yaml_files_folder.iterdir():
        with yaml_file.open() as my_yaml_file:
            content = yaml.safe_load(my_yaml_file)

            for stem_entry in content["stems"]:
                instrument = content["stems"][stem_entry]["instrument"]
                if type(instrument) is list:
                    instrument = instrument[0]
                count : int = 0
                if instrument in counter:
                    count : int = counter[instrument]
                count += 1
                counter[instrument] = count

    print(yaml.dump(counter))


if __name__ == '__main__':
    app.run(main)
