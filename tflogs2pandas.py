#!/usr/bin/env python3

"""
based on https://github.com/theRealSuperMario/supermariopy/blob/master/scripts/tflogs2pandas.py
"""

import glob
import os
import pprint
import struct
import traceback

import click
import pandas as pd
from tensorboard.backend.event_processing.event_accumulator import EventAccumulator
from pathlib import Path


processed_files: int = 0


# Extraction function
def tflog2pandas(path: str, song: str = "", stem: str = "", instrument: str = "") -> pd.DataFrame:
    """convert single tensorflow log file to pandas DataFrame

    Parameters
    ----------
    path : str
        path to tensorflow log file
    song : str
        the name of the song
    stem : str
        the name and number of the stem
    instrument : str
        the name of the instrument

    Returns
    -------
    pd.DataFrame
        converted dataframe
    """
    DEFAULT_SIZE_GUIDANCE = {
        "compressedHistograms": 1,
        "images": 1,
        "scalars": 0,  # 0 means load all
        "histograms": 1,
    }
    runlog_data = pd.DataFrame({"metric": [], "value": [], "step": []})
    # noinspection PyBroadException
    try:
        event_acc = EventAccumulator(path, DEFAULT_SIZE_GUIDANCE)
        event_acc.Reload()
        # tags = event_acc.Tags()["scalars"]
        tags = event_acc.Tags()["tensors"]
        for tag in tags:
            if tag in ['gin/operative_config-0', 'steps_per_sec']:
                continue

            # event_list = event_acc.Scalars(tag)
            event_list = event_acc.Tensors(tag)

            r_values = []
            r_steps = []
            for event in event_list:
                r_values.append(struct.unpack('f', event.tensor_proto.tensor_content)[0])
                r_steps.append(event.step)

            r = {
                "song": [song] * len(r_steps),
                "instrument": [instrument] * len(r_steps),
                "stem": [stem] * len(r_steps),
                "metric": [tag] * len(r_steps),
                "value": r_values,
                "step": r_steps
            }
            r = pd.DataFrame(r)
            runlog_data = pd.concat([runlog_data, r])
            global processed_files
        # noinspection PyUnboundLocalVariable
        processed_files += 1

    # Dirty catch of DataLossError
    except Exception:
        print("Event file possibly corrupt: {}".format(path))
        traceback.print_exc()
    return runlog_data


def many_logs2pandas(event_paths, song: str = "", stem: str = "", instrument: str = ""):
    all_logs = pd.DataFrame()
    for path in event_paths:
        print("Processing {path}".format(path=str(path)))
        log = tflog2pandas(str(path), song=song, stem=stem, instrument=instrument)
        if log is not None:
            if all_logs.shape[0] == 0:
                all_logs = log
            else:
                all_logs = all_logs.append(log, ignore_index=True)
    return all_logs


@click.command()
@click.argument("logdir-or-logfile")
@click.option(
    "--write-pkl/--no-write-pkl", help="save to pickle file or not", default=False
)
@click.option(
    "--write-csv/--no-write-csv", help="save to csv file or not", default=True
)
@click.option("--out-dir", "-o", help="output directory", default=".")
def main(logdir_or_logfile: str, write_pkl: bool, write_csv: bool, out_dir: str):
    """This is a enhanced version of
    https://gist.github.com/ptschandl/ef67bbaa93ec67aba2cab0a7af47700b

    This script exctracts variables from all logs from tensorflow event
    files ("event*"),
    writes them to Pandas and finally stores them a csv-file or
    pickle-file including all (readable) runs of the logging directory.

    Example usage:

    # create csv file from all tensorflow logs in provided directory (.)
    # and write it to folder "./converted"
    tflogs2pandas.py . --write-csv --no-write-pkl --o converted

    # create csv file from tensorflow logfile only and write into
    # and write it to folder "./converted"
    tflogs2pandas.py tflog.hostname.12345 --write-csv --no-write-pkl --o converted
    """
    pp = pprint.PrettyPrinter(indent=4)
    songs: bool = False
    if os.path.isdir(logdir_or_logfile):
        # Get all event* runs from logging_dir subdirectories
        event_paths = glob.glob(os.path.join(logdir_or_logfile, "**/event*"), recursive=True)
        songs = True
    elif os.path.isfile(logdir_or_logfile):
        event_paths = [logdir_or_logfile]
    else:
        raise ValueError(
            "input argument {} has to be a file or a directory".format(
                logdir_or_logfile
            )
        )

    total_files = len(event_paths)
    write: bool = False
    if songs:

        print("Found {x} tensorflow logs to process:".format(x=total_files))

        all_logs = pd.DataFrame()

        base_dir = Path(logdir_or_logfile)

        for song_dir in base_dir.iterdir():
            for stem_dir in song_dir.iterdir():
                event_paths = stem_dir.glob(pattern="**/events.out*")
                stem = stem_dir.name[0:7]
                instrument = stem_dir.name[8:]
                logs_to_append = many_logs2pandas(event_paths, song=song_dir.name, stem=stem, instrument=instrument)
                if logs_to_append is not None:
                    if all_logs.shape[0] == 0:
                        all_logs = logs_to_append
                    else:
                        all_logs = all_logs.append(logs_to_append, ignore_index=True)

        print("All processing done. {x} processings were successful. {y} processings had errors."
              .format(x=processed_files, y=(total_files-processed_files)))
        print("Head of created dataframe")
        pp.pprint(all_logs.head())
        write = True
    # Call & append
    elif event_paths and not songs:
    # if event_paths:
        pp.pprint("Found tensorflow logs to process:")
        pp.pprint(event_paths)
        all_logs = many_logs2pandas(event_paths)
        pp.pprint("Head of created dataframe")
        pp.pprint(all_logs.head())
        write = True

    if write:
        os.makedirs(out_dir, exist_ok=True)
        if write_csv:
            print("saving to csv file")
            out_file = os.path.join(out_dir, "all_training_logs_in_one_file.csv")
            print(out_file)
            # noinspection PyTypeChecker
            all_logs.to_csv(out_file, index=None)
        if write_pkl:
            print("saving to pickle file")
            out_file = os.path.join(out_dir, "all_training_logs_in_one_file.pkl")
            print(out_file)
            all_logs.to_pickle(out_file)
    else:
        print("No event paths have been found.")


if __name__ == "__main__":
    main()
