# call as: nohup bash scripts/train_vq_vae_violin.sh > log/train_ae_violin.log &

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
root_dir="$(dirname "$script_dir")"
export PYTHONPATH=$root_dir

ddsp_run \
  --mode=train \
  --alsologtostderr \
  --save_dir="D:/devel/trained_models/ae_violin_local" \
  --gin_file="$root_dir/models/vq_vae.gin" \
  --gin_file=datasets/tfrecord.gin \
  --gin_param="TFRecordProvider.file_pattern='D:/devel/datasets/violin_long_50_ddsp/*'" \
  --gin_param="batch_size=1" \
  --gin_param="train_util.train.num_steps=30000" \
  --gin_param="train_util.train.steps_per_save=300" \
  --gin_param="trainers.Trainer.checkpoints_to_keep=10"