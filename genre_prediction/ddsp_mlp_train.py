import time

import numpy as np
import tensorflow as tf
import yaml
from keras import Sequential
from keras.layers import Input, Dense, Dropout, BatchNormalization

import codes


def get_dataset(split, dataset_name, model_name, batch_size=10):
    return codes.get_codes_dataset(split, dataset_name=dataset_name, model_name=model_name) \
        .map(codes.get_parse_codes_example_map_fn()) \
        .map(codes.get_calc_frequencies_map_fn(normalize=True)) \
        .map(codes.get_as_tuple_map_fun('code_frequencies', 'genre_idx')) \
        .shuffle(8000) \
        .batch(batch_size)


def run_trial(train_data, val_data, test_data, num_classes, max_epochs):
    model = Sequential([
        Input(64),
        Dropout(0.2),
        Dense(64, activation='relu'),
        BatchNormalization(),
        Dropout(0.2),
        Dense(32, activation='relu'),
        BatchNormalization(),
        Dropout(0.2),
        Dense(16, activation='relu'),
        BatchNormalization(),
        Dropout(0.2),
        Dense(num_classes, activation='softmax')
    ])

    optimizer = tf.optimizers.Adam()
    model.compile(loss='sparse_categorical_crossentropy', optimizer=optimizer, metrics='accuracy')

    patience = 30
    esc = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=patience, mode='auto',
                                           restore_best_weights=True)

    model.fit(train_data, epochs=max_epochs, validation_data=val_data, callbacks=[esc])
    num_epochs = esc.stopped_epoch - patience
    _, acc = model.evaluate(test_data)
    return acc, num_epochs


def main():
    number_of_trials = 10
    max_epochs = 1000
    mean_accuracies = {}
    mean_epoch_counts = {}

    for dataset_name, num_classes in [('gtzan', 10), ('fma_small', 8)]:
        for model_name in ['gtzan_train', 'fma_small_train', ]:
            accuracies = []
            epoch_counts = []
            for i in range(number_of_trials):
                train_data, val_data, test_data = [get_dataset(split, dataset_name, model_name, 56) for split in
                                                   ['training', 'validation', 'test']]
                acc, epoch_count = run_trial(train_data, val_data, test_data, num_classes, max_epochs)
                epoch_counts.append(epoch_count)
                accuracies.append(acc)
            key = f'{dataset_name}_using_model_{model_name}'
            mean_accuracies[key] = np.mean(accuracies).item()
            mean_epoch_counts[key] = np.mean(epoch_counts).item()

    report_name = f'ddsp_mlp_train'
    report_path = f'D:/devel/reports/genre_prediction/{report_name}_{time.strftime("%Y%m%d-%H%M%S")}.yml'
    report = {
        'name': report_name,
        'trials': number_of_trials,
        'max_epochs': max_epochs,
        'mean_accuracies': mean_accuracies,
        'mean_epoch_counts': mean_epoch_counts
    }

    report_yml = yaml.dump(report, indent=4)
    print(report_yml)
    with open(report_path, 'w') as file:
        file.write(report_yml)


if __name__ == '__main__':
    main()
