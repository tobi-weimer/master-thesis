import time

import numpy as np
import pandas as pd
import tensorflow as tf
import yaml
from sklearn.preprocessing import LabelEncoder, StandardScaler
from keras import Sequential
from keras.layers import Input, Dense, Dropout, BatchNormalization

import codes


def get_libros_df(split, dataset_name):
    df = pd.read_csv(f'D:/devel/datasets/{dataset_name}/librosa/{split}.csv')
    return df


def get_codes_df(split, dataset_name):
    ds = codes.get_codes_dataset(split, dataset_name=dataset_name, model_name=f'{dataset_name}_train') \
        .map(codes.get_parse_codes_example_map_fn()) \
        .map(codes.get_calc_frequencies_map_fn(normalize=True))

    features = ds.map(codes.get_as_tuple_map_fun('code_frequencies')).as_numpy_iterator()
    track_ids = ds.map(codes.get_as_tuple_map_fun('track_id')).as_numpy_iterator()
    track_ids = np.array([tid.decode('utf-8') for tid in track_ids])
    if dataset_name == 'fma_small':
        track_ids = np.array(([f'{tid}.wav' for tid in track_ids]))

    df = pd.DataFrame.from_records(features)
    df['filename'] = track_ids

    return df


def get_merged_dataset(split, dataset_name):
    librosa_df = get_libros_df(split, dataset_name)
    librosa_df = librosa_df.drop(columns=[f'mfcc{i}' for i in range(1, 21)])  # Drop the MFCC features
    codes_df = get_codes_df(split, dataset_name)
    merged_df = pd.merge(codes_df, librosa_df, on='filename')

    # Dropping unneccesary columns
    data = merged_df.drop(['filename'], axis=1)

    genre_list = data.iloc[:, -1]
    encoder = LabelEncoder()
    y = encoder.fit_transform(genre_list)

    # normalizing
    scaler = StandardScaler()
    x = scaler.fit_transform(np.array(data.iloc[:, :-1], dtype=float))
    return x, y


def run_trial(train_data, val_data, test_data, num_classes, epochs):
    model = Sequential([
        Input(70),
        Dropout(0.2),
        Dense(64, activation='relu'),
        BatchNormalization(),
        Dropout(0.2),
        Dense(32, activation='relu'),
        BatchNormalization(),
        Dropout(0.2),
        Dense(16, activation='relu'),
        BatchNormalization(),
        Dropout(0.2),
        Dense(num_classes, activation='softmax')
    ])

    optimizer = tf.optimizers.Adam()
    model.compile(loss='sparse_categorical_crossentropy', optimizer=optimizer, metrics='accuracy')

    esc = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=30, mode='auto', restore_best_weights=True)

    model.fit(train_data[0], train_data[1], epochs=epochs, validation_data=val_data, callbacks=[esc], batch_size=56)
    _, acc = model.evaluate(test_data[0], test_data[1])
    return acc


def main():
    number_of_trials = 10
    max_epochs = 1000
    mean_accuracies = {}

    for dataset_name, num_classes in [('gtzan', 10), ('fma_small', 8)]:
        accuracies = []
        for i in range(number_of_trials):
            train_data, val_data, test_data = [get_merged_dataset(split, dataset_name) for split in
                                               ['training', 'validation', 'test']]
            # Test twice while swapping val_data and test_data
            accuracies.append(run_trial(train_data, val_data, test_data, num_classes, max_epochs))
            mean_accuracies[f'{dataset_name}'] = np.mean(accuracies).item()

    report_name = f'ddsp_codes+librosa_mlp'
    report_path = f'D:/devel/reports/genre_prediction/{report_name}_{time.strftime("%Y%m%d-%H%M%S")}.yml'
    report = {
        'name': report_name,
        'trials': number_of_trials,
        'max_epochs': max_epochs,
        'mean_accuracies': mean_accuracies
    }

    report_yml = yaml.dump(report, indent=4)
    print(report_yml)
    with open(report_path, 'w') as file:
        file.write(report_yml)


if __name__ == '__main__':
    main()
