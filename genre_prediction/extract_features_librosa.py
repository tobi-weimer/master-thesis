import librosa
import numpy as np
import os
import csv
from fma_utils import FmaTracks
from gtzan_utils import GtzanTracks
from tqdm import tqdm


def extract_features(audio_dir, extracted_features_file, metadata_tracks, dataset):
    # generating a dataset
    header = 'filename chroma_stft rmse spectral_centroid spectral_bandwidth rolloff zero_crossing_rate'
    for i in range(1, 21):
        header += f' mfcc{i}'
    header += ' label'
    header = header.split()

    os.makedirs(os.path.dirname(extracted_features_file), exist_ok=True)
    with open(extracted_features_file, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(header)
        for filename in tqdm(os.listdir(audio_dir)):
            track_id = filename if dataset == 'gtzan' else int(filename.split(".")[0])
            genre = metadata_tracks.get_genre(track_id)
            songname = os.path.join(audio_dir, filename)
            y, sr = librosa.load(songname, mono=True)
            chroma_stft = librosa.feature.chroma_stft(y=y, sr=sr)
            rmse = librosa.feature.rms(y=y)
            spec_cent = librosa.feature.spectral_centroid(y=y, sr=sr)
            spec_bw = librosa.feature.spectral_bandwidth(y=y, sr=sr)
            rolloff = librosa.feature.spectral_rolloff(y=y, sr=sr)
            zcr = librosa.feature.zero_crossing_rate(y)
            mfcc = librosa.feature.mfcc(y=y, sr=sr)
            to_append = f'{filename} {np.mean(chroma_stft)} {np.mean(rmse)} {np.mean(spec_cent)} {np.mean(spec_bw)} {np.mean(rolloff)} {np.mean(zcr)}'
            for e in mfcc:
                to_append += f' {np.mean(e)}'
            to_append += f' {genre}'
            writer.writerow(to_append.split())


def main():
    datasets = ['fma_small', 'gtzan']
    splits = ['training', 'validation', 'test']
    dataset_root_dir = 'D:/devel/datasets'
    overwrite = False

    for dataset in datasets:
        for split in splits:
            audio_dir = os.path.join(dataset_root_dir, dataset, 'wav', split)
            extracted_features_file = os.path.join(dataset_root_dir, dataset, 'librosa', f'{split}.csv')

            if os.path.isfile(extracted_features_file) and not overwrite:
                print(f'Features file for {dataset}/{split} already exists. Skipping')
                continue

            tracks = FmaTracks(os.path.join(dataset_root_dir, dataset, 'metadata/tracks.csv')) if dataset == 'fma_small' \
                else GtzanTracks(os.path.join(dataset_root_dir, dataset, 'metadata/features_30_sec.csv'))
            print(f'Extraction features for {dataset}/{split} using {tracks} metadata')
            extract_features(audio_dir, extracted_features_file, tracks, dataset)
            print(f'Finished extracting features for {dataset}/{split}')


if __name__ == '__main__':
    main()
