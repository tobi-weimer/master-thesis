import time

import numpy as np
import pandas as pd
import tensorflow as tf
import yaml
from sklearn.preprocessing import LabelEncoder, StandardScaler
from keras import Sequential
from keras.layers import Input, Dense, Dropout, BatchNormalization


def get_dataset(split, dataset_name):
    data = pd.read_csv(f'D:/devel/datasets/{dataset_name}/librosa/{split}.csv')

    # Dropping unneccesary columns
    data = data.drop(['filename'], axis=1)

    genre_list = data.iloc[:, -1]
    encoder = LabelEncoder()
    y = encoder.fit_transform(genre_list)

    # normalizing
    scaler = StandardScaler()
    X = scaler.fit_transform(np.array(data.iloc[:, :-1], dtype=float))
    return X, y


def run_trial(train_data, val_data, test_data, num_classes, max_epochs):
    model = Sequential([
        Input(26),
        Dropout(0.2),
        Dense(64, activation='relu'),
        BatchNormalization(),
        Dropout(0.2),
        Dense(32, activation='relu'),
        BatchNormalization(),
        Dropout(0.2),
        Dense(16, activation='relu'),
        BatchNormalization(),
        Dropout(0.2),
        Dense(num_classes, activation='softmax')
    ])

    optimizer = tf.optimizers.Adam()
    model.compile(loss='sparse_categorical_crossentropy', optimizer=optimizer, metrics='accuracy')

    esc = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=30, mode='auto', restore_best_weights=True)
    model.fit(train_data[0], train_data[1], epochs=max_epochs, validation_data=val_data, callbacks=[esc], batch_size=64)
    _, acc = model.evaluate(test_data[0], test_data[1])
    return acc


def main():
    number_of_trials = 10
    max_epochs = 1000
    report = {
        'trials': number_of_trials,
        'max_epochs': max_epochs,
        'mean_accuracies': {}
    }
    report_path = f'D:/devel/reports/genre_prediction/librosa_mlp_{time.strftime("%Y%m%d-%H%M%S")}.yml'
    for dataset_name, num_classes in [('gtzan', 10), ('fma_small', 8)]:
        accuracies = []
        for i in range(number_of_trials):
            train_data, val_data, test_data = [get_dataset(split, dataset_name) for split in
                                               ['training', 'validation', 'test']]
            # Test twice while swapping val_data and test_data
            accuracies.append(run_trial(train_data, val_data, test_data, num_classes, max_epochs))
            report['mean_accuracies'][f'{dataset_name}'] = np.mean(accuracies).item()
    report_yml = yaml.dump(report, indent=4)
    print(report_yml)
    with open(report_path, 'w') as file:
        file.write(report_yml)


if __name__ == '__main__':
    main()
