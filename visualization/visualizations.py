from pathlib import Path

from absl import flags

FLAGS = flags.FLAGS
flags.DEFINE_string("config", None, "Path to a report config yaml file.")


class Report:
    def __init__(self, report_file, report_name):
        self.report_dir = Path(report_file).parent
        self.report_dir.mkdir(parents=True, exist_ok=True)
        self.file = open(report_file, 'w+', encoding="utf-8-sig")
        self.name = report_name

    def __enter__(self):
        self.file.write('<!DOCTYPE html>')
        self.file.write('<html><head>')
        self.file.write('<meta charset="utf-8"/>')
        self.file.write(f'<title>{self.name}</title>')
        self.file.write('</head><body>')
        self.file.write(f'<h1 style="width:100%; text-align: center">{self.name}</h1>')
        self.file.write('<table style="margin: 0 auto">')
        return self

    def add_img_row(self, *img_files):
        self.file.write('<tr>')
        for img in img_files:
            img_ref = Path(img).relative_to(self.report_dir)
            self.file.write('<td>')
            self.write_image(img_ref)
            self.file.write('</td>')
        self.file.write('</tr>')

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.write('</table></body></html>')
        self.file.close()

    def write_image(self, img_ref):
        self.file.write('<div align="center">')
        self.file.write(f'<img src="{img_ref}" />')
        self.file.write('</div>')


def create_offset_labels(labels):
    hl = r'$\frac{\;\;\;\;\;\;\;\;\;}{\;}$'
    return [c if i % 2 == 0 else f'{c} {hl}' for i, c in enumerate(labels)]
