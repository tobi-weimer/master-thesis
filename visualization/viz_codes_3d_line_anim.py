import matplotlib
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import tensorflow as tf
from mpl_toolkits.mplot3d import Axes3D

from codes import get_codes_dataset, get_parse_codes_example_map_fn, get_calc_code_transitions_map_fn


def main():
    matplotlib.use("TkAgg")
    ds = get_codes_dataset("training", dataset_name='fma_small') \
        .map(get_parse_codes_example_map_fn()) \
        .map(get_calc_code_transitions_map_fn())

    for example in ds:
        transitions, track_id = example['transitions'], example['track_id']
        timesteps = tf.expand_dims(tf.range(0, transitions.shape[0]), axis=-1)
        points = tf.concat((timesteps, transitions), axis=-1).numpy()
        # points = points[:100]
        frn = points.shape[0]
        max_points = 30
        fps = 15  # frames per sec

        def plot_3d(plot_points):
            return Axes3D.plot(ax, xs=plot_points[:, 1], ys=plot_points[:, 2], zs=plot_points[:, 0], c='b')

        def update_plot(i, plot):
            plot[0][0].remove()
            start = max(0, i - max_points)
            end = i
            frame_points = points[start: end]
            plot[0] = plot_3d(frame_points)

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.set_xlabel("s(t)")
        ax.set_ylabel("s(t+1)")
        ax.set_zlabel("t")
        plot = [plot_3d(points[:0])]
        ani = animation.FuncAnimation(fig, update_plot, frn, fargs=(plot,), interval=1000 / fps)
        plt.xlim(0, 64)
        plt.ylim(0, 64)

        plt.show()
        exit()


if __name__ == '__main__':
    main()
