import unittest

import codes
from visualization.visualize_transition_heatmap import create_ordered_transition_counts_matrix_turned_90_degrees
import numpy as np
import numpy.testing as np_test


class TestVisualizeCodesHeatmap(unittest.TestCase):

    def test_create_ordered_transition_counts_matrix_turned_90_degrees(self):
        #    0 1
        # 0| 1 2
        # 1| 3 4
        trans = np.array([[1, 1], [0, 1], [0, 0], [0, 1], [1, 1], [1, 0], [1, 0], [1, 1], [1, 0], [1, 1]])
        unique_trans, probs = codes.unique_transitions_with_probabilities(trans)
        m = create_ordered_transition_counts_matrix_turned_90_degrees(unique_trans, probs, code_ordering=[1, 0],
                                                                      codebook_size=2)
        np_test.assert_array_equal(m, np.array([[3 / 7, 1 / 3],
                                                [4 / 7, 2 / 3]]))
