import os
from collections import defaultdict

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

import codes
from fma_utils import FmaTracks
from gtzan_utils import GtzanTracks
from visualization import visualizations


def create_ordered_transition_counts_matrix_turned_90_degrees(transition_counts, code_ordering, codebook_size,
                                                              drop_self_transitions):
    transitions = transition_counts.non_self_transitions if drop_self_transitions else transition_counts.all_transitions
    counts = transition_counts.non_self_counts if drop_self_transitions else transition_counts.all_counts
    m = np.zeros((codebook_size, codebook_size))
    m[transitions[:, 0], transitions[:, 1]] = counts
    m = m[:, code_ordering][code_ordering]
    m = np.rot90(m)
    return m


def create_transition_prob_heatmap(transition_counts, code_ordering, plot_title, save_file, drop_self_transitions,
                                   show):
    # Normalize counts by the number of tracks, so results are comparable between FMA and GTZAN
    counts = transition_counts.non_self_counts if drop_self_transitions else transition_counts.all_counts
    m = create_ordered_transition_counts_matrix_turned_90_degrees(transition_counts, code_ordering, 64,
                                                                  drop_self_transitions)
    fig, ax = plt.subplots(figsize=(7, 7))

    norm = mpl.colors.LogNorm()
    vmin = np.min(counts[np.nonzero(counts)])

    im = ax.imshow(m, vmin=vmin, vmax=np.max(counts), norm=norm, cmap='Blues')
    cbar = ax.figure.colorbar(im, ax=ax, fraction=0.046, pad=0.04)
    cbar.ax.set_ylabel(r"Mittlere Anzahl an Übergängen $s_i \to s_j $ pro Song", rotation=90, va='bottom',
                       fontsize='x-large', labelpad=20)
    ticks = np.arange(0, 64)
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.set_xlabel("s(t)", fontsize='x-large')
    ax.set_ylabel("s(t+1)", fontsize='x-large')
    if plot_title:
        ax.set_title(plot_title, fontsize='x-large')
    tick_labels = visualizations.create_offset_labels(code_ordering)
    ax.set_xticklabels(tick_labels, fontsize='7', rotation='90')
    ax.set_yticklabels(tick_labels[::-1], fontsize='7')
    plt.tight_layout()
    plt.savefig(save_file)
    if show:
        plt.show()
    plt.close()


def chunks(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def visualize(clustering_method, root_dir, show_title, show):
    img_files = defaultdict(list)

    for drop_self_transitions in [False]:  # [False, True]
        for dataset in ['fma_small', 'gtzan']:
            trained_model_dir = f'D:/devel/trained_models/vq_vae_{dataset}_train'
            codebook = codes.load_model_codebook(trained_model_dir)
            code_ordering = codes.get_optimal_codebook_ordering(codebook, clustering_method)
            ds = codes.get_codes_dataset("training", "validation", "test", dataset_name=dataset,
                                         model_name=f'{dataset}_train') \
                .map(codes.get_parse_codes_example_map_fn()) \
                .map(codes.get_calc_code_transitions_map_fn())

            tracks_path = f"d:/devel/datasets/{dataset}/metadata/tracks.csv"
            tracks = FmaTracks(tracks_path) if dataset == 'fma_small' else GtzanTracks(tracks_path)

            transitions_by_genre = defaultdict(list)

            for example in ds:
                transitions, genre_idx = example['transitions'], example['genre_idx']
                genre = tracks.genres[genre_idx]
                transitions_by_genre[genre].append(transitions)

            transitions_by_genre = {g: tf.concat(t, axis=0) for g, t in transitions_by_genre.items()}

            vis_save_dir = root_dir + '/transition-heatmap'
            os.makedirs(vis_save_dir, exist_ok=True)
            name_segment_log_scale = 'no-self' if drop_self_transitions else 'all'

            for genre, transitions in transitions_by_genre.items():
                # Show the normal transition probability heatmap
                transition_counts = codes.compute_transition_counts(transitions, norm_by_num_tracks=True)
                plot_title = f'{dataset.upper()} - {genre}' if show_title else None
                img_file = os.path.join(vis_save_dir,
                                        f'heatmap-{name_segment_log_scale}-{dataset}-{genre.lower()}.png')
                create_transition_prob_heatmap(transition_counts, code_ordering, plot_title, img_file,
                                               drop_self_transitions,
                                               show)
                img_files[f'{drop_self_transitions}-{dataset}'].append(img_file)

            report_file = root_dir + "/transition-heatmaps.html"
            with visualizations.Report(report_file,
                                       f"Heatmaps der Codeübergänge (Clusteringmethode={clustering_method})") as report:
                for img_group in img_files.values():
                    for imgs in chunks(img_group, 2):
                        report.add_img_row(*imgs)


if __name__ == '__main__':
    visualize("average", f'D:/devel/reports/visualizations-average',
              show_title=True, show=True)
