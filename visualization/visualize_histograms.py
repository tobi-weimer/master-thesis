import os
from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np

from codes import get_parse_codes_example_map_fn, get_codes_dataset, load_model_codebook, \
    get_optimal_codebook_ordering, get_calc_frequencies_map_fn
from fma_utils import FmaTracks
from gtzan_utils import GtzanTracks
from visualization import visualizations


def chunks(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


# noinspection DuplicatedCode
def show_histogram(frequencies, code_ordering, min_frequency, max_frequency, plot_title, save_file, show):
    frequencies = frequencies[code_ordering]
    bins = np.arange(64)
    fig, ax = plt.subplots(figsize=(8, 5))
    ax.bar(bins, frequencies)
    ax.set_xticks(bins)
    ax.set_xticklabels(visualizations.create_offset_labels(code_ordering), rotation=90, fontsize='small')
    ax.set_ylim(min_frequency * 1.1, max_frequency * 1.1)
    plt.xlabel("Codes", fontsize="x-large")
    plt.ylabel("Mittlere Codefrequenz pro Song", fontsize="x-large")
    if plot_title:
        plt.suptitle(f'\n{plot_title}', fontsize="x-large")
    plt.tight_layout()
    plt.savefig(save_file)
    if show:
        plt.show()
    plt.close()


def visualize(clustering_method, root_dir, show_title, show):
    viz_dir = f'{root_dir}/histogram'
    report_file = f'{viz_dir}.html'
    os.makedirs(viz_dir, exist_ok=True)

    all_viz_files = defaultdict(list)
    for dataset in ['fma_small', 'gtzan']:
        trained_model_dir = f'D:/devel/trained_models/vq_vae_{dataset}_train'
        codebook = load_model_codebook(trained_model_dir)
        code_ordering = get_optimal_codebook_ordering(codebook, clustering_method)
        ds = get_codes_dataset("training", "validation", "test", dataset_name=dataset, model_name=f'{dataset}_train') \
            .map(get_parse_codes_example_map_fn()) \
            .map(get_calc_frequencies_map_fn(normalize=False))

        tracks_path = f"d:/devel/datasets/{dataset}/metadata/tracks.csv"
        tracks = FmaTracks(tracks_path) if dataset == 'fma_small' else GtzanTracks(tracks_path)

        frequencies_by_genre = {g: [] for g in tracks.genres}

        for example in ds:
            code_frequencies, genre_idx = example['code_frequencies'], example['genre_idx']
            genre = tracks.genres[genre_idx]
            frequencies_by_genre[genre].append(code_frequencies.numpy())

        frequencies_by_genre = {g: np.mean(f, axis=0) for g, f in frequencies_by_genre.items()}
        frequencies_of_dataset = np.mean(list(frequencies_by_genre.values()), axis=0)
        max_frequency = np.max(list(frequencies_by_genre.values()) + [frequencies_of_dataset])
        frequency_diffs_by_genre = {g: f - frequencies_of_dataset for g, f in frequencies_by_genre.items()}
        min_freq_diff, max_freq_diff = np.min(list(frequency_diffs_by_genre.values())), \
                                       np.max(list(frequency_diffs_by_genre.values()))

        plot_title = f'{dataset.upper()}' if show_title else None
        viz_file = os.path.join(viz_dir, f'histogram-{dataset}.png')
        all_viz_files[f'{dataset}-complete'].append(viz_file)
        show_histogram(frequencies_of_dataset, code_ordering, 0, max_frequency, plot_title, viz_file, show)
        for genre in frequencies_by_genre.keys():
            frequencies, frequency_diffs = frequencies_by_genre[genre], frequency_diffs_by_genre[genre]
            # Create the frequency bar chart
            plot_title = f'{dataset.upper()} - {genre}' if show_title else None
            viz_file = os.path.join(viz_dir, f'histogram-{dataset}-{genre.lower()}.png')
            all_viz_files[f'{dataset}-abs'].append(viz_file)
            show_histogram(frequencies, code_ordering, 0, max_frequency, plot_title, viz_file, show)
            # Create the frequency diff bar chart
            plot_title = f'{dataset.upper()} - {genre} (Difference to {dataset.upper()})' if show_title else None
            viz_file = os.path.join(viz_dir, f'histogram-{dataset}-{genre.lower()}-diff.png')
            all_viz_files[f'{dataset}-diff'].append(viz_file)
            show_histogram(frequency_diffs, code_ordering, min_freq_diff, max_freq_diff, plot_title, viz_file, show)

    with visualizations.Report(report_file, f"Code Frequenzen (Clusteringmethode={clustering_method})") as report:
        for img_group in all_viz_files.values():
            [report.add_img_row(*imgs) for imgs in chunks(img_group, 2)]


if __name__ == '__main__':
    visualize("average", f'D:/devel/reports/visualizations-average', show_title=True, show=True)
