from visualization import visualize_hac_dendogram, visualize_histograms, visualize_transition_heatmap, \
    visualize_transition_graph_per_genre

print("Creating visualizations:")
for show_title in [True, False]:
    for clustering_method in [None, "average"]:  # "single", "complete", "ward"
        print(f"Method: {clustering_method}")

        save_dir = f'D:/devel/reports/visualizations-{clustering_method}{"" if show_title else "-no-title"}'

        if clustering_method:
            print(f"  - visualizing denodgrams")
            visualize_hac_dendogram.visualize(clustering_method, save_dir, show_title, show=False)

        print(f"  - visualizing histograms")
        visualize_histograms.visualize(clustering_method, save_dir, show_title, show=False)

        print(f"  - visualizing heatmaps")
        visualize_transition_heatmap.visualize(clustering_method, save_dir, show_title, show=False)

        print(f"  - visualizing graphs")
        visualize_transition_graph_per_genre.visualize(clustering_method, save_dir, show_title, show=False)
