import matplotlib
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import numpy as np

from codes import get_codes_dataset, get_parse_codes_example_map_fn


def main():
    matplotlib.use("TkAgg")
    ds = get_codes_dataset("training", dataset_name='fma_small') \
        .map(get_parse_codes_example_map_fn())

    num_visible_codes = 200
    for example in ds:
        y = example['codes'].numpy()
        x = np.arange(len(y))

        fig, ax = plt.subplots()
        line, = ax.plot(x, y, color='k')

        def update(num, x, y, line):
            min_idx = max(0, num - num_visible_codes)
            max_idx = num
            line.set_data(x[min_idx:max_idx], y[min_idx:max_idx])
            line.axes.axis([min_idx, min_idx + num_visible_codes, 0, 63])
            return line,

        ani = animation.FuncAnimation(fig, update, len(y), fargs=[x, y, line],
                                      interval=10, blit=True)
        # ani.save('test.gif')
        plt.show()
        exit()


if __name__ == '__main__':
    main()
