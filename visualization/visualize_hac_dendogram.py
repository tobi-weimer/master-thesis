from pathlib import Path

import matplotlib.pyplot as plt
from scipy.cluster import hierarchy

import codes
from visualization import visualizations
from visualization.visualizations import Report


def visualize(clustering_method, root_dir, show_title, show):
    viz_dir = f'{root_dir}/clustering'
    Path(viz_dir).mkdir(parents=True, exist_ok=True)
    viz_files = []

    for dataset in ['fma_small', 'gtzan']:
        trained_model_dir = f'D:/devel/trained_models/vq_vae_{dataset}_train'
        codebook = codes.load_model_codebook(trained_model_dir)
        code_ordering, linkage = codes.get_optimal_codebook_ordering(codebook, clustering_method, return_linkage=True)

        hierarchy.dendrogram(linkage)
        if show_title:
            plt.title(f'{dataset.upper()} Code clustering', fontsize='x-large')
        ax = plt.gca()
        ax.set_xticklabels(visualizations.create_offset_labels(code_ordering), rotation=90, fontsize='small')
        plt.xlabel("Codes", fontsize='x-large')
        plt.ylabel("Distanz", fontsize='x-large')
        viz_file = f'{viz_dir}/clustering-{dataset}.png'
        viz_files.append(viz_file)
        plt.tight_layout()
        plt.savefig(viz_file)
        if show:
            plt.show()
        plt.close()

    report_file = f'{root_dir}/clustering.html'
    with Report(report_file, f"Clustering (Methode={clustering_method})") as report:
        report.add_img_row(*viz_files)


if __name__ == '__main__':
    visualize("average", f'D:/devel/reports/visualizations-average', show_title=True, show=True)
