import os
from collections import defaultdict

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import tensorflow as tf
from matplotlib import colors, cm

import codes
from fma_utils import FmaTracks
from gtzan_utils import GtzanTracks
from visualization.visualizations import Report


def draw_graphs(transitions, code_frequencies, code_ordering, dataset, genre, save_dir, use_log_norm, show_title, show):
    tcs = codes.compute_transition_counts(transitions, norm_by_num_tracks=True)

    files = []
    for n_bins in [20]:  # [10, 20, 50]
        file_name = os.path.join(save_dir,
                                 f'graph-{dataset}-{genre.lower()}-cut-off-{n_bins}.png')
        files.append(file_name)

        bin_counts, bin_edges = np.histogram(tcs.non_self_counts, bins=n_bins)
        threshold = bin_edges[1]
        edges, counts = tcs.non_self_transitions[np.where(tcs.non_self_counts >= threshold)], \
                        tcs.non_self_counts[np.where(tcs.non_self_counts >= threshold)]

        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 6))
        plt.sca(ax1)
        plt.title(fr'Codeübergänge $s_i \to s_j$ (Bin 1/{n_bins} ignoriert)', fontsize='x-large')
        plt.sca(ax2)
        plt.title('Verteilung der Übergangshäufigkeit', fontsize='x-large')
        if show_title:
            plt.suptitle(f'{dataset.upper()} - {genre}', fontweight='bold', fontsize='x-large', y=0.1, x=0.25)

        G = nx.DiGraph()
        for i in code_ordering:
            G.add_node(i)
        for edge, count in zip(edges, counts):
            G.add_edge(*edge, count=count)
        node_sizes = np.take(code_frequencies.numpy(), G.nodes()) * 64 * 100
        pos = nx.circular_layout(G)
        # Create colors on a log scale
        g_edges, g_weights = zip(*nx.get_edge_attributes(G, 'count').items())
        norm_fn = colors.LogNorm if use_log_norm else colors.Normalize
        norm = norm_fn(vmin=counts.min(), vmax=counts.max())
        mapper = cm.ScalarMappable(norm=norm, cmap=cm.Blues)
        edge_colors = [mapper.to_rgba(w) for w in g_weights]
        options = {
            'arrowstyle': '-',
            'with_labels': True,
        }
        nx.draw(G, pos=pos, node_size=node_sizes, edgelist=g_edges, edge_color=edge_colors, ax=ax1, **options)
        # plt.colorbar(mapper, ax=ax1)

        bar = ax2.bar(np.arange(n_bins), bin_counts)
        ax2.set_xlabel(r"Übergangsfrequenz $s \to s'$ pro Song (binned)", fontsize='x-large')
        ax2.set_xticks(np.arange(len(bin_counts)))
        x_labels = ['' for _ in bin_counts]
        x_labels[0] = f'<{bin_edges[1]:.2f}'
        x_labels[-1] = f'<={bin_edges[-1]:.2f}'
        ax2.set_xticklabels(x_labels)
        ax2.set_ylabel('Anzahl Übergänge im Bin', fontsize='x-large')
        plt.tight_layout()

        plt.savefig(file_name)
        if show:
            plt.show()
        plt.close()
    return files


def visualize(clustering_method, root_dir, show_title, show):
    for use_log_norm in [False]:  # [True, False]
        vis_save_dir = f'{root_dir}/transition-graph-per-genre{"-log" if use_log_norm else ""}'
        report_file_name = f'{vis_save_dir}.html'
        os.makedirs(vis_save_dir, exist_ok=True)

        all_vis_files = []
        for dataset in ['fma_small', 'gtzan']:
            trained_model_dir = f'D:/devel/trained_models/vq_vae_{dataset}_train'
            codebook = codes.load_model_codebook(trained_model_dir)
            code_ordering = codes.get_optimal_codebook_ordering(codebook, clustering_method)

            ds = codes.get_codes_dataset("training", dataset_name=dataset, model_name=f'{dataset}_train') \
                .map(codes.get_parse_codes_example_map_fn()) \
                .map(codes.get_calc_code_transitions_map_fn()) \
                .map(codes.get_calc_frequencies_map_fn(normalize=True))

            tracks_path = f"d:/devel/datasets/{dataset}/metadata/tracks.csv"
            tracks = FmaTracks(tracks_path) if dataset == 'fma_small' else GtzanTracks(tracks_path)
            transitions_by_genre, frequencies_by_genre = defaultdict(list), defaultdict(list)

            for example in ds:
                genre = tracks.genres[example['genre_idx']]
                transitions_by_genre[genre].append(example['transitions'])
                frequencies_by_genre[genre].append(example['code_frequencies'])

            transitions_by_genre = {g: tf.concat(t, axis=0) for g, t in transitions_by_genre.items()}
            frequencies_by_genre = {g: tf.reduce_mean(f, axis=0) for g, f in frequencies_by_genre.items()}

            for genre in tracks.genres:
                files = draw_graphs(transitions_by_genre[genre],
                                    frequencies_by_genre[genre],
                                    code_ordering,
                                    dataset, genre,
                                    vis_save_dir, use_log_norm, show_title, show)
                all_vis_files += files

        with Report(report_file_name, f"Code Übergangsgraphen (Clusteringmethode={clustering_method}"
                                      f"{', log scale)' if use_log_norm else ')'}") as report:
            [report.add_img_row(file) for file in all_vis_files]


if __name__ == '__main__':
    visualize("average", f'D:/devel/reports/visualizations-average', show_title=True, show=True)
