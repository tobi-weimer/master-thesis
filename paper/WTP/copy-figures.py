from shutil import copy

src_dir = 'D:/devel/reports'
trgt_dir = 'D:/devel/PythonProjects/wtp-music-embedding/paper/figs'

copy_items = {
    "/visualizations-average-no-title/clustering/clustering-fma_small.png": "/clustering_average",
    "/visualizations-average-no-title/transition-graph-per-genre/graph-fma_small-pop-cut-off-20.png": "/clustering_average",
    "/visualizations-average-no-title/transition-heatmap/heatmap-all-fma_small-pop.png": "/clustering_average",
    "/visualizations-average-no-title/histogram/histogram-fma_small.png": "/clustering_average",
    "/visualizations-None-no-title/transition-heatmap/heatmap-all-fma_small-pop.png": "/clustering_none",
}

for src_file, trgt_sub_dir in copy_items.items():
    copy(src_dir + src_file, trgt_dir + trgt_sub_dir, )
