\section{Forschungsstand}\label{sec:forschungsstand}

\subsection{DDSP}\label{subsec:DDSP}
Der DDSP-Ansatz von Google AI befasst sich mit der Synthese von Audio mit Hilfe von Synthesizerkomponenten als Teil eines erlernbaren Modells. Begleitend zum Paper wurde ein Python-Paket zum Konstruieren entsprechender Modelle veröffentlicht.
Ein zentrales Feature des Pakets ist der Transfer von Audiodaten von einem Solo-Instrument auf ein anderes, zum Beispiel von einstimmigem Gesang zu Violine oder von Violine zu Flöte~\cite{GoogleDDSP2020}.

Das DDSP-Modell ist als Autoencoder konstruiert, dessen Encoder-Teil für uns interessant ist.
Wie in Abbildung~\ref{fig:ddsp_autoencoder} zu sehen ist, wird ein Audiosignal zuerst auf eine latente Repräsentation abgebildet. Hierzu gehört neben der $F_0$-Grundfrequenz und der Loudness des Audiosignals auch das Encoding $Z$. Dieses zeitvariante, latente Encoding enthält die übrigen, nicht in $F_0$ und Loudness enthalten Informationen. Wie Engel et al. feststellen, handelt es sich hierbei in erster Linie um die Klangfarbe~\cite{GoogleDDSP2020}.

Beim Decoder-Teil von DDSP gab es einige Herausforderungen, die Audio-Generierung an neuronale Netze stellt.
Audio besteht oft aus vielen übereinander gelegten Frequenzen, die unterschiedliche Phasenlängen haben.
Dies ist bei der Generierung und Verkettung von (sehr) kurzen Audiosamples (wenige Millisekunden) ein Problem.
Um längere Audiosamples (mehrere hundert Millisekunden bis Sekunden) zu generieren werden größere Netze benötigt, die mehr Daten zum Training benötigen.
Zudem sind diese Netze oft inkompatibel zu Audio-Vergleichsmethoden, die für einen Loss benötigt werden.
Bei der Nutzung von Synthesizern zur Audiogenerierung wird oft Expertenwissen genutzt, um Synthesizer-Parameter händisch einzustellen. Somit ist hier unüberwachtes, automatisiertes Lernen nicht vollständig möglich.
All diese Herausforderungen haben Engel et. al. bei DDSP überwunden~\cite{GoogleDDSP2020}.
%Wir müssen noch mal unseren Zitierstil vereinheitlichen.%

\begin{figure}
	\centerline{\framebox{
			\includegraphics[width=\columnwidth]{figs/ddsp_autoencoder_by_Google.png}}}
	\caption{Schematische Darstellung des DDSP Autoencoders (Quelle: Engel et al.~\cite{GoogleDDSP2020}).}
	\label{fig:ddsp_autoencoder}
\end{figure}

\subsection{VQ-VAE}

\begin{figure}
	\centerline{\framebox{
			\includegraphics[width=\columnwidth]{figs/VQ-VAE.png}}}
	\caption{Schematische Darstellung eines VQ-VAE (Quelle: nach S. Yadav~\cite{vq-vae-blog}).}
	\label{fig:vq-vae}
\end{figure}

Ein VQ-VAE ist ein Autoencoder, bei dem die Anzahl der Vektoren zur Datenrepräsentation zwischen Encoder- und Decoder-Teil begrenzt wird.
Damit wird eine bessere Generalisierung und Stabilität bei Variationen erreicht.
Daraus folgt eine höhere Kompression der Daten, wobei Verluste und Ungenauigkeiten bei den Details zu einem gewissen Grad in Kauf genommen werden.

Zentraler Baustein eines VQ-VAEs ist der Quantisierungslayer zwischen Encoder und Decoder, der den sich frei im latenten Raum bewegenden Encoder-Output auf eine begrenzte Menge an Prototypvektoren im gleichen Raum abbildet~\cite{vq-vae}.
Dazu wird für den Output-Vektor des Encoders die Distanz zu jedem Vektor im Codebook berechnet (siehe Gleichung~\ref{eq:vq-vae-nn}) und der am Nächsten liegende Vektor wird als Code genutzt.
Dabei ist $q(z|x)$ die a-posteriori Wahrscheinlichkeitsverteilung über die Codebook-Vektoren.

\begin{equation}\label{eq:vq-vae-nn}
q(z = k|x) = \begin{cases}
	1 & \quad \text{für } k=\argmin _j\parallel z_e (x) - e_j\parallel _2 \\
	0 & \quad \text{andernfalls}
\end{cases}
\end{equation}
mit $z_e(x)=$ Output-Vektor des Encoders \\
und $e_j=$ Codebook-Vektor $j$.\\

Der Output des Quantisierungslayers, und damit auch der Eingabe-Vektor für den Decoder, ist somit $e_k$ gemäß Gleichung~\ref{eq_vq-vae-ql}.

\begin{equation}\label{eq_vq-vae-ql}
z_q (x) = e_k , \quad \text{ mit } k=\argmin _j \parallel z_e (x) - e_j\parallel _2
\end{equation}

Um den Quantisierungslayer in der Trainingsphase zu berücksichtigen, wird der Loss um zwei Terme erweitert:
einen Codebook-Loss (siehe Gleichung~\ref{eq:codebook-loss}) und einen Commitment Loss (siehe Gleichung~\ref{eq:commitment-loss}).
Beide Terme sind auch Teil von unserem Gesamt-Loss (siehe Kapitel~\ref{sec:methodik}, Gleichung~\ref{eq:ddsp-vq-vae:loss}).

\begin{equation}\label{eq:codebook-loss}
\parallel \sg [z_e (x) ] - e \parallel ^2 _2
\end{equation}
\begin{equation}\label{eq:commitment-loss}
\parallel z_e (x) - \sg [e] \parallel ^2 _2
\end{equation}
beide mit $\sg =$ Stop Gradient Operator.\\
\\
(Gleichungen~\ref{eq:vq-vae-nn},~\ref{eq_vq-vae-ql},~\ref{eq:codebook-loss} und~\ref{eq:commitment-loss} aus~\cite{vq-vae})

\subsection{Grundlagen Audio-Signalverarbeitung}

Viele Verfahren zur Featureerkennung bei der Musikklassifikation haben eine mehrjährige Vorgeschichte im Bereich der Spracherkennung~\cite{genrePredIsmir2010}.
Dazu gehören häufig die Unterteilung des Audios in Phoneme, kleinste sinnvolle Abschnitte, wie zum Beispiel das \glqq B\grqq\ von Badezimmer.

Oft werden die Mel Frequency Cepstral Coefficients (MFCCs) genutzt, um das Audio kompakt darzustellen.
Das Mel ist eine Möglichkeit, die wahrgenommene Tonhöhe zu beschreiben.
Mit dem Cepstrum können periodische Strukturen in Frequenzspektren analysiert werden~\cite{KlassifikationMuster}.
Damit lassen sich die ,,Filter\textquotedblright\ (bei Sprache z.B. Mund und Zunge, bei Instrumenten klingende und resonierende Elemente) beschreiben, die einem Ton seinen spezifischen Klang geben oder auch ein \glqq A\grqq\ von einem \glqq O\grqq\ bei gleicher Tonhöhe unterscheiden.
Für unser Baseline-Modell nutzen wir diese MFCC-Features, die wir von Librosa aus dem Audio errechnen lassen (siehe~\ref{sec:genrePrediction}).



\subsection{Genre-Bestimmung}

Die Bestimmung des Genres eines Musikstücks ist eine schon länger thematisierte und immer wieder bearbeitete Aufgabe für MIR-Anwendungen.
Eine Anwendung mit guten Ergebnissen haben J. Bergstra, M. Mandel und D. Eck in \glqq Scalabe Genre and Tag Prediction with Spectral Covariance\grqq~\cite{genrePredIsmir2010} veröffentlicht.
Sie arbeiten in zwei Phasen: erst werden auf Audio-Frame-Ebene (Audioschnipsel von 20 - 50 Millisekunden Länge) die bereits bekannten MFCC-Features extrahiert.
Anschließend werden auf Segmentebene (3 - 10 Sekunden) die MFCC-Features der darin enthaltenen Audioframes statistisch ausgewertet.
Dabei enthält jedes Segment 512, 1024 oder 2048 Frames, abhängig von der Segmentlänge und der Sampling-Rate des Audios.
Alternativ zu den MFCC-Features extrahieren sie auf Frame-Ebene Features aus einem logarithmisch skalierten Mel-Frequenz-Spektrogramm.
Sie vergleichen die Trefferquoten zur Bestimmung dreier Audio-Tags in ihrer Arbeit, darunter das Genre-Tag, welches auch wir bestimmen.
Deswegen geben wir später die Performance-Werte aus~\cite{genrePredIsmir2010} zum Vergleich an.
