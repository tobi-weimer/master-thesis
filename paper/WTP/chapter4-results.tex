\section{Experimente \& Resultate}\label{sec:resultate}
Für unsere Implementierungen und Experimente stützen wir uns auf TensorFlow\footnote{https://www.tensorflow.org/}, ein weit verbreitetes Python-Paket zum Entwicklen und Trainieren von Machine Learning Modellen.
Wir wählen TensorFlow vor allem deshalb, weil es das Rahmenwerk ist, auf dessen Basis das ursprüngliche DDSP-Modell implementiert wurde.
Neben diversen weiteren gebräuchlichen Python-Paketen verwenden wir zudem Librosa\footnote{https://librosa.org/doc/latest/index.html} bei der Konstruktion eines Baseline-Modells.
Librosa ist ein gebräuchliches Python-Paket zur Analyse von Audio-Daten.
Neben dem zentralen Core-Modul ist für uns vor allem das Spektral-Feature-Modul interessant für die Audio-Analyse.

Unser DDSP-VQ-VAE-Modell trainieren wir separat auf zwei Datasets: zum einen auf dem Free Music Archive (FMA)~\cite{fma16}, speziell auf dem \textit{small} Subset (\textit{FMA\_small}).
Dieses umfasst 8.000 Tracks einer Länge von 30 Sekunden, die ausbalanciert auf 8 Genres verteilt sind.
Zum anderen trainieren wir unser Modell auf dem \textit{GTZAN}-Dataset~\cite{gtzan02, gtzanDataset}, das 1.000 Tracks der Länge 30 Sekunden umfasst, welche sich ausbalanciert auf 10 Genres verteilen.
Für das \textit{FMA\_small}-Dataset ordnen die Autoren alle Tracks in einen Training/Validation/Test-Split von 80/10/10\% ein, den wir übernehmen.
Für GTZAN existieren hierzu keine Vorgaben der Originalautoren.
Analog zum FMA\_small-Split wählen wir deshalb die ersten 80\% der Tracks als unser Traingsset, die nächsten 10\% als Validierungsset und die letzten 10\% als Testset.

Unser DDSP-VQ-VAE-Modell trainieren wir jeweils ausschließlich auf dem Trainingsset, indem wir versuchen, uns einer optimalen Rekonstruktion der Tracks anzunähern.
Das trainierte DDSP-VQ-VAE-Modell ist für uns nur Mittel zum Zweck.
Eigentlich sind wir am zeitvarianten, latenten Encoding $Z^*$ zu jedem Track interessiert, das wir erzeugen, indem wir nur den Encoder-Teil des Modells auf die Tracks anwenden.
Die Komponenten $F_0$ und Loudness ignorieren wir für unsere Experimente.
Den Decoder-Teil verwenden wir nicht.
Als Ergebnis erhalten wir ein neues Dataset, das nun für jeden Track das zeitvariante, latente Encoding $Z^*$ enthält.

Beim Generieren des Encodings schieben wir ein Fenster der Länge 4 Sekunden (von den DDSP-Autoren verwendete Standardeinstellung) ohne Überlappung über den Track.
Für jedes Fenster erhalten wir 1.000  Codevektoren mit 16 Komponenten, was durch die Architektur des Modells vorgegeben ist.
Durch die Länge der Tracks von 30 Sekunden erhalten wir sieben Fenster für jeden Track, wodurch wir die ersten 28 Sekunden Trackaudio abdecken und somit  $7 * 1.000$ Codevektoren erhalten.
Um unsere Ergebnisse nicht zu verfälschen, verwerfen wir die letzten zwei Sekunden Trackaudio, da wir sie zum Berechnen eines achten Fensters mit zwei Sekunden Stille padden müssten.
Die erhaltenen Codevektoren stacken wir, sodass jeder Track durch eine Matrix mit $7.000*16$ Einträgen repräsentiert wird.

Die Größe des Codebooks im Quantisierungslayer wählen wir für unsere Experimente mit 64.
Das bedeutet, dass jeder Codevektor aus einer Menge von 64 Codevektoren stammen muss.
Im Gegensatz zum Standard DDSP-Modell, in dem die Komponenten der Codevektoren ein beliebige reelle Zahl annehmen können, bekommen unsere Codevektoren damit einen kategorischen Charakter.
In vielen unserer Experimente nutzen wir diese Eigenschaft aus, indem wir jeden Codevektor auf seinen Index im Codebook reduzieren und ihn als Code bezeichnen.
In diesem Fall wird jeder Track durch einen 7.000-dimensionalen Vektor repräsentiert, in dem jede Komponente einen Code aus dem Codebook referenziert.
Bei der Generierung der Codes-Datasets aus den Audio-Datasets behalten wir den ursprünglichen Training/Validation/Test-Split bei.

\subsection{Genreerkennung}\label{sec:genrePrediction}                                                        

\begin{figure*}
	\framebox{\includegraphics[width=\linewidth]{figs/Genre-Classifier.png}}
	\caption{Schematische Darstellung des Genre-Klassifizierers, \textit{oben} werden die $Z^*$-Codes unseres DDSP-VQ-VAE-Encoders genutzt, \textit{unten} werden die Features durch Librosa aus dem Audio extrahiert.}
	\label{fig:Genre-Classifier}
\end{figure*}

Unter der Annahme, dass das latente Encoding $Z^*$ im wesentlichen die Klangfarbenfeatures der Tracks einfängt, und da Bergstra et al. in~\cite{genrePredIsmir2010} gezeigt haben, dass sich Klangfarbenfeatures erfolgreich für die Genreerkennung einsetzen lassen, vermuten wir, dass das latente Encoding hierfür ebenfalls verwendet werden kann.

Für die Genreerkennung bauen wir ein Multi Layer Perceptron (MLP) als Klassifizierer (Schema in Abbildung~\ref{fig:Genre-Classifier} \textit{oben}).
Da wir unseren Klassifizierer möglichst einfach halten wollen und ein schnelles Training möglich sein soll, entscheiden wir uns gegen die Implementierung eines Recurrent Neural Networks (RNN) oder eines Convolutional Neural Networks (CNN) und ignorieren so die zeitliche Abfolge der Codes.
Stattdessen kondensieren wir die 7.000 zeitabhängigen Codes pro Track in einen Frequenzvektor, der nur noch die Häufigkeit des Auftretens der Codes im Track widerspiegelt.
Die Größe des verwendeten Codebooks diktiert dabei, dass der Frequenzvektor 64 Komponenten haben muss.

Den 64-dimensionalen Frequenzvektor normalisieren wir und verwenden ihn als Input für unser MLP. Das MLP besteht aus drei Layern (jeder Layer bestehend aus Drop\-out, Dense und Batch Normalization) %SIC!%
mit jeweils 64, 32 und 16 Neuronen.
Für die Dropout-Wahrscheinlichkeit wählen wir einen Wert von 0,2. Als Aktivierungsfunktion benutzen wir die Rectified Linear Unit (ReLu)~\cite{relu}. Den Kopf des MLPs bildet ein Softmax-Layer mit so vielen Neuronen, wie es Klassen (d.h. Genres) im Dataset gibt – 8 für FMA\_small, 10 für GTZAN. Als Loss-Funktion wählen wir die  Sparse Categorical Crossentropy und trainieren das Netz mit dem Adam Optimizer~\cite{kingma2017adam} mit einer Lernrate von 0,001.
Wir wählen eine hohe Anzahl von Epochen (1.000) und verwenden gleichzeitig Early Stopping mit einer Patience von 30, um das Training auf Basis des Validierungssets zu beenden, wenn unser Modell beginnt zu overfitten.
Je nach Dataset wird die beste Genauigkeit auf dem Validierungsset im Mittel nach $\approx 121$ Epochen (FMA\_small) bzw. $\approx 116$ Epochen {GTZAN) erreicht. Auf Grund der geringen Größe des Modells dauert das Training pro Modell und Dataset nur einige Minuten.

Wir trainieren unseren Klassifizierer jeweils auf dem Trainingsplit des Code-Datasets, das wir mit dem DDSP-VQ-VAE-Encoder erstellt haben. Das Validierungsset verwenden wir, um das beschriebene Early Stopping zu implementieren. Nach dem Training führen wir die Evaluierung auf dem Testset durch.
Eine Evaluierung unseres Klassifizierers via k-Fold Cross Validation ist unter praktischen Gesichtspunkten zu aufwändig, da wir hier für jeden Fold unser DDSP-VQ-VAE-Modell neu trainieren müssten, was auf einer Geforce RTX 2080 TI ca. drei Tage dauert.

Als Vergleich implementieren wir ein Baseline-Model auf Basis von Features, die wir mit Librosa extrahieren (vgl.~\cite{gr_librosa}).
Diese Features sind a) chroma\_stft, rms, spectral\_centroid, spectral\_bandwidth, spectral\_rolloff, zero\_crossing\_rate und b) 20 MFCCs.
Diese 26 Features nutzen wir als Input für einen Klassifizierer, der dem oben beschriebenen bis auf die Anzahl der Inputneuronen exakt gleicht (siehe Abbildung~\ref{fig:Genre-Classifier} \textit{unten}).

Zusätzlich testen wir auch, welche Genauigkeit unser Klassifizierer erreichen kann, wenn wir ihn auf Codes trainieren, die mit dem DDSP-VQ-VAE-Modell extrahiert wurden, das auf dem jeweils anderen Datenset trainiert wurde.
Das heißt, wir extrahieren die Codes für das \textit{FMA\_small}-Dataset zunächst mit dem DDSP-VQ-VAE-Modell, das auch auf dem \textit{FMA\_small}-Dataset trainiert wurde, außerdem aber auch mit dem Modell, das auf dem \textit{GTZAN}-Dataset trainiert wurde.

Tabelle~\ref{tab:accuracies_ddsp} fasst unsere Ergebnisse zusammen.
Es ist klar zu erkennen, dass das FMA\_small-Dataset bezogen auf die Genreerkennung das anspruchsvollere Dataset ist.
Je nach Modell sind die Ergebnisse für GTZAN um ca. 15\% bzw. 21\% besser, obwohl bei GTZAN sogar zwei Genres mehr existieren als bei FMA\_small.
Diese Ergebnisse sind vermutlich mit dem zumindest perzeptuellen Qualitätsunterschied zwischen GTZAN und FMA\_small zu erklären.
Letzteres Dataset umfasst teilweise Tracks, die nur entfernt als Musik kategorisiert werden können.

Es zeigt sich auch, dass der DDSP-VQ-VAE basierte Klassifizierer das Librosa basierte Baseline-Modell mit einer Verbesserung um ca. 5\% bzw. 11\% deutlich übertrifft.
Dabei scheint es nicht ausschlaggebend zu sein, dass das DDSP-VQ-VAE-Modell auch auf dem Dataset trainiert wurde, aus dem es die Codes extrahiert.
Selbst wenn es zuvor auf dem anderen Dataset trainiert wurde, sind die Genauigkeiten bei der Genreerkennung sehr ähnlich.
Wir schließen daraus, dass die extrahierten Codes relativ stabile und gut generalisierende Features sind.

\begin{table}
	\centering
	\begin{tabular}{l|r} 
		\hline
		\multicolumn{1}{c|}{Dataset} & \multicolumn{1}{c}{Accuracy in \%}  \\ 
		\hline\hline
		\multicolumn{2}{c}{Features: DDSP-VQ-VAE \textit{FMA\_mall} }      \\ 
		\hline
		FMA\_mall           & 41,6                               \\
		GTZAN                        & 61,6                               \\ 
		\hline\hline
		\multicolumn{2}{c}{Features: DDSP-VQ-VAE \textit{GTZAN} }          \\ 
		\hline
		FMA\_mall           & 42,2                               \\
		GTZAN                        & 63,7                               \\ 
		\hline\hline
		\multicolumn{2}{c}{Features: Librosa (Baseline)}                              \\ 
		\hline
		\textit{FMA\_mall}           & 36,7                               \\
		GTZAN                        & 52,3                               \\
		\hline
	\end{tabular}
	\caption{\label{tab:accuracies_ddsp}Vorhersagegenauigkeit bei Nuzung der DDSP-VQ-VAE-Features und Vergleich zu dem Baseline Model.}
\end{table}

In unseren Experimenten haben wir lediglich das latente Encoding $Z^*$ verwendet.
Die Informationen, die in den vom Encoder produzierten $F_0$- und Loudness-Features enthalten sind, könnten vermutlich dazu beitragen, die Genauigkeit unseres Klassifizierers weiter zu erhöhen.
Da diese Features aber zeitvariant sind und wir sie in unserem MLP nicht ohne weiteres verarbeiten können, verwenden wir sie in unseren Experimenten nicht.
Stattdessen verwenden wir zusätzlich zu unseren Codes die mit Librosa extrahierten Features a):  chroma\_stft, rms, spectral\_centroid, spectral\_bandwidth, spectral\_rolloff, zero\_crossing\_rate.
Wir haben die Hoffnung, dass diese Features unsere Codes ergänzen können.
Für dieses Experiment erzeugen wir die Codes für beide Datasets jeweils mit dem DDSP-VQ-VAE-Modell, das auch auf dem entsprechenden Dataset trainiert wurde.
Die Ergebnisse dieses Experiments sind in Tabelle~\ref{tab:accuracies_ddsp_librosa} dargestellt.
Auf dem FMA\_small-Dataset erreichen wir eine Verbesserung gegenüber dem nur auf den Codes trainierten Klassifizierer von 0,9 Prozentpunkten.
Auf dem GTZAN-Dataset fällt die Verbesserung mit ca.
6,9 Prozentpunkten deutlich stärker aus.

\begin{table}
	\centering
	\begin{tabular}{l|r} 
		\hline
		\multicolumn{1}{c|}{Dataset} & \multicolumn{1}{c}{Accuracy in \%}  \\ 
		\hline\hline
		\multicolumn{2}{c}{Features: DDSP-VQ-VAE + Librosa}                \\ 
		\hline
		\textit{FMA\_mall}           & 43,1                               \\
		GTZAN                        & 70,6                              
	\end{tabular}
	\caption{\label{tab:accuracies_ddsp_librosa}Vorhersagegenauigkeit bei kombinierter Nutzung der DDSP-VQ-VAE und Librosa Features.}
\end{table}
Abschließend ist festzustellen, dass unsere Ergebnisse deutlich hinter den von Bergstra et al. in~\cite{genrePredIsmir2010} veröffentlichten zurückbleiben. Für ihre verschiedenen Modelle geben die Autoren dort Genauigkeiten zwischen 70\% und 81\% auf dem GTZAN-Dataset an.
Grundsätzlich zeigt sich jedoch die Nützlichkeit der extrahierten Features für die Genreerkennung.

\subsection{Visualisierungen}
Wir erstellen verschiedene Visualisierungen, um das latente Encoding $Z^*$ sichtbar zu machen.
Wir verwenden hierzu Histogramme, um die durchschnittliche Auftretenshäufigkeit der Codes pro Track für alle Genres in den beiden untersuchten Datasets abzubilden.
Außerdem stellen wir die Codeübergänge in den Tracks dar, indem wir sie in Übergangsmatrizen und Graphen visualisieren.

In unseren initialen Experimenten stellen sich die Übergangsmatrizen und Graphen recht ungeordnet dar, da wir die Codes in den Visualisierungen nach ihrem Index im Codebook anordnen.
Wir nehmen deshalb zunächst ein hierarchisches Clustering der Codes vor und bestimmen auf dieser Basis eine optimale Ordnung der Codes, die ihre Ähnlichkeit berücksichtigt.

Abbildung~\ref{fig:clustering_none/heatmap-all-fma_small-pop} zeigt exemplarisch eine Übergangsmatrix, bevor eine Sortierung der Codes auf Basis eines Clusterings vorgenommen wurde. Im Vergleich dazu zeigt Abbildung~\ref{fig:clustering_average/heatmap-all-fma_small-pop} auf Seite~\pageref{fig:clustering_average/heatmap-all-fma_small-pop} dieselbe Übergangsmatrix nach Sortierung der Codes. Der Effekt des Clusterings ist deutlich zu erkennen.

\begin{figure}
	\includegraphics[width=\linewidth]{figs/clustering_none/heatmap-all-fma_small-pop.png}
	\caption{Übergangsmatrix für das Genre Pop aus dem FMA\_small-Dataset - kein Clustering der Codes. Bis auf die Hauptdiagonale ist wenig Struktur zu erkennen. }
	\label{fig:clustering_none/heatmap-all-fma_small-pop}
\end{figure}


\subsubsection{Clustering}
Wir nehmen ein hierarchisch-agglomeratives Clustering (HAC) der Codes vor.
Als Basis hierfür dienen uns die 16-dimensionalen Codevektoren, die hinter jedem Code stehen.
Als Ähnlichkeitsmaß in diesem Vektorraum wählen wir den euklidischen Abstand.
Eine wesentlich Stellschraube beim HAC ist die Methode, nach der die Distanzen zwischen Clustern bestimmt werden.
Wir untersuchen zunächst die vier Methoden Single Linkage, Complete Linkage, Average Linkage und die Ward Methode~\cite{clusteringMethods09, clusteringWard63}.
Experimentell stellen wir fest, dass Single Linkage zu den schlechtesten Visualisierungen führt.
Die Übergangsmatrizen und Graphen ähneln stark denen ohne Clustering.
Besonders bei den Graphen fällt auf, dass bei der Single Linkage Methode vermehrt Kanten entstehen, die durch die Mitte des Kreises laufen.
Bei den anderen drei Methoden verlaufen die Kanten häufiger an den Rändern des Kreises, d.h. zwischen Knoten, die auf dem Kreis näher beieinander liegen, weil sie sich laut dem HAC ähnlicher sind.
Auch bei den Übergangsmatrizen sind die Ergebnisse mit den drei anderen Clusteringmethoden perzeptuell besser.

Welche der verbliebenen drei Clusteringmethoden (Average-Linkage, Complete-Linkage und die Ward Methode) am besten ist, lässt sich visuell schwer entscheiden.
Hier muss festgehalten werden, dass wir ein Clustering von 64 Datenpunkten in einem 16-dimensionalen Raum vornehmen.
Das Bilden von Clustern in einem so spärlich bevölkerten Raum ist grundsätzlich schwierig.
Verglichen mit Single- und Complete-Linkage, die jeweils dazu tendieren, Ketten bzw.
kompakte Cluster zu finden, machen Average-Linkage und die Ward Methode weniger Annahmen über die Struktur der Cluster. Dies sehen wir im vorliegenden spärlichen Raum als Vorteil, da wir lieber keine Annahmen treffen wollen als die falschen.
Wir entscheiden uns daher für den Einsatz von Average-Linkage – auch deshalb, weil es im Vergleich zur Ward Methode der simplere Ansatz ist und wir durch die gesteigerte Komplexität bei der Ward Methode keinen gesteigerte Qualität der Visualisierung erhalten.

Das Clustering für das auf Basis des FMA\_small-Datasets gelernte Codebook wird in Abbildung~\ref{fig:clustering_average/clustering-fma_small} dargestellt.
Die Sortierung der Codes in der Darstellung folgt bereits der beim Clustering berechneten optimalen Ordnung der Codes.

\begin{figure}
	\includegraphics[width=\linewidth]{figs/clustering_average/clustering-fma_small.png}
	\caption{Hierarchisches Clustering der Codes des FMA\_small-Datasets. }
	\label{fig:clustering_average/clustering-fma_small}
\end{figure}

\subsubsection{Histogramme}
Wir berechnen pro Dataset und pro Genre im Dataset die Frequenz der Codes und stellen diese in Histogrammen dar.
Jeder Balken im Histogramm repräsentiert die Häufigkeit, mit der der zugehörige Code im Mittel pro Track vorkommt.
Abbildung~\ref{fig:clustering_average/histogram-fma_small} zeigt das Histogramm für das FMA\_small-Dataset.
Auffällig ist, dass gewisse Codes deutlich häufiger auftreten als andere und keine uniforme Verteilung vorliegt.
Dies deutet darauf hin, dass Töne mit gewissen Klangfarben-Features häufiger auftreten als andere, wobei zu vermuten steht, dass der Encoder sehr ähnliche Klangfarben auf einen Code abbildet, während er für sehr unterschiedliche Klangfarben jeweils einen eigenen Code wählt.
Durch die Begrenzung des Codebooks auf 64 Codes ist der Encoder diesbezüglich mit einer Knappheit an Codes und dem folgenden Dilemma konfrontiert: tritt eine Klangfarbe nur sehr selten auf, dann ergibt es ggf. Sinn hierfür keinen Code zu opfern, sondern die Klangfarbe auf einen anderen Code abzubilden, wodurch punktuell eine große Abweichung in der Rekonstruktion entsteht.
Tritt eine Klangfarbe häufig auf, lässt sich die Rekonstruktion verbessern, indem die Klangfarbe unterteilt und auf zwei verschiedene Codes abgebildet wird.
Zwar entstünde bei der Abbildung auf nur einen Code keine große Abweichung, sie würde aber häufig auftreten und damit in Summe den entsprechenden Einfluss auf den Loss ausüben.
Der Encoder muss während des Training somit eine möglichst ökonomische Abbildung der Klangfarben auf die Codes erlernen.

\begin{figure}
	\includegraphics[width=\linewidth]{figs/clustering_average/histogram-fma_small.png}
	\caption{Mittlere Auftretenshäufigkeit der Codes pro Track im FMA\_small-Dataset.}
	\label{fig:clustering_average/histogram-fma_small}
\end{figure}


\subsubsection{Übergangsmatrizen}
Da das latente Encoding $Z^*$ zeitvariant ist, bietet sich eine Untersuchung der Codeübergänge an.
Weil jeder Track durch 7.000 Codes repräsentiert wird, gibt es entsprechend 6.999 Übergänge von einem Code in den nächsten: $s(t) \rightarrow s(t+1)$.
Wir sprechen an dieser Stelle auch dann von Codeübergängen, wenn der Code  $s(t)$ derselbe ist wie  $s(t+1)$.
Für das Genre Pop im FMA\_small-Dataset stellt die Übergangsmatrix in Abbildung~\ref{fig:clustering_average/heatmap-all-fma_small-pop} die Anzahl der  Codeübergänge von dem Ausgangszustand $s(t)$ auf der X-Achse  in den Folgezustand $s(t+1)$ auf der Y-Achse dar.
Die Anzahl der Codeübergänge mitteln wir dabei über alle Tracks im Genre und bilden sie auf eine logarithmische Farbskala ab.

\begin{figure}
	\includegraphics[width=\linewidth]{figs/clustering_average/heatmap-all-fma_small-pop.png}
	\caption{Übergangsmatrix für das Genre Pop aus dem FMA\_small-Dataset - Average-Linkage Clustering der Codes. Der Effekt des Clusterings ist deutlich zu erkennen.}
	\label{fig:clustering_average/heatmap-all-fma_small-pop}
\end{figure}

\begin{figure*}[t!]
	\includegraphics[width=\linewidth]{figs/clustering_average/graph-fma_small-pop-cut-off-20.png}
	\caption{Graph der Codeübergänge für das Genre Pop im Dataset FMA\_small - Average-Linkage Clustering der Codes. Der Graph (links) stellt die Übergänge vom Code $s_i$ auf den zeitlich nachfolgenden Code $s_j$ mit der Kante $s_i \rightarrow s_j$ dar. Je häufiger ein Codeübergänge dabei im Mittel pro Track auftritt, desto stärker gesättigt/dunkler ist die zugehörige Kante. Zur Einordnung zeigt das Histogramm (rechts) die Häufigkeitsverteilung über die Codeübergänge. Am linken Balken (,,<0.25\textquotedblright) ist zu erkennen, dass ein signifikanter Prozentsatz der Codeübergänge im Mittel weniger als 0,25 mal pro Track vorkommt. Diese sehr zahlreichen aber wenig signifikanten Codeübergänge stellen wir im Graphen nicht dar, um die Grafik nicht zu überfrachten.}
	\label{fig:clustering_average/graph-fma_small-pop-cut-off-20}
\end{figure*}

Durch die Betrachtung der Hauptdiagonale der Übergangsmatrix wird deutlich, dass es eine starke Tendenz gibt, dass ein Code über zwei aufeinanderfolgende Zeitschritte gehalten wird.
Zudem wird deutlich, dass Codes, für die das HAC eine größere Ähnlichkeit errechnet hat, tendenziell öfter ineinander übergehen als Codes, die nach dieser Metrik weiter voneinander entfernt sind.
Dies erscheint logisch, da zeitlich direkt aufeinanderfolgende Töne in der Musik in Bezug auf ihre Klangfarbe auch oft fließend ineinander übergehen, und kann somit als Hinweis auf die Effektivität des Clusterings  verstanden werden.
Das Modell scheint zumindest in einem gewissen Rahmen gelernt zu haben, ähnliche Klangfarben auch auf ähnliche Codevektoren im latenten Raum abzubilden.

\subsubsection{Graphen}
Zusätzlich zu den Übergangsmatrizen erstellen wir Graphen, die die Codeübergänge zeigen.
Wir ordnen hierzu die Codes kreisförmig als Knoten des Graphen an, wobei wir sie nach der optimalen Ordnung sortieren, die wir mittels Clustering ermittelt haben.
Die Codeübergänge stellen wir mit Kanten von einem Knoten zum anderen dar.
Die Häufigkeit der Codeübergänge bilden wir dabei linear auf die Farbe der Kanten ab.
Übergänge von einem Code in denselben Code stellen wir nicht dar.
Stattdessen bilden wir die Frequenz der Codes auf die Fläche der Knoten ab.
Je häufiger ein Code vorkommt, desto größer ist der Knoten.
Durch die Übergangsmatrizen wissen wir, dass Codes besonders oft in sich selbst übergehen und dadurch jeweils ein beträchtlicher Teil der Größe eines Knotens durch solche „Selbstübergänge“ ausgemacht wird.

Wir entscheiden uns außerdem, die Richtung der Codeübergänge nicht darzustellen.
Die hierzu genutzten Pfeilspitzen sind durch die hohe Anzahl von Knoten und Kanten visuell nicht mehr zu interpretieren.
Weiterhin stellen wir nicht alle Codeübergänge dar, weil ein Großteil der Codeübergänge nur sehr selten vorkommt und die zugehörigen Kanten die  Interpretierbarkeit der Graphen stark stören, ohne einen nennenswerten Informationsgehalt zu kommunizieren.
Wir dünnen deshalb die Menge der Codeübergänge aus, indem wir die seltensten Codeübergänge weglassen.
Wir bestimmen diese, indem wir die Codeübergänge nach der Häufigkeit ihres Auftretens in ein Histogramm mit 20 Bins einordnen und dann alle Codeübergänge ignorieren, die in den Bin mit den geringsten Häufigkeiten fallen.

Abbildung~\ref{fig:clustering_average/graph-fma_small-pop-cut-off-20} zeigt den Graphen für das Pop-Genre im FMA\_small-Dataset, sowie das Histogramm mit der Einordnung der Codeübergänge nach ihrer Häufigkeit.
Mit Blick auf das Histogramm erkennt man, dass der überwiegende Teil der Codeübergänge sehr selten auftritt, nämlich durchschnittlich weniger als 0,25 Mal pro Track.
Diese vielen, eher unbedeutenden Codeübergänge stellen wir im Graphen nicht dar.
Dadurch wird sichtbar, dass ein Großteil der Codeübergänge an den Rändern des Kreises verläuft, den der Graph beschreibt.
Zudem sehen wir viele Gruppen von Kanten, die annähernd parallel zueinander verlaufen.
Beide Effekte führen wir auf das vorgenommene Clustering und die Sortierung der Codes zurück.
Eine Darstellung des gleichen Graphen ohne Clustering und Sortierung der Codes befindet sich im Online-Supplement.


\subsection{MIR}
Für ein letztes Experiment starten wir mit den Frequenzvektoren pro Track, die wir schon im Abschnitt~\ref{sec:genrePrediction} verwendet haben, und nutzen sie als Basis für ein sehr einfaches, interaktives MIR-System.
Das System erlaubt als Einstiegspunkt die Auswahl eines beliebigen Tracks.
Auf Basis der Frequenzvektoren berechnen wir für diesen Track die Distanzen zu allen übrigen Tracks im Dataset, wobei wir den Euklidischen Abstand als Distanzmaß wählen.
Die fünf ähnlichsten Tracks zeigen wir dem Benutzer als Ergebnis an.
Für einige ausgewählte Tracks stellen wir die Ergebnisse des Systems im Online-Supplement zur Verfügung.

Für manche Tracks sind die Ergebnisse überraschend gut, für andere hingegen eher dürftig.
Generell lässt sich aber eine Tendenz erkennen, dass das System Tracks finden kann, in denen ähnliche Klänge vorkommen und die sich eben in dieser Hinsicht ähnlich sind.
Da wir nur die Frequenzvektoren verwenden, hat das System keine Möglichkeit, Features wie z.B. das Tempo der Musik einzubeziehen. Dadurch kann es vorkommen, dass es zu einem sehr langsamen Track auch einen sehr schnellen als ähnlich identifizieren kann, was den Benutzer irritieren wird.

Die Berechnung der Distanz von zwei Tracks auf Basis des euklidischen Abstands ihrer Frequenzvektoren lässt entscheidende Informationen außer Acht. Dazu zählt z.B. die Distanz der Codevektoren im latenten Raum, die zu den Codes gehören. Eine Distanzbestimmung, die diese Informationen einbeziehen würde, würde vermutlich besser Ergebnisse ermöglichen.
