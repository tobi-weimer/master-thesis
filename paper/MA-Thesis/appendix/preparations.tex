\chapter{Preparations and Shell commands}


\textit{Hint:} Within all code listings every occurrence of words written in \textsc{capitals} and surrounded by less-than and greater-than signs ( < and > ) has to be replaced according to the specific system setup and folder structure.
For example \texttt{<PYTHON\_VENVS\_DIR>} has to be replaced with the relative or full qualified path to enter the file system folder the python virtual environment shall be stored in, e.g. \texttt{/home/tweimer/python-venvs} .


\section{Setup an Environment for the Experiments}

To perform the experiments I did for this work I recommend using a computer running Linux and suited to train and run neural networks (e.g. by using a high performance NVidia graphics card), because it includes a lot of matrix calculations.
The system should have a current version of Python 3 installed as well.
The first task to complete is downloading the source code from my Github repository
\url{https://gitlab.com/tobi-weimer/master-thesis/-/tree/Master-Thesis-Final} .
After the download is finished a virtual Python environment can be set up, e.g. by using the shell commands listing \ref{lst:SetupPythonEnvironment}.


\begin{lstlisting}[style=Bash, caption={Commands to setup a virtual Python environment.}, label=lst:SetupPythonEnvironment]
cd <PYTHON_VENVS_DIR>
# set up venv
python3 -m venv <VENV_NAME>

# activate venv
source <VENV_NAME>/bin/activate

# update python package management tools
python -m pip install --upgrade pip setuptools

# install wheel for improved installation of
# all following python packages
python -m pip install wheel

# install project dependencies from requirements.txt:
python -m pip install \
-r <PROJECT_ROOT_DIR>/requirements.txt
\end{lstlisting}


\section{Train DDSP models on MeyleyDB}\label{sec:trainDDSP}

This section is all about training \ac{ddsp} models using the MedleyDB dataset published by \cite{medleyDB} and the extension MedleyDB 2.0 by \cite{medleyDB2}.
The MedleyDB dataset and extension are available online\footnotemark{} unter the termes of a Creative Commons BY-NC-SA license.
\footnotetext{MedleyDB website: \href{https://medleydb.weebly.com/}{https://medleydb.weebly.com/}, last visited on Feb. 28th, 2024} 

\textit{Hint:} It is recommended to run these commands in background and being able to logout while they run, because they tend to run for several hours and some even days.
To achieve this surround the command with a preceding "\texttt{nohup}" command and trailing "\texttt{\&}" (ampersand).
\texttt{nohup} runs a command in a mode with is immune to logouts, meaning the command runs on and finishes its tasks after the parent process terminates.
The user may even log out from the system.
Trailing a command with \texttt{\&} (ampersand) tells Linux shells to run in the command in background.
This way the shell is ready to use while the started program runs.


\subsection{Training preparations}

After downloading the MedleyDB audios\footnote{
	MedleyDB Audios Part 1: \href{https://doi.org/10.5281/zenodo.1649325}{https://doi.org/10.5281/zenodo.1649325},\\
	MedleyDB Audios Part 2: \href{https://doi.org/10.5281/zenodo.1715175}{https://doi.org/10.5281/zenodo.1715175}
}
and the metadata\footnote{
	MedleyDB Metadata: \href{https://github.com/marl/medleydb/tree/master/medleydb/data/Metadata}{https://github.com/marl/medleydb/tree/master/medleydb/data/Metadata}
}
(a folder full of \ac{YAML}-files on Github) some pre-processing has to be done before training the models.
I recommend securing a copy of the downloaded MedleyDB files, just in case they might be needed later.

Now the archives need to be unpacked and the two parts of MedleyDB audio files need to be joined together.
There should not occur any issues as their files do not contain duplicates.

As a model per instrument shall be trained the first step after downloading and backup creation is renaming the audio files - adding the instruments' name to the file names of single instrument audios - using the script call in listing \ref{lst:RenameSingleInstrumentAudioFiles}.
Now the audios are split into halves using the command in listing \ref{lst:PrepareWavFiles}.
The first half is used for model training, the second half for validation later.
After splitting \ac{ddsp} TFRecord files are calculated (see listing \ref{lst:CalculateTFRecords}), using groups of audio files depending on the flavors of the models to be trained.

\begin{lstlisting}[style=Bash, caption={Rename single instrument audio files to let the filenames contain the name of the instrument.}, label=lst:RenameSingleInstrumentAudioFiles]
python <PROJECT_ROOT_DIR>/MedleyDB_rename_by_yaml.py \
--yaml_files_folder=<PATH_TO_MEDLEY_DB_YAML_FILES_FOLDER> \
--files_root_dir=<PATH_TO_MEDLEY_DB_FILES> \
> <PATH_TO_LOG_DIR>/MedleyDB_rename_by_yaml.`date +"%Y-%m-%d_%H-%M"`.info.log \
2> <PATH_TO_LOG_DIR>/MedleyDB_rename_by_yaml.`date +"%Y-%m-%d_%H-%M"`.error.log
\end{lstlisting}

\begin{lstlisting}[style=Bash, caption={Split all audio files in the source folder including subfolders into halves.
The created halves are stored in the target folder, keeping the relative file structure.}, label=lst:PrepareWavFiles]
python <PROJECT_ROOT_DIR>/split_audio_to_halves.py \
<PATH_TO_SOURCE_FOLDER> \
>PATH_TO_TARGET_FOLDER> > \
<PATH_TO_LOG_DIR>/split_audio_to_halves.`date +"%Y-%m-%d_%H-%M"`.info.log \
2><PATH_TO_LOG_DIR>/split_audio_to_halves.`date +"%Y-%m-%d_%H-%M"`.error.log
\end{lstlisting}

\begin{lstlisting}[style=Bash, caption={Create TFRecords from audio files (being split into halves before).}, label=lst:CalculateTFRecords]
python <PROJECT_ROOT_DIR>/data/prepare_dataset_for_ddsp.py \
--wav_input_root_dir=<PATH_TO_SOURCE_FOLDER> \
--tfrecord_output_root_dir=<PATH_TO_TARGET_FOLDER> \
--sliding_window_hop_secs=1 \
> <PATH_TO_LOG_DIR>/prepare_dataset_MedleyDB.`date +"%Y-%m-%d_%H-%M"`.info.log \
2><PATH_TO_LOG_DIR>/prepare_dataset_MedleyDB.`date +"%Y-%m-%d_%H-%M"`.error.log
\end{lstlisting}


\subsection{Model training}

\subsubsection{Mix based song model}

\begin{lstlisting}[style=Bash, caption={Run Python script to train one DDSP model per song in MedleyDBs' dataset on the first half of the song itself.}, label=lst:SongModelsMixBased]
python <PROJECT_ROOT_DIR>ddsp_scripts/train_ae_per_song.py \
--tfrecords_root_dir <PATH_TO_TFRECORD_FILES> \
--tfrecords_path_and_name_pattern "/*MIX*half1.tfrecord-00000-of-00001"
--models_root_dir <PATH_TO_MODELS_BASE_SAVEDIR> \
--train_steps=30001 \
> <PATH_TO_LOG_DIR>/train_ae_per_song.`date +"%Y-%m-%d_%H-%M"`.info.log \
2><PATH_TO_LOG_DIR>/train_ae_per_song.`date +"%Y-%m-%d_%H-%M"`.error.log
\end{lstlisting}

I acknowledge all models created with the call in listing \ref{lst:SongModelsMixBased} have a very strong tendency to overfit.
This training does not aim to serve models for general application, but to validate whether DDSP can produce qualitative useful models for some specific polyphonic audio.


\subsubsection{Single instrument audios based song models}

\begin{lstlisting}[style=Bash, caption={Train one DDSP model per song on its single instrument audios.}, label=lst:SongModelsInstrumentsBased]
python <PROJECT_ROOT_DIR>ddsp_scripts/train_ae_per_song.py \
--tfrecords_root_dir <PATH_TO_TFRECORD_FILES> \
--tfrecords_path_and_name_pattern "/**/*STEM*half1.tfrecord-00000-of-00001"
--models_root_dir <PATH_TO_MODELS_BASE_SAVEDIR> \
--train_steps=30001 \
> <PATH_TO_LOG_DIR>/train_ae_per_song.`date +"%Y-%m-%d_%H-%M"`.info.log \
2><PATH_TO_LOG_DIR>/train_ae_per_song.`date +"%Y-%m-%d_%H-%M"`.error.log
\end{lstlisting}

The Python script in listing \ref{lst:SongModelsInstrumentsBased} is the same as in listing \ref{lst:SongModelsMixBased} but with different parameters.
The change causes the models to be trained on the single instrument audios per song (STEMs) instead of the songs mix audio.


\subsubsection{Instrument models}\label{sssec:instrumentModels}

\begin{lstlisting}[style=Bash, caption={This python script trains one DDSP model per instrument available in MedleyDBs' datasets.}, label=lst:TrainInstrumentModels]
python <PROJECT_ROOT_DIR>/ddsp_scripts/train_ae_per_instrument.py \
--tfrecords_root_dir <PATH_TO_TFRECORD_FILES> \
--create_tfrecords_if_necessary True \
--audio_root_dir <PATH_TO_WAV_FILES> \
--models_root_dir <PATH_TO_INSTRUMENT_MODELS_DIR> \
--yaml_files_folder <PATH_TO_YAMFILES> \
--train_steps 10001 \
> <PATH_TO_LOG_DIR>/train_ae_per_instrument.`date +"%Y-%m-%d_%H-%M"`.info.log \
2 > <PATH_TO_LOG_DIR>/train_ae_per_instrument.`date +"%Y-%m-%d_%H-%M"`.error.log
\end{lstlisting}

The quality of models trained with the call in listing \ref{lst:TrainInstrumentModels} vary widely because the amount of audios per instrument is not well distributed.
Some instruments are polyphonic and thus Googles' DDSP is not equipped to adjust to them.


\section{Schemes of shell commands used for the experiments}

\subsection{Encode WAV Files to CSVs}

Listing \ref{lst:EncodeSingleWavFile} provides a Python script call to encode a single \ac{WAV} file and save the result as a CSV file while \ref{lst:EncodeMultipleWavFiles} encodes all \ac{WAV} files in a folder and save the results in a CSV file per source audio file.
Depending on the amount of data to process the scripts may run for a while.
So as in  section \ref{sec:trainDDSP} it is recommended to surround the command to be executed with leading \texttt{nohup} command and trailing \texttt{\&} (ampersand).

\begin{lstlisting}[style=Bash, caption={
Encode a single \ac{WAV} file to a CSV file
}, label=lst:EncodeSingleWavFile]
python <PROJECT_ROOT_DIR>/ddsp_scripts/audio_worker.py \
--MODE encode \
[--OVERWRITE <True|False>] \
--MODEL_DIR <PATH_TO_INSTRUMENT_MODEL_DIR> \
--SOURCE_AUDIO_FILE <PATH_AND_NAME_OF_SOURCE_WAV_FILE> \
--TARGET_CSV_FILE <PATH_AND_NAME_OF_TARGET_CSV_FILE> \
[--CHECKPOINT_NAME <CHECKPOINT_INDEX_FILE>] \
[-v 1] \
> <PATH_TO_LOG_DIR>/audio_worker.encode.`date +"%Y-%m-%d_%H-%M"`.info.log \
2><PATH_TO_LOG_DIR>/audio_worker.encode.`date +"%Y-%m-%d_%H-%M"`.error.log
\end{lstlisting}

\begin{lstlisting}[style=Bash, caption={
Encode multiple \ac{WAV} files to \ac{CSV} files
}, label=lst:EncodeMultipleWavFiles]
python <PROJECT_ROOT_DIR>/ddsp_scripts/encode_multiple_audio_to_csv.py \
--MODEL_BASE_DIR <PATH_TO_INSTRUMENT_MODELS_DIR> \
--SOURCE_AUDIO_BASE_DIR <PATH_TO_WAV_FILES> \
--SOURCE_AUDIO_PATTERN=**/*STEM*.wav \
> <PATH_TO_LOG_DIR>/encode_multiple_audio_to_csv.`date +"%Y-%m-%d_%H-%M"`.info.log \
2><PATH_TO_LOG_DIR>/encode_multiple_audio_to_csv.`date +"%Y-%m-%d_%H-%M"`.error.log
\end{lstlisting}


\subsection{Decode CSV Files to WAVs}

Similar to the section above,
this section provides Python script calls to decode a single \ac{CSV} file to an audio \ac{WAV} file (listing \ref{lst:DecodeSingleWavFile})
and another script to decode all \acp{CSV} in a directory (listing \ref{lst:DecodeMultipleCsvFiles}).
Depending on the amount of files and data the decoding process may take a while.

\begin{lstlisting}[style=Bash, caption={
Decode a single \ac{CSV} file to a \ac{WAV} file
}, label=lst:DecodeSingleWavFile]
python <PROJECT_ROOT_DIR>/ddsp_scripts/audio_worker.py \
--MODE decode \
--OVERWRITE False \
--MODEL_DIR <PATH_AND_NAME_OF_INSTRUMENT_MODELS_DIR> \
--SOURCE_CSV_FILE <PATH_TO_SOUNRCE_CSV_FILE> \
--TARGET_AUDIO_FILE <PATH_AND_NAME_OF_TARGET_WAV_FILE> \
[--CHECKPOINT_NAME <CHECKPOINT_INDEX_FILE>] \
[-v 1] \
> <PATH_TO_LOG_DIR>/audio_worker.decode.`date +"%Y-%m-%d_%H-%M"`.info.log \
2><PATH_TO_LOG_DIR>/audio_worker.decode.`date +"%Y-%m-%d_%H-%M"`.error.log
\end{lstlisting}

\begin{lstlisting}[style=Bash, caption={
Decode multiple \ac{CSV} files to \ac{WAV} files
}, label=lst:DecodeMultipleCsvFiles]
python <PROJECT_ROOT_DIR>/ddsp_scripts/process_musescore_experiments.py \
--mode decode \
--model_dir <PATH_TO_INSTRUMENT_MODELS_DIR> \
--source_dir <PATH_TO_CSV_FILES> \
--target_dir <PATH_TO_WAV_FILES> \
[--with_parameters True] \
><PATH_TO_LOG_DIR>/process_musescore_experiments.`date +"%Y-%m-%d_%H-%M-%S"`.info.log 2><PATH_TO_LOG_DIR>/process_musescore_experiments.`date +"%Y-%m-%d_%H-%M-%S"`.error.log
\end{lstlisting}


\subsection{Create Graph Plots from CSVs}


The figures in chapter \ref{c:LatentRepresentationAnalysis} have been created from \acp{CSV} files using the command in listing \ref{lst:graphPlotFromCSVmulti}.
This command wrapps a batch of calls the script in listing \ref{lst:graphPlotFromCSVsingle},
which creates a graph plot from multiple provided \acp{CSV} and columns.

\begin{lstlisting}[style=Bash, caption={
Create many \ac{SVG} graph files from a collection of \ac{CSV} files
}, label=lst:graphPlotFromCSVmulti]
python <PROJECT_ROOT_DIR>/MuseScore/create_all_plots.py \
--FOLDER_PATH <PATH_TO_SOURCE_CSV_FOLDR> \
--FILE_SAVE_PATH <PATH_TO_SAVE_GENERATED_SVG_FILES> \
--GRAPH_MODES <COMMA_SEPARATED_LIST with values: instrument_groups,one_frequency_styles,two_frequency_styles,z > \
--FILE_NAME_PREFIX <TARGET_FILE_NAME_PREFIX> \
><PATH_TO_LOG_DIR>/create_all_plots.`date +"%Y-%m-%d_%H-%M-%S"`.info.log 2><PATH_TO_LOG_DIR>/create_all_plots.`date +"%Y-%m-%d_%H-%M-%S"`.error.log
\end{lstlisting}

\begin{lstlisting}[style=Bash, caption={
Create one \ac{SVG} graph file from specific columns found in multiple \ac{CSV} files.
}, label=lst:graphPlotFromCSVsingle]
python <PROJECT_ROOT_DIR>/MuseScore/create_plot_from_csvs.py \
--FOLDER_PATH <PATH_TO_SOURCE_CSV_FOLDR> \
--FILE_PATTERN <THE_PATTERN_FOR_THE_INCLUDED_CSV_FILES> \
--COLUMNS <COMMA_SEPARATED_LIST_OF_COLUMNS_TO_INCLUDE> \
--Y_AXIS_TITLE <THE_Y_AXIS_TITLE> \
--TITLE <THE_HEADING> \
><PATH_TO_LOG_DIR>/create_plot_from_csvs.`date +"%Y-%m-%d_%H-%M-%S"`.info.log 2><PATH_TO_LOG_DIR>/create_plot_from_csvs.`date +"%Y-%m-%d_%H-%M-%S"`.error.log
\end{lstlisting}


\subsection{Edit CSVs}

I created \ac{CSV} edit scripts in order to not have to edit all \ac{CSV} files manually for the experiments performed for this work.
The script \texttt{csv\_editor.py} (listing \ref{lst:editCsvFileSingle}) changes the values of one parameter in one latent representation.
It reads the representation from a source \ac{CSV} file and saves the changed representation to a target \ac{CSV} file.
Calling the edit functions has been automated in \texttt{process\_musescore\_experiments.py} (see listen \ref{lst:editCsvFileMulti}).


\begin{lstlisting}[style=Bash, caption={
Edit a latent representation parameter in the data from a \ac{CSV} file
}, label=lst:editCsvFileSingle]
python <PROJECT_ROOT_DIR>/ddsp_scripts/csv_editor.py \
--source_csv_file <PATH_AND_NAME_TO_SOURCE_CSV_FILE> \
--target_csv_file <PATH_AND_NAME_TO_TARGET_CSV_FILE> \
--parameter_name <THE_NAME_OF_THE_PARAMETER_TO_EDIT> \
--edit_function <THE_EDIT_FUNCTION_TO_USE> \
[--value <THE_VALUE_TO_SET] \
[--jump_length <THE_JUMP_LENGT_FOR_JUMP_EDITS] \
[--wave_length <THE_WAVE_LENTH_FOR_SINE_EDITS] \
[--low_value <THE_LOWER_VALUE_FOR_JUMP_AND_SINE_EDITS] \
[--high_value <THE_UPPER_VALUE_FOR_JUMP_AND_SINE_EDITS] \
><PATH_TO_LOG_DIR>/process_musescore_experiments.`date +"%Y-%m-%d_%H-%M-%S"`.info.log 2><PATH_TO_LOG_DIR>/process_musescore_experiments.`date +"%Y-%m-%d_%H-%M-%S"`.error.log
\end{lstlisting}

\begin{lstlisting}[style=Bash, caption={
Bulk-edit latent representation \acp{CSV}, applying all edit options to all parameters
}, label=lst:editCsvFileMulti]
python <PROJECT_ROOT_DIR>/ddsp_scripts/process_musescore_experiments.py \
--mode edit \
--source_dir <PATH_TO_CSV_FILES> \
--target_dir <PATH_TO_WAV_FILES> \
><PATH_TO_LOG_DIR>/process_musescore_experiments.`date +"%Y-%m-%d_%H-%M-%S"`.info.log 2><PATH_TO_LOG_DIR>/process_musescore_experiments.`date +"%Y-%m-%d_%H-%M-%S"`.error.log
\end{lstlisting}

