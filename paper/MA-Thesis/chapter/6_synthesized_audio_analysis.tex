\chapter{Analysis of DDSP-Synthesized Audio}
\label{c:AnalysisDDSP-SynthesizedAudio}


This chapter is about my findings made during hearing analysis of audios synthesized by \ac{ddsp}'s decoder from unmodified or edited latent representations.
It starts with section \ref{sec:method_synthesis} presenting the method applied.
Section \ref{sec:analysis:unchanged} is a summary of my findings made on audio from unendited latent representations.
Section \ref{sec:analysis:edited} summarizes the findings made on edited latent representations.
In section \ref{sec:discussion:audios} I summarize this chapter's findings and discuss hypotheses about their causes.


\section{Method and Experiments}
\label{sec:method_synthesis}


The source data for audio re-synthesis and editing \acp{CSV} are the audio files created immediately beforehand.
To analyze the performance of the \ac{ddsp}'s decoder and the quality of the synthesized audio, its decoder is called using the already known script \linebreak
\texttt{process\_musescore\_experiments.py}, this time with the parameters shown in listing \ref{lst:DecodeMuseScoreAudios}.

I have performed the analysis by listening to the original audio and the re-synthesized audio, noting any audible differences in short form into the appended table \ref{tab:comareMuseScore-re-synthesis}.
I have placed the emphasis on harmonics, timbre, loudness and pitches.

A part of this work is to explore how the \ac{ddsp} reacts to changes in the latent representation.
To achieve this I have written a script to edit one parameter in the latent representation's \ac{CSV} file at a time.
The new values for each timestep have been computed in the following edit modes:
\begin{itemize}
	\item Inverted Parameter Values:\\
	$\text{\textbf{new value}} = \text{minimum value} + \text{distance} - \text{old value}$\\
	with $\text{distance} = \text{maximum value} - \text{minimum value}$
	\item Maximum Value: set all values of a parameter to its maximum value.
	\item Minimum Value: set all values of a parameter to its minimum value.
	\item Jump back and forth between Minimum and Maximum values: set the first \texttt{jump\_length} values to min\_value, the next \texttt{jump\_length} values to max\_value, and repeat until all values are replaced. In my experiments \texttt{jump\_length} has been set to 250, which is equal to one second of audio.
	\item Sinusoid Function Between Minimum and Maximum:
	\begin{equation*}
		\text{\textbf{new value}} = 
		\frac{\text{distance}}{2} * \sin\left(\frac{2}{\text{wavelength}} * \pi * \text{timestep}\right)
	\end{equation*}
	with distance $=$ maximum value $-$ minimum value\\
	and wavelength denoting the length of a sinus period in timesteps, after which the values repeat.\\
	In my experiments the wavelength has been set to 250.
	\item Zero Values: set all values of a parameter to $0$ (zero)
	\item Delete Values: delete all values of a parameter.
\end{itemize}
The minima and maxima have been determined by domain knowledge and analysis:
\begin{itemize}
	\item The minimim of \texttt{f0\_hz} is $0$~\ac{Hz} and the maximum is $10,000$~\ac{Hz}.
	That range is close to the average upper limit of the hearing range for humans (see \cite{Rossing2007}, p. 747).
	\item \texttt{f0\_scaled} and \texttt{ld\_scaled} are both within the range of $\left[0.0 , 1.0\right]$ - this is the range \ac{ddsp} scales them to.
	\item The value range of \texttt{Z} has been found to be $\left[-3.0 , 3.0\right]$ during my analysis.
\end{itemize}

After editing the \acp{CSV} I have used them as inputs for the \ac{ddsp} decoder to synthesize audios (see script in listing \ref{lst:DecodeMuseScoreAudios}).
I have analyzed the generated audios in the same way as for the unedited re-synthesized audios at the beginning of this chapter.

\begin{lstlisting}[style=Bash, caption={Decode \ac{CSV} files (from MuseScore) to \acp{WAV}}, label=lst:DecodeMuseScoreAudios]
nohup python process_musescore_experiments.py \
--mode decode \
--model_dir <MODELS_ROOT_DIR>/MedleyDB_instrument_models \
--source_dir <PROJECT_ROOT_DIR>/MuseScore/CSVs \
--target_dir <PROJECT_ROOT_DIR>/MuseScore/re-synthesized \
><PATH_TO_LOG_DIR>/process_musescore_experiments.`date +"%Y-%m-%d_%H-%M"`.info.log \
2><PATH_TO_LOG_DIR>/process_musescore_experiments.`date +"%Y-%m-%d_%H-%M"`.error.log
\end{lstlisting}


\section{Analysis of Audios Re-Synthesized from Unchanged Latent Representations}
\label{sec:analysis:unchanged}


The first and most recognizable difference between original audios and their \ac{ddsp} re-synthesized counterparts is a reduced general loudness level,
at least in all experiments I have performed for this thesis.
There are many tools that can adjust the overall loudness level of audios, so I do not consider this a shortcoming.
A number of these tools do not manipulate the audio itself to make the adjustment but rather add an annotation for the playback program instead.
This is a very beneficial approach when handling lossy audio file formats.


\subsection{General Remarks on Instruments}
\label{ssec:general_remarks_on_instruments}


First of all I want to make a point about the overall performance of three of the five instruments that I used in this experiment.
Audios synthesized by the female singer model and the male singer model exhibit a lot of noise.
The violin model is at the other end of the overall performance scale, as it performs best in all experiments.

A reason for the noise produced by the singer models may be a difference between the experiment's data and the training data.
In the training data the singers have lyrics to sing along with the melody,
but the experiment's data do not have any lyrics.

Unfortunately I have no reasonable hypothesis for why the solo violin outperforms the solo flute and the viola section.


\subsection{Performance on Monophonic Audio}


\subsubsection{Reaction to Loudness Changes}


Changes in the loudness of an original audio were hardly transferred to the re-synthesized audio.
In fact only the viola section model responded to the change, even though the adjustment was less then expected.
All other models almost completely failed to change the loudness level.


\subsubsection{Reaction to Frequency Changes}

All models reacted to frequency changes and so all these were transferred to the re-synthesized audios.
The changes in the timbre quality are very interesting:
for higher frequencies I found all models to perform better.
The models for flute and violin balely exhibit a change in timbre quality.
But in audios synthesized by the female singer model and male singer model frequency changes lead to a remarkable difference in timbre quality.


\subsection{Performance on Polyphonic Audio}


For each instrument and audio change combination, I had an audio with two equal instruments played simultaneously, producing sound at two frequencies being a Third (for definition see section \ref{ssec:intervals} and table \ref{tab:intervals}) apart from each other.

In the re-synthesized audio I can hear only one instrument that switches between two frequencies at a high rate.
This maps the data series of \texttt{f0\_hz} and \texttt{f0\_scaled} for those audios, as described  in section \ref{ssec:two_frequencies}.
To me the audios sound as if one instrument tries to imitate two.

Apart from this finding the performance of the "polyphonic" audios \linebreak
 matches with the performance of the monophonic audios as far as possible with respect to the polyphonic sound flaw.


\section{Analysis of Audios Synthesized from Edited Latent Representations}
\label{sec:analysis:edited}


In the following sections I describe the effects of editing the latent representation \ac{CSV} in certain ways before using it as input for the \ac{ddsp} decoder to create audio.
All ways of editing have been explained in section \ref{sec:method_synthesis}.


\subsubsection{Exclusion Notices}

Due to the bad performance of the female singer model and male singer models I have not evaluated how they behave when the latent representation gets edited.
For the same reason I have excluded all latent representations from audios containing two instruments playing two different tones in parallel.

Editing both f0-parameters, \texttt{f0\_hz} and \texttt{f0\_scaled}, with respect to their relation (as written in section \ref{ssec:f0_relation}) is excluded here, too.
This approach only combined the negative effects that occured on editing one of them,
as noted in the detail table \ref{tab:comareMuseScore-re-synthesis} in the appendix.


\subsection{Inverted Parameter Values}

The effect of inverting a latent parameter's value depends completely on which latent parameter is inverted.

Editing the fundamental frequency \textbf{\texttt{f0\_hz}} has not led to any good result.
My expectation was that such editing would lead to audios with very high harmonic elements.
But all audios have lost most or all of their harmonics and timbre; the dominating sounds are pitches and noise.

For parameter \textbf{\texttt{f0\_scaled}} the results are much more acceptable, but again different to my expectations mentioned above:
I cannot report noticeable change patterns in the harmonics, instead they became more noisy or, in some cases, did not change recognizably compared to the unedited re-synthesized audio.

Inverting the \textbf{\texttt{ld\_scaled}} parameter does result in the expected change in the synthesized audio: loud audio sections should become rather quiet, and quiet sections should become loud.
The \texttt{ld\_scaled} values for all audios have been in the louder half of the value range.
By inverting this parameter's values all audios have become quieter, and the loudest sections have become completely silent.

The effects of editing parameter \textbf{\texttt{Z}} are mostly destructive to harmonics and timbre.
Some audios have (parts of) their harmonics remaining, but most audios lost their harmonics and timbre and are left with pitches and noise only.


\subsection{Maximum Parameter Values}

Setting the parameter \textbf{\texttt{f0\_hz}} to the high value of $10,000$~\ac{Hz} results in a loss of harmonics and timbre for all audios.
On the other hand, setting the parameter \textbf{\texttt{f0\_scaled}} to its maximum value $1.0$ has hardly any effect on the synthesized audio at all.

The audios synthesized from \ac{CSV} files with parameter \textbf{\texttt{ld\_scaled}} set to value $1.0$ or parameter \textbf{\texttt{Z}} set to value $3.0$ do not differ from re-synthesized audios without editing.


\subsection{Minimum Parameter Values}

The result for all synthesized audios where the latent parameter \textbf{\texttt{f0\_hz}} has been set to $0.0$~\ac{Hz} is noisy sound with pitches but without harmonics or timbre, as was the case when setting the value to $10,000$~\ac{Hz} or inverting it.

Setting the parameter \textbf{\texttt{f0\_scaled}} to $0.0$ has mostly led to silence in the audios, except for a few miliseconds in some audios that contain noise at a very perceptible loudness level.
Losing the harmonics and keeping the noise meets my expectations for the two cases under consideration.
But completely losing sound in a majority of the audios, including the noise with \texttt{f0\_scaled} set to $0.0$, is contrary to my expectations.

When I set the parameter \textbf{\texttt{ld\_scaled}} to $0.0$ the results differ much more from my expectations.
As this parameter contains loudness information I expected complete silence in all audios.
The audios generated by the flute model meet this expectation, containing no sounds at all,
but the audios for the viola model and the violin model both contain significant noise.

The result of setting the parameter \textbf{\texttt{Z}} to $-3.0$ has a completely different effect:
it adds some noise to the audios (less than expected) and increases the loudness, including the loudness of the instrument.
This second effect was not expected.


\subsection{Parameter Values Jumping between Minimum and Maximum}

As might be expected the result of the experiments above,
setting a parameter to series of maximum and minimum values alternately results in a mix of the findings above.
For parameter \textbf{\texttt{f0\_hz}} this leads to the audios consisting of noise and pitches without harmonics or timbre.
When the parameter \textbf{\texttt{f0\_scaled}} is edited,
the audios become mostly unaffected for the part with \texttt{f0\_scaled} $= 1.0$ (maximum) and mostly silent for the part with \texttt{f0\_scaled} $= 0.0$ (minimum).
Editing the parameter \textbf{\texttt{ld\_scaled}} leads to another mix of the results above:
only the flute model becomes silent with synthesizing when \texttt{ld\_scaled} is set to $ 0.0 $.
The other parts of these audios as well as all of the viola and violin audios have noise added to their sound whilst preserving harmonics and timbre.

There is one parameter where the results of this experiment are not just a mix of the maximum and minimum experiment: the parameter \textbf{\texttt{Z}}.
The way my editing tool works the \texttt{Z} parameter has got short sections of 15 timesteps (= 60 milliseconds) where all 16 dimensions are set to $3.0$,
alternating with equally short section where all 16 dimensions are set to $-3.0$.
Every time these two intervals change there is one timestep with some dimensions having value $3.0$ and the others having value $-3.0$.
As with constant maximum or minimum values noise has been added to the audio.
But far more striking for to the listener is a vibrato effect that appears in all audios where \texttt{Z} has been edited in this manner.


\subsection{Parameter Values from Sinusoid Function between Minimum and Maximum}

Setting the value series of parameter \textbf{\texttt{f0\_hz}} to the results of a sinusoid function leads to audios where the frequency goes up and down in a wavy form.
There are short silent parts in the audio when the sinusoidal function is close to a low or close to a high.

Editing the parameter \textbf{\texttt{f0\_scaled}} in the same way results in audios where the sound changes in a continuous wavy form between short parts of silence or less loudness and parts with synthesizer-like versions of the instrument's timbre with added noise.

For parameter \textbf{\texttt{ld\_scaled}} the results can be derived from the maximum and minimum experiments:
For the flute model the sound changes back and forth from silent parts to noisy flute parts.
Both the viola model and violin model react to changes of the \texttt{ld\_scaled} value with added noise.
All their audios have short parts containing harmonics and timbre with added noise,
interrupted by sections with noise only.

Editing the \textbf{\texttt{Z}} parameter using this sinusoidal function in the same way as in the parameter jumping section leads to very similar results:
all audios have added noise and a vibrato effect.
But in this case the vibrato effect is very fast.
It seems similar to what a stroboscope light looks like, applied to sound.


\subsection{Parameter Values set to Zero}

Setting the values of a parameter to zero ($0$) leads to silence or very silent sounds in most cases.
Applying this to parameter \textbf{\texttt{f0\_hz}} does not result in any synthesized audio being completely silent but rather very quiet and noisy.
The viola model is an exception:
its audios contain only noise with all harmonic elements and timbre removed,
yet they exhibit an average loudness level.

For parameter \textbf{\texttt{f0\_scaled}} the flute model and violin model produce $100\%$ silent "audio".
The viola model is an exception again, producing noise sounds only without any harmonics or timbre, interrupted by silent parts.

Editing parameter \textbf{\texttt{ld\_scaled}} in this way mostly repeats the result pattern from the minimum value experiment.
The flute model meets the expectation with $100\%$ silence,
whilst the viola model and violin model produce unexpected noise again.

Setting the values of parameter \textbf{\texttt{Z}} to zero leads to sounds that are very similar to the sounds re-synthesized from unedited latent representations.
Some audios even have less noise.


\subsection{Deleted Parameter Values}

If the values of any of the four parameters \textbf{\texttt{f0\_hz}}, \textbf{\texttt{f0\_scaled}}, \textbf{\texttt{ld\_scaled}} or \textbf{\texttt{Z}} have been deleted,
the audio files being synthesized from them are $100\%$ silent.
I have investigated how the parameter was read from the \ac{CSV}.
Every value that is not set in the \ac{CSV} is read as a \ac{nan}.
The audio synthesized from a latent representation with a parameter set to \ac{nan} also shows \ac{nan} values only in the debugger.
The mel-frequency-spectrograms of the resulting audio files also show no indication for any sound anywhere.


\section{Discussion}
\label{sec:discussion:audios}


\subsection{On Audios from Unchanged Latent Representations}


\subsubsection{Singer Model Performance}

In section \ref{ssec:general_remarks_on_instruments} I have shown that the female singer model and the male singer model perform with a lot of noise.
Listening to the training data it becomes very clear that the singers have lyrics to sing when they are singing in these audios.
The audios I used for the experiments in this work do not contain any lyrics.
This is the one most notable difference between the training data and the experimental data.
So I believe that the poor performance of these two models is due to this difference in the audios.
As I have not had time nor the audio data to investigate this finding, it might be investigated in a future study.


\subsubsection{Good Violin Performance}

The violin model I used in the re-synthesis experiments presented in this work is just one of a series of well performing violin models.
Even the models and audios the authors of \ac{ddsp} have presented in their paper, including the live demonstration and online appendix\footnotemark, feature a violin model and a flute model.
\footnotetext{
	\href{https://magenta.tensorflow.org/ddsp}{https://magenta.tensorflow.org/ddsp}, last visited on March 19th, 2024.
}
This leads me to believe that those two instruments are easy to synthesize for \ac{ddsp}.


\subsubsection{Frequency Changes}

All models I have used perform better on higher frequencies than on lower frequencies.
This becomes clear when the performance of an audio containing an increasing fundamental frequency is compared with an audio containing a decreasing fundamental frequency.
This finding surprises me because it is contrary to what the authors of \ac{ddsp} have reported:

\textquote[\cite{ddsp}, online appendix\footnotemark]{
	Here, we shift resynthesize audio from the solo violin model after transposing the fundamental frequency down an octave and outside the range of the training data.
	The audio remains coherent and resembles a related instrument such as a cello.
}
\footnotetext{
	\href{https://storage.googleapis.com/ddsp/index.html\#extrapolation}{https://storage.googleapis.com/ddsp/index.html\#extrapolation}, last visited on March 19th, 2024.
}


\subsubsection{Polyphonic Audio}

The audios re-synthesized from originals,
 where two equal instruments play different tones,
  sound as if only one instrument is playing and trying to imitate two instruments.
For instruments designed to play (mostly) monophonically like a flute, a viola or a violin this is a real challenge.
Even expert musicians would struggle with this task, and it does not match the capabilities for which \ac{ddsp} is designed.

It might be possible to enable \ac{ddsp} for two or maybe more tones from one instrument played in parallel.
This would require source code changes in the encoder that currently supports only one fundamental frequency per timestep as well as in the decoder, to make it accept and process more than one frequency input per timestep.


\subsection{On Audios from Edited Latent Representations}


\subsubsection{High Frequencies are Out of Range}

Investigations of the \acp{CSV} with inverted f0\_hz reveal that all values are higher than  $9,000$~\ac{Hz}.
As in music the ratios between two frequencies are the key factor when it comes to differences; $9,000$~\ac{Hz} is close to $10,000$~\ac{Hz},
the maximum fundamental frequency used in this work.
From section \ref{sec:method_synthesis} we remember that this is close to the average upper hearing limit of humans.
These frequencies are also more than one Octave out of range for all instruments tested in this work (compare \cite{Rossing2007}, p. 542, figure 15.4).

The findings made in the audios with sinusoidal \texttt{f0\_hz} design support a hypothesis stating that those high frequencies are out of the range of frequencies that \ac{ddsp} is able to synthesize.
All audios with sinusoid fundamental frequencies have one finding in common:
as the frequencies increase the audio turns silent at some point, until sound comes back in the decreasing part of the sine wave.
The fact that there is no transition between high frequency sound and silence - neither a fade-out nor bad quality audio - supports the idea of an upper limit in the \ac{ddsp}'s frequency range.


\subsubsection{Too Low Minimum Frequency}

When setting the values of the latent representation parameter \texttt{f0\_hz} to $0.0$ the synthesized audios lose their harmonic parts.
As I consider the input now, setting \texttt{f0\_hz} to that value makes no sense, as 0~\ac{Hz} can strictly speaking not be called a frequency.

In addition to the 0~\ac{Hz}-issue, humans can only hear sound frequencies of above appoximately $ 20 $~\ac{Hz}.
The sound system used - namely a typical notebook and headphones - also has a lesser capability as to what frequencies can be produced, which is likely located in the area of about $ 200$~\ac{Hz} at the lower end (\cite{Rossing2007}, p. 541).


\subsubsection{Vibrato Effect Caused by Parameter Z}
\label{sssec:vibratioZ}

When editing the latent parameter \texttt{Z} in the jumping paramter values experiment and in the sinusoidal values experiment,
I discovered that it caused a vibrato effect to the synthesized audio.
This effect might lead to a creative use of the \ac{ddsp},
especially for artists and producers who might add a vibrato effect to some recordings during post processing.

