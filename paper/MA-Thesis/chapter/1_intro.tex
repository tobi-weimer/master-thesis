\chapter{Introduction and Motivation}
\label{sec:intro}


\section{A Concise Overview of Audio Synthesis History}

Research on audio synthesis, the artificial production of sound, is ongoing for more than a century.
A foundation was laid with the invention of a working electromagnetic transducer in 1877 by A. G. Bell (\cite{Kahrs1997}).
Another well-known example of the early days is Cahill's Telharmonium which was granted a patent in the US in 1897 (\cite{Cahill1897}).
With the miniaturization of electronics in the 1960s synthesizers were developed, sold and used, paving the way for new music genres (\cite{intro2ddsp}).

In the late 20th century research focus shifted to digital methods, making use of digital signal processing and numerical methods (\cite{intro2ddsp}).
As deep learning became important in recent years, major technological options were created for audio synthesis.
The first example is the release of WaveNet Version 1 in 2016 (\cite{WaveNet2016}).
More methods were explored, WaveNet was improved and other classes of generative networks were published, e.g. GANSynth (\cite{GANsynth2019}).

As audio signals can be complex, high-dimensional and rich in information generating high-quality audio is still challenging.
Undesireable artifacts were found in audio produced by networks using upsampling (\cite{ArtifactsInAudioSynthesis}), while  networks using frame-based estimations suffer from audio containing multiple frequencies in parallel, and their sinusoidal signals not fitting into frames (\cite{GANsynth2019}).

In 2020 these flaws were overcome by \ac{ddsp} (\cite{ddsp}), but so far this is limited to monophonic audio.
It produces impressive results synthesizing solo singers and monophonic instruments such as violins and trumpets.
As music often involves polyphonic instruments and multiple audio sources in parallel there is need for improvement.


\section{Personal Story with DDSP}

I started my master studies to deep dive into the topics of computational and artificial intelligence.
I wanted to learn what is behind the scenes of artificial intelligence, as its importance increases.

I made my first experiences with \ac{ddsp} in 2020 during a team project, just after it was released by \cite{ddsp}.
My colleague and I checked if the latent representation of \ac{ddsp} contains information that might be useful for other applications.
As a proof of concept we implemented a genre classifier that uses \ac{ddsp}s latent representation as input.
Without any optimization our model performs with about 60\% accuracy based on the datasets \ac{fma} small (\cite{fma_dataset}) containing 8,000 tracks with balanced distribution over eight genres, and GTZAN\footnote{GTZAN Genre Collection. Was published online at \href{http://marsyas.info/downloads/datasets.html}{http://marsyas.info/downloads/datasets.html}. Last successful visit on Dec. 13th, 2020. Seems to be offline lately.}
containing 1,000 tracks evenly distributed over 10 genres.
Obviously an overall accuracy of about $ 60\% $ is not good enough for any productive use case.
So one dream of mine is to find a way to improve the accuracy of this \ac{ddsp} based genre classifier.

I would love to see a version of \ac{ddsp} being able to handle polyphonic audio, as most music is built with multiple sound sources playing in parallel.
This work contributes to this objective by analyzing the latent representation of tracks in \ac{ddsp}.
Understanding the meanings of latent parameters also allows for their use in creative ways.
Artists can already use \ac{ddsp} for timbre transfer of multiple sound sources at once as shown by \cite{musicStar} or create mashups of two or more songs as well as make other song modifications.


\section{Contribution}
\label{ssec:contribution}

The major question of this thesis is:
How does editing the latent representation of an audio made by a \ac{ddsp} encoder effect the audio synthesized by \ac{ddsp}'s decoder from that edited latent representation?

To be able to elaborate the answer to this question effectively,
two other questions have to be answered before in this thesis:
\begin{enumerate}
	\item Which parameters of the latent representation are relevant to the \ac{ddsp}'s decoder?
	By reading the \ac{ddsp}'s paper \cite{ddsp} expectations were to find three parameters in the latent representation:
	one representing $f_0$, one for the loudness, and Z for noise and timbre.
	But in preparation of this thesis I had found the latent representation to consist of seven parameters.

	\item How are the values inside determined? What are the value ranges of each of these parameters?
	How are they used in the \ac{ddsp}'s decoder?
	As deviations from the paper have been seen regarding the latent representation parameters it is curial to fully understand how the \ac{ddsp}'s encoder and decoder process the input data and determine the output.
	Without this knowledge it is neither possible to have well-based expectations,
	nor to explain why things happen the way they do.
\end{enumerate}

To answer these two upstream questions I have performed extensive search and debug sessions with \ac{ddsp}'s source code.
During this task I have gathered information about the algorithms and equations used by \ac{ddsp}.
I have determined which parameters are relevant for the \ac{ddsp}'s decoder and found correlation between some of them as well.
This information and findings have been documented in chapter \ref{c:Dataflow}.

Next I performed experiment with various input audio to find patterns related to the input in \ac{ddsp}'s latent representations. 
I have found correlations between some parameters in \ac{ddsp}'s latent representation by inspecting multiple of them made from various well-defined audios.
The behaviors of the latent representation's parameters are explained in chapter \ref{c:LatentRepresentationAnalysis}.

Finally, I focused on the major question of this thesis.
I have edited the parameters of multiple latent representations with respect to the two questions I have named just above.
In each trial I set one or two parameters to specific values to check the effects on the synthesized audio.
I have documented the observed changes in the audios in chapter \ref{c:AnalysisDDSP-SynthesizedAudio}.
I have added sections on performance issues and limitations in that chapter, too.

Based on my findings I have made some suggestions for future studies in chapter \ref{c:conclusion}.
One of them is a possible extension of \ac{ddsp}'s capabilities towards polyphonic processing.
Another documented idea is the creation of a software based on \ac{ddsp} that enables music producers and artists to use a vibrato effect that I have found for their creative work.\footnote{
Online-Supplement: \href{https://gitlab.com/tobi-weimer/master-thesis}{https://gitlab.com/tobi-weimer/master-thesis}\\
The tag "Master-Thesis-Final" is the last version related to this work:\\ \href{https://gitlab.com/tobi-weimer/master-thesis/-/tree/Master-Thesis-Final}{https://gitlab.com/tobi-weimer/master-thesis/-/tree/Master-Thesis-Final}
}










