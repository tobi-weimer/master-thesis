\chapter{Latent Representation Analysis}
\label{c:LatentRepresentationAnalysis}


In section \ref{sec:Method}, this chapter starts with a documentation of my approach on analyzing latent representations of audios made by \ac{ddsp}'s encoder.
Section \ref{sec:exploreLatentRepresentation} presents my findings made during analyses of latent representations saved to \acp{CSV} and their representations in graphs.
In section \ref{sec:discussion:latent_representation_analysis} I summarized and evaluated the findings.


\section{Method: Creating and Analyzing CSV Files}
\label{sec:Method}


To faciliate the analysis of \ac{ddsp}'s latent representation, I decided to perform it on self-synthesized and thereby controlled audio inputs.
I chose audios with 4.25 seconds playback time only, which results in 1,061 lines of encoded data in the \ac{CSV}, because \ac{ddsp} splits each second of audio into 250 timesteps\footnote{
	250 is the default frame rate in \ac{ddsp}, see the source code and paper by \cite{ddsp}.
}.
Four seconds of audio equal two four quarter bars on a sheet of music played at 120 beats per minute.
Each created audio fits into one of five patterns:
\begin{itemize}
	\item constant frequency and constant loudness,
	\item constant frequency and increasing loudness,
	\item constant frequency and decreasing loudness,
	\item increasing frequency and constant loudness, or
	\item decreasing frequency and constant loudness.
\end{itemize}

To get some more variation in the data I synthesized audios for each pattern played by five different instruments: a flute, a violin, a viola section, a female singer and a male singer.
To investigate how \ac{ddsp} handles two fundamental frequencies ($f_0$ and $f_1$) I synthesized a second audio for each pattern and instrument, with the same instrument playing the same melody, but the second instrument playing a Third (definition see chapter \ref{ssec:intervals}, table \ref{tab:intervals}) apart.
With all these variations and mutations I ended up synthesizing 50 different audio files for analysis.
All audio has been synthesized with the free and open source notation software MuseScore\footnote{
	Software available at: \href{https://musescore.org/en}{https://musescore.org/en}. Last visited on Jan. 23rd, 2024.\\
	Source code repositoy: \href{https://github.com/musescore/MuseScore}{https://github.com/musescore/MuseScore}. Last visited on Jan. 23rd, 2024.
} version 4.2.
The sheets of music are included in appendix \ref{sec:MuseScoreSheetsAndAudio}.


\subsection{Preprocessing of MuseScore Audio Files}

MuseScore adds three seconds of playback time to synthesized audio after the end of the notated music in order to have just enough time for all reverbs and effects to finish.
In this experiment no effects or reverbs are involved, so the extra seconds of audio are not needed and have been removed.
This can be done using the Linux shell program \texttt{sox} as shown in listing \ref{lst:soxShortenAudios}.

\begin{lstlisting}[style=Bash, caption={
Shorten SOURCE\_FILE by trimming away all audio except what is played for 4.25 seconds stating at playbacktime 0 seconds, and save the resulting audio in TARGET\_FILE.
}, label=lst:soxShortenAudios]
sox <SOURCE_FILE> <TARGET_FILE>  trim 0 4.25
\end{lstlisting}


\subsection{Dataset for Model Training}


The audio dataset used to train all \ac{ddsp} models for this work is the combination of MedleyDB from \cite{medleyDB} and its extension MedleyDB~2.0 from \cite{medleyDB2}.
Their data is presented as \texttt{\ac{WAV}}~files in a well-structured order: every song has its own folder with a \texttt{mix} file, a subfolder with \texttt{stem} files and another subfolder with \texttt{raw} files.

\texttt{Raw} files are the starting point of the recording production process: unprocessed mono tracks from a single microphone, only some noise has been removed.
These \texttt{raw} files are grouped together by audio source and enriched with all effects that apply to the selected source.
The output of this processing stage is saved in the \texttt{stem} files.
\texttt{Mix} files are produced by mixing all stems belonging to the selected track and mastering it if required. (\cite{medleyDB})

MedleyDB also provides metadata to each song. This is presented as a \texttt{\ac{YAML}} file per track which contains often used information like album and genre, other details such as composer, producer and a flag if bleeding occurs in a track's recordings.
On per-audio-source recordings bleeding is the term for any audio from another source which may be present in an audio source's recording, e.g. when multiple instruments are recorded in the same room simultaneously (\cite{medleyDB}).
A comprehensive list of all available files for the selected track is included in its \texttt{\ac{YAML}} file, too. For \texttt{raw} and \texttt{stem} files the audio source is named in the listing, e.g. the instrument.


\subsection{Model selection}


How to train equivalent models is documented in the appendix (chapter \ref{sssec:instrumentModels}).
\footnotetext{
	available online: \href{https://gitlab.com/tobi-weimer/master-thesis/-/blob/Master-Thesis-Final/MuseScore/process_musescore_experiments.py}{https://gitlab.com/tobi-weimer/master-thesis/-/blob/Master-Thesis-Final/MuseScore/process\_musescore\_experiments.py}
}
The models used for this analysis are cherry-picked models trained on MeyleyDB that have demonstrated good performance
(i.e. low loss and well-sounding reconstruction of a song)
in a comparison among all models trained on a single mix audio included in MeyleyDB \cite{medleyDB} or \cite{medleyDB2}.

Each of them was trained with 2,000 iterations on the audio data listed in table \ref{tab:modelsTrainingAudios}.
My approach to training \ac{ddsp} models is documented in the appendix, chapter \ref{sec:trainDDSP}.


\begin{table}[b]
	\centering
	\caption{The audios used for training the selected \ac{ddsp} models. "Solo" refers to exactly one person or instrument. The term "section" means a group.}
	\label{tab:modelsTrainingAudios}
	\begin{tabular}{ | c | c | c |}
		\hline
		\textbf{instrument} & \textbf{song} & \textbf{artist} \\
		\hline
		solo female singer & Country2 & MusicDelta \\
		\hline
		solo flute & An Evening With Oliver & Matthew Entwistle \\
		\hline
		solo male singer & Rock & MusicDelta \\
		\hline
		viola section & Vivaldi & MusicDelta \\
		\hline
		solo violin & An Evening With Oliver & Matthew Entwistle \\
		\hline
	\end{tabular}
\end{table}


\subsection{Creation of CSV Files and Graphs for Analysis}

To be able to examine a latent representation, it first has to be created from input data.
To do so, I have run the \ac{ddsp} encoder to get the latent representations of the audios.
The latent representations have been saved to \ac{CSV} files.
I automated this using the script \linebreak
\texttt{process\_musescore\_experiments.py}\footnotemark as documented in listing \ref{lst:EncodeMuseScoreAudios}. \linebreak
Whilst executing this script a few values may be cut off at the end of each parameter in order to make them fit exactly into frame- and hop-sizes.

\footnotetext{
	available online: \href{https://gitlab.com/tobi-weimer/master-thesis/-/blob/Master-Thesis-Final/MuseScore/process_musescore_experiments.py}{https://gitlab.com/tobi-weimer/master-thesis/-/blob/Master-Thesis-Final/MuseScore/process\_musescore\_experiments.py}
}



Looking into the \acp{CSV} I quickly realized that the values vary all the time.
Originally I had planned a numerical comparison, but on this data (1,061 values per column) that is possible for proficient knowledge in statisticians only.
Instead, I have created graphs from the \ac{CSV} data using the \texttt{create\_all\_plots.py} script as shown in listing \ref{lst:CreateGraphsFromCSVs} and compared them.
All graphs are available in the online appendix\footnote{
	Folder with plot graphs: \href{https://gitlab.com/tobi-weimer/master-thesis/-/tree/Master-Thesis-Final/MuseScore/graphs}{https://gitlab.com/tobi-weimer/master-thesis/-/tree/Master-Thesis-Final/MuseScore/graphs}
}.

\begin{lstlisting}[style=Bash, caption={
Encode \ac{WAV} files from MuseScore to \acp{CSV}.
}, label=lst:EncodeMuseScoreAudios]
nohup python process_musescore_experiments.py \
--mode encode \
--model_dir <MODELS_ROOT_DIR>/instrument_models \
--source_dir <PROJECT_ROOT_DIR>/MuseScore/re-synthesized \
--target_dir <PROJECT_ROOT_DIR>/MuseScore/re-synthesized_CSVs \
><PATH_TO_LOG_DIR>/process_musescore_experiments.`date +"%Y-%m-%d_%H-%M"`.info.log \
2><PATH_TO_LOG_DIR>/process_musescore_experiments.`date +"%Y-%m-%d_%H-%M"`.error.log
\end{lstlisting}

\begin{lstlisting}[style=Bash, caption={
Create graphs from CSV files.
}, label=lst:CreateGraphsFromCSVs]
nohup python <PROJECT_ROOT_DIR>/MuseScore/create_all_plots.py \
--FOLDER_PATH <PATH_TO_CSV_FILES> \
--FILE_SAVE_PATH <PATH_TO_STORE_SVG_FILES> \
> <PATH_TO_LOG_DIR>/create_all_plots.`date +"%Y-%m-%d_%H-%M"`.info.log \
2><PATH_TO_LOG_DIR>/create_all_plots.`date +"%Y-%m-%d_%H-%M"`.error.log
\end{lstlisting}


\section{Exploring the Latent Representation}
\label{sec:exploreLatentRepresentation}


\subsection{Relation between f0\_hz and f0\_scaled}
\label{ssec:f0_relation}

\begin{figure}[h]
	\caption{The frequency over time in \ac{Hz} (left) and scaled by \ac{ddsp} (right) for female and male singer, playing an increasing scale solo (monophonic) according to notation \ref{fig:notation_incr_freq_const_ld}. These graphs look alike for all other instruments.}
	\label{fig:incr_freq_const_ld_f0_both}
	\includesvg[height=8.5cm]{figures/latent_representation_graphs/increasing_frequency_constant_loudness_singer_f0_both}
\end{figure}

\begin{figure}
	\caption{The frequency in \ac{Hz} (left) and scaled by \ac{ddsp} (right) over time for female and male singer, playing a decreasing scale solo (monophonic) according to notation \ref{fig:notation_decr_freq_const_ld}. These graphs look alike for all other instruments.}
	\label{fig:derc_freq_const_ld_f0_hz}
	\includesvg[height=8cm]{figures/latent_representation_graphs/decreasing_frequency_constant_loudness_singer_f0_both}
\end{figure}

Based on the design of the encoder (see section \ref{ssec:encoder} and figure \ref{fig:ddsp_encoder_dataflow}) the parameters \texttt{f0\_hz} and \texttt{f0\_scaled} were expected to show similar behavior with respect to constant, increasing and decreasing sections.
Examination of the \ac{CSV} files and graphs drawn from the \acp{CSV}' data
(figures \ref{fig:incr_freq_const_ld_f0_both} and \ref{fig:derc_freq_const_ld_f0_hz})
indicates a continuative correlation, just as expected.

There is one noteable difference between \texttt{f0\_hz} and \texttt{f0\_scaled}:
The absolute difference for \texttt{f0\_hz} between two low frequencies that are one tone (see section \ref{ssec:intervals}, table \ref{tab:intervals}) apart from each other is smaller than the absolute difference between two high frequencies that are one Tone apart from each other.
In \texttt{f0\_scaled} these two differences exhibit almost the same absolute value.
The reason for this finding is the fact that in terms of music the difference between two notes is defined as ratios between the frequencies.
As the scaling from \texttt{f0\_hz} to \texttt{f0\_scaled} applies a logarithmic function to the frequency the expression of differences get changed from ratios to absolute values.


\subsection{Reaction of f0 Parameters to Frequency Changes}


The values and value changes of the two parameters \texttt{f0\_hz} and \texttt{f0\_scaled} correspond approximately to expectations.
While a tone is made the frequency deviates around the target frequency in the range of $ \pm 1 \% $.
The values of \texttt{f0\_hz} in the latent representations from all audio experiments are in the range $ \left[ 130.072 , 1,330.358 \right] $.
The corresponding values of \texttt{f0\_scaled} are in the range $ \left[ 0.377 , 0.694 \right] $.

Before evaluating the \ac{CSV} files I assumed every generated audio represents its notated music.
During inspection of the \texttt{f0\_hz} values this assumption has been proved to be true.
The frequency changes indicated in the notations of the source music are present in the \texttt{f0\_hz} value series as well.
Most changes are performed within a few timesteps.
There are some delays and shifts back and forth on the flute and female singer recordings, which are likely caused by the creation tool.
Example notations for the source audio are available in appendix \ref{sec:MuseScoreSheetsAndAudio}.


\subsection{Relation between loudness and ld\_scaled}


\begin{figure}[h]
	\caption{The loudness in \ac{dB} (left) and scaled by \ac{ddsp} (right) over time for solo violin playing an increasing scale solo (monophonic) according to notation \ref{fig:notation_const_freq_incr_ld}.}
	\label{fig:const_freq_incr_ld_loudness}
	\includesvg[height=7.5cm]{figures/latent_representation_graphs/constant_frequency_increasing_loudness_violin_loudness_both}
\end{figure}
\begin{figure}
	\caption{The loudness in \ac{dB} (left) and scaled by \ac{ddsp} (right) over time for solo violin playing a decreasing scale solo (monophonic) according to notation \ref{fig:notation_const_freq_decr_ld}.}
	\label{fig:const_freq_decr_ld_loudness}
	\includesvg[height=7.5cm]{figures/latent_representation_graphs/constant_frequency_decreasing_loudness_violin_loudness_both}
\end{figure}

Like \texttt{f0\_hz} and \texttt{f0\_scaled} the two parameters \texttt{loudness\_db} and \linebreak
\texttt{ld\_scaled} have a direct correlation, which is defined by equation \ref{eqn:ld-scaled} in chapter \ref{sssec:ld_scaled}.
The correlation is visible in the figures \ref{fig:const_freq_incr_ld_loudness} and \ref{fig:const_freq_decr_ld_loudness}.
Because the \ac{ddsp}'s decoder requires \texttt{ld\_scaled} as input, this section focuses on that parameter and leaves parameter \texttt{loudness\_db} out of consideration.

Between timestep 100 and 1,000 the majority of the values from \texttt{ld\_scaled} are in the range of $ \left[ 0.8 , 0.95 \right] $.
During that period the audios indicating higher loudness values are those with an annotation to play very loudly in the sheets of music ($fff$), so this is a reation to specifics of the audio.
Some of these audios and sections attain \texttt{ld\_scaled} values of more than 1.0.
At the very beginning (first 50 timesteps at maximum) and at the end (up to last 200 timesteps) a behavior of a fast fade-in and a slightly slower fade-out can be observed.
The values of \texttt{ld\_scaled} from all audio experiments are in the range $ \left[ 0.176 , 1.131 \right] $.


\subsection{Z}


\texttt{Z} is a 16-dimensional parameter.
The majority of \texttt{Z}-values  in all latent representations examined at any timestep distribute more or less evenly within the range of $ \left[ -2.0 , 2.0 \right] $, with few exceptions.
A significant proportion of all \acp{CSV} exhibit \texttt{Z}-values with higher absolute values in the first 100 timesteps, between the timesteps 400 to 600, and in the last 100 timesteps.
The average of all \texttt{Z} dimensions at any timestep are in the range of $ \left[ -0.4 , 0.4 \right] $ in most cases.
Unfortunately I could not identify any other patterns.

The total value range of parameter \texttt{Z} from all audio experiments are in the range $ \left[ -5.633 , 4.662 \right] $.
This is due to some outliers, 90\% of the values are within the range $ \left[ -3.0 , 3.0 \right] $.


\subsection{Two fundamental Frequencies in Parallel}
\label{ssec:two_frequencies}


\begin{figure}
	\caption{The frequency in \ac{Hz} over time for one singer or instrument trying to cover two frequencies in parallel.\\
	Top: female singer, middle: flute, bottom: solo violin}
	\label{fig:female_singer_f0_hz}
	\includesvg[height=6.6cm]{figures/latent_representation_graphs/female_singer_two_frequencies_f0_hz}

	\label{fig:flute_f0_hz}
	\includesvg[height=6.6cm]{figures/latent_representation_graphs/flute_two_frequencies_f0_hz}

	\label{fig:violin_f0_hz}
	\includesvg[height=6.6cm]{figures/latent_representation_graphs/violin_two_frequencies_f0_hz}
\end{figure}

\texttt{f0\_hz} and \texttt{f0\_scaled} from audios with two different fundamental frequencies played at the same time jump back and forth between them in an unpredictable manner (see figures \ref{fig:female_singer_f0_hz}, \ref{fig:flute_f0_hz} and \ref{fig:violin_f0_hz}).
This is a proof of what the \ac{ddsp} authors have written in their paper:
\ac{ddsp} was designed and built for monophonic audio.

As far as observed behavior is concerned and based on the \ac{ddsp}'s architecture,
there is potential for modifications to \ac{ddsp} to implement handling of multiple fundamental frequencies per timestep.
Source separation techniques (introduced in chapter \ref{sec:Source_separation}) may help to achieve this.


\section{Discussion}
\label{sec:discussion:latent_representation_analysis}


The analysis of the latent representation has not led to groundbreaking new discoveries, but proves well-reasoned assumptions.
The relation between \texttt{f0\_hz} and \texttt{f0\_scaled} was as expected based on the \ac{ddsp}'s encoder (see chapter \ref{ssec:encoder} and equation \ref{eqn:f0-scaled}).
Likewise the relation between the music notation that has been the base for the original audio and the frequency values found in the \acp{CSV} match the domain expectation.
The same is true for the relation between \texttt{loudness\_db} and \texttt{ld\_scaled}. 

I am disappointed by not having been able to identify helpful patterns in parameter \texttt{Z} values.
The expression of \texttt{Z} can be very different for two latent representations of the same audio that have been produced by two different \ac{ddsp} models.
Consequentially the effects of certain values of \texttt{Z} are different for every \ac{ddsp} model.

