\chapter{Background}
\label{c:state_of_the_art}


This chapter features an overview of the works and knowledge this thesis relies on.
Section \ref{sec:terms_acoustics} presents terms from the music domain that are used in this thesis.
Section \ref{sec:signal_processing} contains a brief introduction of the term signal and the Fourier transforms.
Especially the Fast Fourier Transform and Short-Time Fourier Transform are used heavily in digital audio processing.
For an extensive introduction to sound, acoustic, hearing and signal processing I recommend reading "Springer Handbook of Acoustics" by \cite{Rossing2007}.

In section \ref{sec:basics_deep_learning} a brief introduction to Deep Learning is presented.
It covers topics relevant for this thesis only.
To gain a more complete and deep understanding of this field I recommend the textbook "Deep Learning" by \cite{DeepLearningBook}\footnote{
	The Deep Learning textbook is available online for free at \href{https://www.deeplearningbook.org/}{https://www.deeplearningbook.org/} (last visited on March 22nd, 2024).
}.

Section \ref{sec:audio_processing_libraries} introduces the audio processing libraries used by \ac{ddsp} and in this thesis briefly.
Finally an introduction to \ac{ddsp} and its contribution to audio processing with Deep Learning technologies are presented in section \ref{ddsp}.


\section{Terms in Acoustics}
\label{sec:terms_acoustics}

Since \ac{ddsp} and this thesis deal with topics from acoustics and music, we have to use some terms from these fields of application.


\subsection{Octave}

An octave (oct) is a ratio between two frequencies $f_1$ and $f_2$ (in \ac{Hz}) which is defined as a ratio of $ 1~:~2 $ (see DIN~1320:1997-06 \cite[]{DIN1999} and \cite{Rossing2007}).
This means the ratio between $f_1 = 220 \text{~Hz}$ and $f_2 = 440 \text{~Hz}$ is called an octave, as well as the ratio between $f_2 = 440 \text{~Hz}$ and $f_3 = 880 \text{~Hz}$.
This is the reason why logarithmic scales and functions are very common in acoustics, audio and music.


\subsection{Cent}
\label{ssec:cent}

The term cent (\textcent, C or \ac{cent}) has two independend definitions.

According to the more common definition, cent is a relative unit used to describe the differences between two frequencies, with one cent representing the ratio $1~:~\sqrt[1200]{2} \equiv 1~:~2^{\frac{1}{1200}} \approx 1.0005777895 $ (see DIN~13320:1979-06 \cite{DIN1999}).
From the definitions of octaves and cents it follows that the relationship between them is defined as $1200 \text{~cent} = 1 \text{~oct}$.

According to the second definition cent is an absolute unit.
Absolute cents represent fixed frequencies.
In absolute cent terms, 1~Hz equals 600~cent.
According to the above ratio rule, the equivalent for 2~Hz is 1.200~cent, 4~Hz equals 2.400~cent etc. (see \cite{Dahlhaus2001}).
This is in contrast with the definition of $1.200 \text{ (relative) cent} = 1 \text{~oct}$,
therefore readers have to take care whether an author has used relative or absolute cent.

\ac{ddsp} uses absolute cent as well, but with a different definition. In the \ac{ddsp}'s source code we find the equation

\begin{equation}
	\text{frequenz in Hz} = 10 * 2 ^{ \frac{\text{cent}}{1.200} }
\end{equation}

From this equation we can derive the table \ref{tab:cents_ddsp}.

\begin{table}
	\centering
	\caption{Frequencies and absolute cents as used in \ac{ddsp}.}
	\label{tab:cents_ddsp}
	\begin{tabular}{ | c | c |}
		\hline
		\textbf{absolute cent} & \textbf{frequency in \ac{Hz}} \\
		\hline
		$ 0 $ & $ 10 $ \\
		\hline
		$ 1,200 $ & $ 20 $ \\
		\hline
		$ 2,400 $ & $ 40 $ \\
		\hline
		$ 3,600 $ & $ 80 $ \\
		\hline
		$ 4,800 $ & $ 160 $ \\
		\hline
		$ 12,000 $ & $ 10,240 $ \\
		\hline
	\end{tabular}
\end{table}



\subsection{Other Interval Names}
\label{ssec:intervals}

In music all commonly used intervals have specific names that are derived from Latin.
The interval names used in this work are listed in table \ref{tab:intervals}.

\begin{table}
	\centering
	\caption{Some names and ratios for intervals in music with equal-temprament, cited from \cite{Rossing2007}, p. 543, Table 15.1. I have added the examples.}
	\label{tab:intervals}
	\begin{tabular}{ | c | c | c | c |}
		\hline
		\textbf{interval name} & \textbf{ratio} & \textbf{difference in cent} & \textbf{example} \\
		\hline
		Octave & $ 1~:~2 $ & 1,200 & c''' $ \rightarrow $ c'''' \\
		\hline
		Major Third & $ 1~:~2^{\frac{4}{12}} $ & 400 & d $ \rightarrow $ f\# \\
		\hline
		Minor Third & $ 1~:~2^{\frac{3}{12}} $ & 300 & d $ \rightarrow $ f \\
		\hline
		Tone & $ 1~:~2^{\frac{2}{12}} $ & 200 & d $ \rightarrow $ e \\
		\hline
		Semione & $ 1~:~2^{\frac{1}{12}} $ & 100 & d $ \rightarrow $ d\# \\
		\hline
	\end{tabular}
\end{table}


\subsection{Musical Instrument Digital Interface (MIDI)}
\label{ssec:MIDI}

\textquote[\cite{Kientzle2002}, p. 273]{
	MIDI is a simple digital protocol and was rapidly incorporated into computer music applications.
	By connecting a computer to a keyboard through a MIDI connection, you get the musical equivalent of word processing.
	(\dots)
	In 1998, the \textit{Standard MIDI File Format} was adopted by the members of the \textit{MIDI Manufacturer's Association} (MMA; \href{www.midi.org}{www.midi.org})
}

In MIDI the pitch of a sound is encoded as an integer in the range of $[21,108]$.
The value range comprises 88 integers as with a normal piano keyboard.
The range of pitch also corresponds to that of a piano and extends from A0 (German A'') with 27.5~Hz at code 21 to C8 (German c''''') with 4,186.0~Hz at code 108.
The standard pitch for tuning instruments is A4 (German a') with 440~Hz at code 69. (\cite{Warstat2003})

According to \cite{Wolfe} frequencies (freq)(in \ac{Hz}) can be converted to MIDI codes (mc) and vice versa with these two equations:

\begin{equation}
\text{mc} = 12 * \log_{2} \left( \text{freq} / 440 \text{~Hz} \right) + 69
\end{equation}
\begin{equation*}
\text{freq} = 2 ^ {\frac{ \text{mc} - 69 }{ 12 }} * 440 \text{~Hz}
\end{equation*}


\subsection{Timbre}

Most sounds in everyday life and in music are a combination of multiple frequencies that mix with each other.
Each sound source has its own mix of additional frequencies that it adds to the sounds it is supposed to produce.
These additional frequencies depend on the physical characteristics of the sound source and can vary in patterns over time.
When we hear a sound we use these additional frequencies to distinguish the source of that sound, as a violin, for example, has different additional frequencies than a flute or a human. (see \cite{Rossing2007}, p. 483)


\section{Basics of Signal Processing}
\label{sec:signal_processing}

\subsection{Definition of Signal and Fundamental Frequency}


All signals in the field of audio processing are more or less complex wave functions.
These signals are representations of the sound waves we hear and produce - music, speech and noise.
Electro-acoustic interfaces like microphones and loudspeakers transform sound waves to electric waves and vice versa.
\textquote[\cite{Rossing2007}, p. 504]{
	The \textbf{simplest signal} is a sine wave with (\dots) a single frequency. (\dots) A sine wave function with time $t$, amplitude $C$, angular frequency $\omega$ and starting phase $\varphi$ is given by [the equation \ref{eqn:simple_sine_wave}].
}
\begin{equation}
	\label{eqn:simple_sine_wave}
	x \left( t \right) = C \sin \left( \omega t + \varphi \right)
\end{equation}
To represent silence we have to use an amplitude value of zero (for total silence) or (very) close to zero ($ C \rightarrow 0 $).

$ \omega $ and $ \varphi $ are in radians, so we can substitute $ \omega $ with $ 2 \pi f $ resulting in a signal equation with frequency $ f $ in \ac{Hz}:
\begin{equation}
	\label{eqn:simple_sine_wave_frequency}
	x \left( t \right) = C \sin \left( 2 \pi f t + \varphi \right)
\end{equation}

Usually we encounter more \textbf{complex signals}.
They are described as a sum of simple signals:
\begin{equation}
	\label{eqn:complex_sine_wave}
	x \left( t \right) = \sum_{ n = 1 }^{ N } C_n \sin \left( \omega _n t + \varphi _n \right)
\end{equation}
or
\begin{equation}
	\label{eqn:complex_sine_wave_frequency}
	x \left( t \right) = \sum_{ n = 1 }^{ N } C_n \sin \left( 2 \pi f _n t + \varphi _n \right)
\end{equation}

The \textbf{fundamental frequency $f_0$} of a complex signal is the greatest common divisor ($\gcd$) of all frequencies involved:
$ f_0 = \gcd ( f_0, \dots, f_N ) $.
Complex signals have a period of $f_0$ as indicated in equation \ref{eqn:periodic_f_0}.
\begin{equation}
	\label{eqn:periodic_f_0}
	x \left( t \right) = x \left( t + \frac{ m }{ f_0 } \right)
	\text{ for all } m \in \mathbb{Z}
\end{equation}

With the trigonometric identity
\begin{equation}
	\label{eqn:trigonomicIdentity}
	\sin \left( \theta_1 + \theta_2 \right) = \sin\theta_1\cos\theta_2 + \sin\theta_2+\cos\theta_1
\end{equation}
a complex signal can be transformed to a Fourier series-like representation:
\begin{equation}
	\label{eqn:fourierSeries}
	x \left( t \right) = \sum_{ n = 1 }^{ N } A_n \cos \left( \omega_n t \right) + B_n \sin \left( \omega_n t \right)
\end{equation}

(equations \ref{eqn:simple_sine_wave} to \ref{eqn:fourierSeries} are copied or derived from \cite{Rossing2007}, p. 504-505)

\subsection{Fourier Transforms}

Fourier Transforms are used to calculate frequency, amplitude and phase from time-dependent signal values and vice versa.
Many calculations and operations in audio are computed on the basis of frequency, amplitude and phase,
while electro-acoustic interfaces can handle signals only.

\subsubsection{Fast Fourier Transform (FFT)}
\label{ssec:fft}

Fast Fourier Transforms (\acp{fft}) apply the principle of "divide and conquer" to the Discrete Fourier Transform (DFT, equation \ref{eqn:discreteFourierTransform}), which is a matrix transform.
The matrix to be transformed is split using the stretching and shift theorems.
This process can be repeated multiple times,
eventually reducing the number of multiplications from $N^2$ to $ 2 N \log_2 N $,
where $N$ is the number of elements in the input and output (\cite{Bracewell2000}).
For $N = 8$ the \ac{DFT} counts 64 multiplications,
while \ac{fft} counts 48.
For small $N$ the difference is small, but with audio data series, N becomes huge.

\begin{equation}
	\label{eqn:discreteFourierTransform}
	F_{\text{per}} \left( t \right) = a_0 + \sum_m \left[ b_m \cos \left( 2 \pi f_0 m t \right) + c_m \sin \left( 2 \pi f_0 m t \right)\right]
\end{equation}
with $F_{\text{per}} = $ a periodic waveform (signal), \\
$ a_0 $ = a constant offset, \\
$ b_m $ and $ c_m $ = weights of the $m$th harmonic cosine and sine terms and \\
$ m \in \mathbb{Z} $ (at least approximately). \\
(Equation \ref{eqn:discreteFourierTransform} copied from \cite{Rossing2007}, p. 719.)


\subsubsection{Short-Time Fourier Transform (STFT)}
\label{ssec:stft}

The short-time Fourier transform (\ac{STFT}, see equation \ref{eqn:stft}) splits the audio signal into short segments and applies a Fourier transform to each of them.
The length of the segments (window size) and advance distance on the signal per segment (hop size) can be assigned to values so that the \ac{STFT} can approximate human audio perception (\cite{Rossing2007}).

\begin{equation}
	\label{eqn:stft}
	X \left( f, t \right) = \int_{ - \infty }^{ \infty } w \left( t - \tau \right) x \left( \tau \right) e^{ i 2 \pi f \tau } d \tau
\end{equation}
where $ X \left( f, t \right) $ is the short term frequency spectrum, \\
$ t $ is the time variable, \\
$ w \left( t - \tau \right) $ is the shifted window, \\
$ x \left( \tau \right) $ is the input signal, and \\
$ \left( i 2 \pi f \tau \right) $ is the complex exponential. \\
(Equation \ref{eqn:stft} copied from \cite{Allen1977}.)


\section{Short Basics of Deep Learning}
\label{sec:basics_deep_learning}

\subsection{Definition of Machine Learning}


Deep Learning is a specialized topic in the field of Machine Learning.
One possible definition of machine learning is \\
\textquote[\cite{Mitchell1997} , p. 2]{
	A computer program is said to learn from experience $ E $ with respect to some class of tasks $ T $ and performance measure $ P $, if its performance at tasks in $ T $, as measured by $ P $, improves with experience $ E $.
}

The meaning of task $ T $ is not learning, but what the machine shall perform after the learning is complete.
Common tasks include classification (even in the partial absence of input), regression, transcription (e.g. caption generation for images), (natural language) translation, anomaly detection, synthesis, sampling, density estimation and denoising (\cite{DeepLearningBook}).
The experience $ E $ refers to learning from a dataset,
and the performance measure $ P $ refers to the accuracy (proportion of correct output) or error rate (proportion of incorrect output).


\subsection{Definition of Deep Learning}


\textquote[\cite{DeepLearningBook}]{
	[The idea of deep learning is] to allow computers to learn from experience and understand the world in a hierarchy of concepts, with each concept defined through its relation to simpler concepts	(\dots).
	By gathering knowledge from experience, this approach avoids the need for human operators to formally specify all the knowledge that the computer need.
	The hierarchy of concepts enables the computer to learn complicated concepts by building them out of simpler ones.
	If we draw a graph showing how these concepts are built on top of each other, the graph is deep, with many layers.
	For this reason, we call this approach to artificial intelligence  \textbf{deep learning}.
}


\subsection{Deep Feedforward Networks}


The quintessence of deep learning models are deep feedforward networks, also known as multilayer perceptrons (MLPs) (see \cite{DeepLearningBook}, chapter 6).
Another common name is neural networks because they are inspired by neuroscience.
They are built from a chain of functions, as in equation \ref{eqn:feedforwardNetwork}.
\begin{equation}
	\label{eqn:feedforwardNetwork}
	f \left( x \right) = f^{(1)}\left( f^{(2)}\left( f^{(3)} \left( x \right)\right) \right)
\end{equation}
with $ f^{(n)} $ being the $ n $th layer of the network.

The length of this chain is the depth of the model.
Each layer may be built from a different function, e.g. sigmoid (equation \ref{eqn:sigmoid}), $ \tanh $, rectified linear units ($ o = \max(0, x) $), maximum functions, etc.
Many functions are implemented with parameters that are adjusted during learning.

\begin{equation}
	\label{eqn:sigmoid}
	\text{output } o = \sigma( x ) = \frac{1}{1+e^{ - x } }
\end{equation}

As a rule, the input data and layers work with vectors and matrices.
Depending on the architecture each element of each function can potentially receive many inputs, representing a vector-to-scalar function.
This concept originates from neuroscience, which is why the function computation units are referred to as neurons.

A challenge to the training of these networks are the hidden layers,
referring to each result of a function inside the network which is not shown in the output,
and is (thus) not contained in training data.
To train them, the expected output information has to be processed in reverse direction through the network,
to estimate the expected output of each hidden layer.
This process is called back-propagation.
The estimated outputs are used to determine expectation deviations.
They are necessary to gradually change the parameters of each function during learning,
in order to eventually let the network output match the expected output.


\subsection{Learning: Optimizing the Network}


In the learning phase of a Deep Learning network, the aim is to optimize the parameters within the layer chain, so that the networks output $ f(x) $ approximates the expected result.
The most commonly used algorithm for this optimization is stochastic gradient descent.
The gradient $ g $ is estimated as
\begin{equation}
	\label{eqn:gradient}
	g = \frac{1}{m} \nabla _\theta \sum_{i=1}^{m} L \left( x^{(i)}, y^{(i)}, \theta \right)
\end{equation}
with $ m = $ the count of examples in a training data batch, \\
$ L = $ the loss function, \\
$ x = $ the input value, \\
$ y = $ the target output value, and \\
$ \theta = $ the applied function.

To improve the performance of the network $ g $ is used to update the function parameters:
\begin{equation}
	\label{eqn:updateFunction}
	\theta \leftarrow \theta - \epsilon g
\end{equation}
with $ \epsilon $ denoting the learning rate.

(equations \ref{eqn:gradient} and \ref{eqn:updateFunction} copied from \cite{DeepLearningBook}, p. 149)


\subsection{Recurrent Neural Networks (RNNs)}
\label{ssec:rnn}


Recurrent neural networks are a group of neural networks designed to process sequential data such as language or audio.
They can process long data sequences and many are capable to process data of variable length (see \cite{DeepLearningBook}, chapter 10).
Most of them contain hidden memory variables to keep track of the context whilst processing the data input over time.
Bidirectional \acp{rnn} are a variant.
They are used for applications where each output requires information from the entire input data.
\ac{ddsp} uses a \ac{rnn} in the encoder and in the decoder.

\subsection{Encoder, Decoder, Autoencoder}


An autoencoder is a type of neural network that aims to reconstruct the original input (\cite{DeepLearningBook}).
It consists of two parts:
a) an encoder that transforms the input into a latent representation, and
b) a decoder that reconstructs the input from the latent representation.

Autoencoders are designed with a representational bottleneck.
This prevents them from simply setting $ g \left( f \left( x\right)\right) = x $.
Instead they are built and/or trained with one or more of these limitations:
\begin{itemize}
	\item with limited capacity for the representation (to learn compression and relevant features),
	\item with a sparse penalty that forces them to use the latent representation space as effectively as possible and thus learn to recognize features,
	\item to denoise corrupted input (in training and in use),
	\item to learn manifolds,
	\item to use as little latent representation space as possible (warp the space),
	\item to fulfill even more tasks effectively.
\end{itemize}


\section{Audio Processing Libraries used by DDSP}
\label{sec:audio_processing_libraries}

\subsection{librosa}
\label{sec:librosa}

\texttt{librosa}\footnote{
	\textquote[\cite{McFee2015}]{The name is derived from LabROSA: the LABoratory for the Recognition and Organization of Speech and Audio at Columbia University, where the initial development took place.}
} is a core library in Python that facilitats access to and processing of audio data in a Python environment (\cite{McFee2015}).
In the years leading up to the release of \texttt{librosa}, a growing community increased its research efforts in the application of machine learning to audio.
Initially, there was no well-documented general-purpose audio library for Python, so each project developed its own solution.
The release of \texttt{librosa} has fast-tracked the transition of audio machine learning research to Python, boosted by the integration of important features and approaches from other libraries.

\texttt{librosa} offers a huge set of features including loading from files and saving to files in multiple formats.
It can calculate and display various spectral characteristics, provide information about tempo and beats, perform structural analyses, and break down audio into components and activations.

Since the initial release in 2014, \texttt{librosa} has been actively development and maintained. In the experiments for this work, I used version 0.9.2 which was released in June 2022 (\cite{McFee2022}).


\subsection{CREPE}
\label{sec:crepe}


\texttt{CREPE}\footnote{
	Convolutional REpresentatino for Pitch Estimation
} is a deep convolutional neural network which estimates the frequencies over time from a monophonic audio.
It was released in 2018 and has since been available as a Python module in a pre-trained version.
It shows better accuracy in frequency estimation ($ 99.9\% $) then other state-of-the-art algorithms including robustness on noisy sounds (\cite{CREPE}).


\section{Google Magenta's DDSP}
\label{ddsp}


\begin{figure}
	\includegraphics[width=\columnwidth]{figures/ddsp_autoencoder_by_Google.png}
	\caption{\ac{ddsp}'s autoencoder architecture.
		Red components are part of the neural network architecture,
		green components are the latent representation,
		and yellow components are deterministic synthesizers and effects. Copied from \cite{ddsp}.}
	\label{fig:ddsp_ae_by_Google}
\end{figure}


Differentiable Digital Signal Processing (\ac{ddsp}) is a Python library that integrates interpretable signal processing elements with deep learning using Tensorflow\footnote{
	Tensorflow is an open source library for artificial neural networks. Website:
	\href{https://www.tensorflow.org/}{https://www.tensorflow.org/}. Last visited on Feb. 13th, 2024.
} (\cite{tensorflow}).
This library introduces fully differentiable synthesizers and audio effects that overcome the previous limitations and shortcomings of end-to-end learning.
\ac{ddsp} can be used for noise suppression of monophonic audio,
or for transposing monophonic audio into a different timbre.

\ac{ddsp}'s decoder comprises the following components for audio synthesis:
oscillators, envelopes and filters.
Each of these elements is necessary to create all the characteristics of realistic audio whilst keeping the synthesis process interpretable.

Some details of the DDSP core components are presented in the following sections.


\subsection{Spectral Modeling Synthesis}
\label{ssec:sms}


The \ac{sms} model by \cite{Serra1990} is a very expressive sound model, that is also used in an MPEG-4 audio codec.
\ac{ddsp} uses a version that contains only the Harmonic plus Noise model, which recudes the number of parameters and constrains it to monophonic audio.
Some more theory is described in the following paragraphs, while implementation details can be found in section \ref{sssec:harmonic_audio} and algorithm \ref{algo:synthesisCleanHarmonicAudio}.


\subsubsection{Harmonic Oscillator}


The main component for the audio synthesis is a sinusoidal oscillator.
It produces a signal
\begin{equation}
	\label{eqn:harmonicOscillator}
	x \left( t \right) = \sum_{ k = 1 }^{ K } A_k \left( t \right) \sin \left( \phi _k \left( t \right) \right)
\end{equation}
with $ t = $ timesteps, \\
$ A_k \left( t \right) = $ the time-dependent amplitude of the $ k $-th sinusoidal component, and \\
$ \phi _k \left( t \right) = $ the instantaneous phase of the component.

The phase is calculated by integrating the instantaneous frequency:
\begin{equation}
	\label{eqn:phase}
	\phi _k \left( t \right) = 2 \pi \sum_{ i = 0 }^{t} f_t \left( m \right) + \phi _{ 0, k}
\end{equation}
with $ \phi _{ 0, k} = $ the inital phase.
 
(equations \ref{eqn:harmonicOscillator} and \ref{eqn:phase} copied from \cite{ddsp}.)


\subsubsection{Envelopes} \label{sssec:Envelopes}


For smooth audio synthesis overlapping Hamming windows with a hop size of 4ms (64~timesteps)  and a frame size of 8ms (50\% overlap) were found to be responsive to changes in the audio, with artifacts removed.


\subsubsection{Filtered Noise}

Natural sounds contain harmonic and transient components.
The transient component is synthesized in \ac{ddsp} by applying a substractive \ac{lct-fir} to a stream of random noise.

\subsubsection{Reverb: Long Impulse Responses}

Room reverberation (reverb) is an essential characteristic of realistic audio.
In \ac{ddsp} this is added in a post-synthesis convolution step.
A realistic response can take several seconds, which is associated with extremely long convolutional kernel sizes.
In \ac{ddsp}, convolution is implemented as multiplication in the frequency domain in order to avoid bottlenecks.

\subsection{Multi-Scale Spectral Loss}

As audios with different waveforms can sound very similar, a different solution must be used to compare original and synthesized audio.
This is solved with spectograms $S_i$ and $\hat S_i$ based on \acp{fft} with different sizes, and the loss being the sum of L1 difference between $S_i$ and $\hat S_i$ and L1 difference between $\log S_i$ and $\log \hat S_i$
\begin{equation}
	\label{eqn:ddsp_loss}
	L_i = || S_i - \hat S_i ||_1 + \alpha || \log S_i - log \hat S_i ||_1
\end{equation}
with $\alpha$ as a weight defaulting to $1.0$.
The total reconstruction loss is the sum of all spectral losses, $L_{\text{reconstruction}} = \sum_i L_i$.

(Equation \ref{eqn:ddsp_loss} copied from \cite{ddsp}.)

