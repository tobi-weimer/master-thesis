\chapter{Analysis of Data Flow in DDSP}
\label{c:Dataflow}


This chapter contains validations and findings I have made while analyzing the {ddsp}'s source code and comparing it to the \ac{ddsp}'s paper (\cite{ddsp}).
My approach is documented in section \ref{sec:method:source_code_analysis}.
I documented the implementation details relevant for this thesis in section \ref{sec:latentRepresentationAlgorithmsAndEquations},
including algorithms and equations found in the source code.
In section \ref{sec:discussion:data_flow} I have written a summary of the analysis's outcome and how they contribute to this thesis.

This analysis of the \ac{ddsp}'s source code has necessary for multiple reasons:

\begin{enumerate}
	\item As this work relies on the \ac{ddsp} implementation I needed to know how it processes the data in detail.
	I focused on the details of the \ac{ddsp}'s encoder and decoder to gain knowledge about how \ac{ddsp} determines a latent representation and how it is resynthesized.
	This knowledge is a foundation for the experiments performed in chapters \ref{c:LatentRepresentationAnalysis} and \ref{c:AnalysisDDSP-SynthesizedAudio}.

	\item The \ac{ddsp} paper has been released along with source code and a \ac{ddsp} Python package - both named version 0.0.6\footnote{
		\ac{ddsp} source code released with paper: \href{https://github.com/magenta/ddsp/releases/tag/v0.0.6}{https://github.com/magenta/ddsp/releases/tag/v0.0.6}
	}.
	At the start of working on this thesis version 3.7.0 has been current and therefore used.
	As breaking changes did take place during that development,
	the documentation in the \ac{ddsp} paper had to be verified.

	\item During preparation of this thesis I found the \ac{ddsp}'s latent representation of an audio to contain much more parameters than expected.
	To achieve the goal of this thesis - gaining knowledge about how editing the latent representation of an audio effects the synthesized audio - I needed to know which of the parameters are relevant to the \ac{ddsp}'s decoder.
\end{enumerate}


\section{Method: Source Code Analysis}
\label{sec:method:source_code_analysis}


As \ac{ddsp} is an autoencoder its processing can be divided into an encoding and a decoding part.
I have taken advantage of this and developed a small tool\footnote{
	\href{https://gitlab.com/tobi-weimer/master-thesis/-/blob/Master-Thesis-Final/ddsp_scripts/audio_worker.py}{https://gitlab.com/tobi-weimer/master-thesis/-/blob/Master-Thesis-Final/ddsp\_scripts/audio\_worker.py}
}
that can run \ac{ddsp} in three different modes:
\begin{enumerate}
	\item "traditional" autoencoder mode
	\item encoder-only mode, saving the resulting latent representation of the audios to a \ac{CSV} file
	\item decoder-only mode, reading the latent space representation from a \acs{CSV} file as input for the decoder
\end{enumerate}

During the development of this tool and by studying the resulting \acp{CSV} as well as running the tool in debug mode I ascertained the names of \ac{ddsp}'s latent space parameters.
Then I scanned the \ac{ddsp}'s source code for these parameter names, placing a debug stop at every occurrence within \ac{ddsp} itself as well as inside the Python libraries that \ac{ddsp} uses: Tensorflow\footnote{
	Tensorflow is an open source library for artificial neural networks. Website:
	\href{https://www.tensorflow.org/}{https://www.tensorflow.org/}. Last visited on Feb. 13th, 2024.
},
Keras\footnote{
	Keras is an API and open library for artificial neural networks. Website: \href{https://keras.io/}{https://keras.io/}. Last visited on Feb. 13th, 2024. The API is integrated in Tensorflow: \href{https://www.tensorflow.org/versions/r2.11/api_docs/python/tf/keras}{https://www.tensorflow.org/versions/r2.11/api\_docs/python/tf/keras}. Last visited on Feb. 13th, 2024.
},
and CREPE (see chapter \ref{sec:crepe}).
In the preprocessing I use \texttt{librosa} (see chapter \ref{sec:librosa}) to load the audio file and determine the samplerate.
To discover how \texttt{librosa} processes the file, I also placed debug stops inside \texttt{librosa}'s source code.
During the scanning I found additional parameter names, so I repeated the process of scanning and placing debug stops.

After searching through the source code and placing a lot of debug stops I set up my tool to run \ac{ddsp}'s encoder in debug mode.
The goal of debugging was to identify:
\begin{enumerate}
	\item which of the code sections are used, including their order of execution
	\item what algorithms and formulas are used by \ac{ddsp} during the processing
	\item how and where the latent representation parameters are created, including whether they are also used as inputs for subsequent encoding steps
\end{enumerate}
All findings on the \ac{ddsp} encoding process are documented in the following chapter.

When the encoding process was fully examined and documented I did the same for the decoder part of the \ac{ddsp} and have also documented all findings in the following chapter.
The goals have been almost the same, only the third goal has been changed to find out which latent representation parameters are mandatory input for the decoder, and how the synthesizers are called and connected to create target audio.

Based on my findings I have modified my tool running \ac{ddsp}'s encoder and decoder.
Unnecessary data is now excluded from being saved to \ac{CSV}, which results in a decrease of saved data of about $70$ to $75\%$ compared to saving the complete latent representation after encoding.


\section{The Latent Representation Parameters}
\label{sec:latentRepresentationAlgorithmsAndEquations}

In this section I describe my findings concerning the parameters of Google Magenta's \ac{ddsp}\footnote{
	Webpage: \href{https://magenta.tensorflow.org/ddsp}{https://magenta.tensorflow.org/ddsp}. Last visited on Dec. 8th, 2023.\\
	Github: \href{https://github.com/magenta/ddsp}{https://github.com/magenta/ddsp}. Last visited on Dec. 8th, 2023.
},
version 3.7.0\footnote{
	Github commit for \ac{ddsp} version 3.7.0:\\ \href{https://github.com/magenta/ddsp/commit/761d61a5f13373c170f3a04d54bb28a1e2f06bab}{https://github.com/magenta/ddsp/commit/761d61a5f13373c170f3a04d54bb28a1e2f06bab}. Last visited on Dec. 8th, 2023.\\
	Python PIP Package: \href{https://pypi.org/project/ddsp/3.7.0/\#files}{https://pypi.org/project/ddsp/3.7.0/\#files}. Last visited on Dec. 8th, 2023.
}.
I have ascertained these by scanning the sources and running the program in debug mode.
This analysis is restricted to the latent representation parameters which contain data derived from the audio and are relevant for the process of encoding and decoding.

As an autoencoder, \ac{ddsp} consists of an encoder and a decoder part.
Both of these can be run separately, which is what I have undertaken for this work.
I analyzed the state of \ac{ddsp}'s latent representation following completion of the encoding process and before commencement of the decoding process.


\subsection{Encoder}
\label{ssec:encoder}


The \ac{ddsp} encoder itself starts with the source audio in memory handed over as a function parameter.
So the \ac{WAV} file and \texttt{librosa} seen at the start in figure \ref{fig:ddsp_decoder_dataflow} are the preprocessing executed by my script and included here for completeness.
The audio is processed by \ac{ddsp} in three parallel but independent paths:
\begin{enumerate}
	\item \acp{mfcc} are computed and fed into a \ac{rnn}. This returns the parameter \texttt{Z}.
	\item A \ac{STFT} is used to determine the loudness over time, which is scaled into the range $\left[0.0 , 1.0\right]$ and returned by the encoder as \texttt{ld\_scaled}.
	\item \texttt{f0\_hz} is estimated by CREPE. It also returns an unused \linebreak
	\texttt{f0\_confidence}. After estimation \texttt{f0\_hz} is scaled into the range $\left[0.0 , 1.0\right]$.	
\end{enumerate}
Figure \ref{fig:ddsp_encoder_dataflow} shows an overview of the dataflow inside \ac{ddsp}'s encoder.
The following subsections contain details on how each parameter is computed.


\begin{figure}
	\centering
	\begin{tikzpicture}[
	>={Latex[width=2mm,length=2mm]},
	node distance={11mm}, thick, align=center,
	minimum width=1cm,
	inner sep=2mm,
	base/.style   = {rectangle, rounded corners, font=\sffamily, draw=black},
	param/.style  = {base, fill=yellow!30},
	input/.style  = {base, fill=blue!30},
	unused/.style = {base, fill=white},
	saved/.style  = {base, fill=orange!30},
	func/.style   = {base, fill=green!30, font=\ttfamily},
	prep/.style   = {func, fill=pink!30}
	]
	
	\node[input]  (wav)                           {.wav-File};
	\node[prep]   (librosa)   [below of=wav]      {librosa};
	\node[input]  (audio)     [below of=librosa]  {audio};
	
	\node[func]   (crepe)     [below of=audio,
	right=25mm]        {CREPE};
	\node[saved]  (f0-hz)     [below of=crepe]    {$f0_{hz}$};
	\node[unused] (f0-conf)   [below of=crepe,
	right=20mm]        {$f0_{confidence}$\\
		(not used)};
	\node[func]   (scale-f0)  [below of=f0-hz]    {scale f0};
	\node[saved]  (f0-scaled) [below of=scale-f0] {$f0_{scaled}$};
	
	\node[func]   (sftf)      [below of=audio]    {STFT in DDSP};
	\node[param]  (ld-db)     [below of=sftf]     {loudness (dB)};
	\node[func]   (scade-ld)  [below of=ld-db]    {scale loudness};
	\node[saved]  (ld-scaled) [below of=scade-ld] {$ld_{scaled}$};
	
	\node[func]   (cmfcc)     [below of=audio,
	left=20mm]         {Compute MFCCs};
	\node[param]  (mfcc)      [below of=cmfcc]    {MFCCs};
	\node[func]   (ddsp)      [below of=mfcc]     {Encoder RNN};
	\node[saved]  (z)         [below of=ddsp]     {$z$};
	
	
	\draw[->] (wav)      -- (librosa);
	\draw[->] (librosa)  -- (audio);
	\draw[->] (audio)    -- (crepe);
	\draw[->] (crepe)    -- (f0-hz);
	\draw[->] (crepe)    -- (f0-conf);
	\draw[->] (f0-hz)    -- (scale-f0);
	\draw[->] (scale-f0) -- (f0-scaled);
	
	\draw[->] (audio)    -- (sftf);
	\draw[->] (sftf)     -- (ld-db);
	\draw[->] (ld-db)    -- (scade-ld);
	\draw[->] (scade-ld) -- (ld-scaled);
	
	\draw[->] (audio)    -- (cmfcc);
	\draw[->] (cmfcc)    -- (mfcc);
	\draw[->] (mfcc)     -- (ddsp);
	\draw[->] (ddsp)     -- (z);
	
	\end{tikzpicture}
	\caption{Overview of the data flow in DDSP encoder. The input is blue, preprocessing functions are pink, processing functions are green, temporary variables are yellow, mandatory variables for the decoder are orange and unused variables are white.} \label{fig:ddsp_encoder_dataflow}
\end{figure}


\subsubsection{audio}

This parameter is the raw audio data as loaded from file by \texttt{librosa}.
It is the input for \ac{ddsp}'s encoder.
It is one of the variables kept in memory by \ac{ddsp}, but is not used by the decoder and thus not saved to \acs{CSV} file.


\subsubsection{time frame}

For encoding this raw audio is split into pieces with a frame size (length) of 16ms.
In \ac{ddsp} and in this work they are called time frames.
The start of each time frame is 4ms behind the start of the previous time frame.
The length of this shifting is called hop size and results in overlapping time frames.
As a consequence of this small hop size,
each second of audio is split into 250 time frames.
These time frames are processed one after another by \ac{ddsp}
which results in 250 values per latent representation parameter per second of audio.


\subsubsection{f0\_hz}
\label{sssec:enc:f0_hz}

The parameter \texttt{f0\_hz} is the fundamental frequency of the audio per time frame.
It is indicated in \ac{Hz}.
The determination process by CREPE is documented in algorithm \ref{algo:determineF0}.
CREPE uses a Keras\footnotemark model to estimate activations.
These are the input for further processing which finally returns \texttt{f0\_hz}.

\footnotetext{Keras is an API and open library for artificial neural networks.}

\begin{algorithm}
	\caption{Determine f0 in \ac{Hz}}
	\label{algo:determineF0}
	\begin{algorithmic}
		\REQUIRE audio, timesteps
		\STATE activations, f0\_confidence $\leftarrow$ Keras model ( audio )
		\STATE cents $\leftarrow$ calculate viterbi cents ( activations ) \\
		\COMMENT{ calculate viterbi cents determines the fequency of represented by the activations using the Viterbi algorithm. }
		\STATE f0\_hz $\leftarrow$ $ 10 * 2 ^ { \frac{\text{cent}}{1200} } $
		\FORALL{ t in timesteps }
		\IF{ f0\_hz ( t ) is \texttt{NaN} }
		\STATE f0\_hz ( t ) $\leftarrow$ 0
		\ENDIF
		\ENDFOR
		\RETURN f0\_hz, f0\_confidence
	\end{algorithmic}
\end{algorithm}


\subsubsection{f0\_confidence}

The parameter \texttt{f0\_confidence} is the confidence of the Keras model while it estimates the activations for the \texttt{f0\_hz} calculation.
Similar to \texttt{f0\_hz} it contains one value per time frame of the audio.
The value range is $ \left[ 0.0 , 1.0 \right] $

This value is never used within the encoder or decoder and is not saved to files.


\subsubsection{f0\_scaled}
\label{sssec:enc:f0_scaled}

\texttt{f0\_scaled} is a converted and scaled version of \texttt{f0\_hz} with a possible value range of $ \left[ 0.0 , 1.0 \right] $.
\ac{ddsp}'s algorithm converts \texttt{f0\_hz} to MIDI (see chapter \ref{ssec:MIDI}) and divides the result by the maximum MIDI value using the two equations \ref{eqn:midicode} and \ref{eqn:f0-scaled}.

\begin{equation}
\label{eqn:midicode}
\text{midi\_code} = 12 * \log_{2} \left( \frac{ \text{f0\_hz} }{ 440 } \right) + 69
\end{equation}
\begin{equation}
\label{eqn:f0-scaled}
\text{f0\_scaled} = \frac{ \max \left( \text{midi\_code} , 0 \right) }{ \text{F0\_range} }
\end{equation}
with F0\_range $ = 127$


\subsubsection{loudness\_db}

\texttt{loudness\_db} is the loudness of the audio per time frame in decibel (dB), with the vast majority of values found within the range $ \left[ -80.0 , 0.0 \right] $.
In variations of \ac{ddsp} this range may be swapped to $ \left[ 0.0 , 80.0 \right] $.
It is calculated by \ac{ddsp} using the four equations \ref{eqn:loudnessPower}, \ref{eqn:min_power}, \ref{eqn:powerToDb} and \ref{eqn:loudness}.
The result of equation \ref{eqn:loudness} is used only to calculate \texttt{ld\_scaled}.
It is not used in the decoder and does not therefore need to be saved in the \acs{CSV} file.

\begin{equation}
\label{eqn:loudnessPower}
\text{power} = \text{reduce\_mean} \left( \Bigl| \text{STFT}\left( \text{audio} \right) \Bigr| ^ 2 * 10 ^ \frac{ \text{A\_weighting} }{ 10 } \right)
\end{equation}

STFT is the Sort-time Fourier Transform and A\_weighting is a perceptual weighting for the Fourier Transform frequencies as implemented in the Python package \texttt{librosa}.

\begin{equation}
\label{eqn:min_power}
\text{min\_power} = \max\left( 10 ^ { - \left( \frac{ \text{range\_db} }{10} \right) } , \text{power} \right)
\end{equation}

with range\_db being a parameter with the default value $ 80 $.

\begin{equation}
\label{eqn:powerToDb}
\text{db} = 10 * \log_{10} \left( \text{min\_power} \right)
\end{equation}

\begin{equation}
\label{eqn:loudness}
\text{loudness\_db} = \max\left( \text{db} - \text{ref\_db} , -\text{range\_db} \right)
\end{equation}

with range\_db being the same parameter as in equation \ref{eqn:min_power}, and ref\_db with default value $ 0 $.


\subsubsection{ld\_scaled}
\label{sssec:ld_scaled}

\texttt{ld\_scaled} is the \texttt{loudness\_db} converted from the dB value range $ \left[ -80.0 , 0 \right] $ to the new target range $ \left[ 0.0 , 1.0 \right] $ using equation \ref{eqn:ld-scaled}.
Due to the design of the loudness determination and the scaling equation values higher than 1.0 may occur, but should be rare within the domain of music.
\begin{equation}
\label{eqn:ld-scaled}
\text{ld\_scaled} = \frac{ \text{loudness\_db} }{\text{db\_range}} + 1
\end{equation}
with db\_range $ = 80 $


\subsubsection{Z}

According to the \ac{ddsp} authors \texttt{Z} is responsible for effects and noise in the resynthesized audio.
To optain \texttt{Z} the \ac{ddsp} encoder computes the \ac{mfcc} values for the input audio.
As a second step \ac{mfcc} are fed into an \ac{rnn} inside the \ac{ddsp}s encoder.
The output of the \ac{rnn} is the 16-dimensional parameter \texttt{Z}.


\subsection{Decoder}
\label{ssec:decoder}


The decoder is the second part of an autoencoder.
In the case of \ac{ddsp} it takes the latent representation as input and produces audio data as output.
An overview of the decoder modules in the \ac{ddsp} is shown in figure \ref{fig:ddsp_decoder_dataflow}.
As for the encoder, the decoder has up to three parallel processing paths, but this time the paths merge together to produce the target audio.

The following subsections contain details on how each parameter is involved in creating the final synthesized audio data.


\begin{figure}
	\centering
	\begin{tikzpicture}[
	>={Latex[width=2mm,length=2mm]},
	node distance={11mm}, thick, align=center,
	minimum width=1cm,
	inner sep=2mm,
	base/.style   = {rectangle, rounded corners, font=\sffamily, draw=black},
	param/.style  = {base, fill=yellow!30},
	input/.style  = {base, fill=blue!30},
	unused/.style = {base, fill=white},
	saved/.style  = {base, fill=orange!30},
	func/.style   = {base, fill=green!30, font=\ttfamily}
	]
	
	\node[input]  (f0-scaled) [left=30mm]           {$f0_{scaled}$};
	\node[input]  (ld-scaled)                       {$ld_{scaled}$};
	\node[input]  (z)         [right=35mm]          {$z$};
	
	\node[func]   (ddsp)      [below of=ld-scaled]  {Decoder RNN};
	
	\node[param]  (amps)      [below of=f0-scaled,
	yshift=-11mm]        {amplitudes};
	\node[param]  (harmonic)  [below of=ddsp]       {harmonic distribution};
	\node[param]  (noise)     [below of=z,
	yshift=-11mm]        {noise magnitudes};
	
	\node[input]  (f0-hz)     [below of=amps]       {$f0_{hz}$};
	\node[func]   (harmsynth) [below of=harmonic,
	yshift=-3mm]         {Harmonic\\
		Synthesizer};
	\node[func]   (noisesynth) [below of=noise,
	yshift=-3mm]         {Noise\\
		Synthesizer};
	
	\node[param]  (audio1)    [below of=harmsynth,
	yshift=-5mm]         {clean harmonic audio\\
		without noise};
	\node[param]  (audio2)    [below of=noisesynth,
	yshift=-5mm]         {filtered\\
		noise audio};
	
	\node[func]   (add)       [below of=audio1,
	right=17.5mm,
	yshift=-2mm]         {$+$};
	
	\node[saved]  (audio)	  [below of=add]        {final audio $\Rightarrow$ .wav-File};
	
	
	\draw[->] (f0-scaled)  -- (ddsp);
	\draw[->] (ld-scaled)  -- (ddsp);
	\draw[->] (z)          -- (ddsp);
	
	\draw[->] (ddsp)       -- (amps);
	\draw[->] (ddsp)       -- (harmonic);
	\draw[->] (ddsp)       -- (noise);
	
	\draw[->] (amps)       -- (harmsynth);
	\draw[->] (harmonic)   -- (harmsynth);
	\draw[->] (f0-hz)      -- (harmsynth);
	
	\draw[->] (noise)      -- (noisesynth);
	
	\draw[->] (harmsynth)  -- (audio1);
	
	\draw[->] (noisesynth) -- (audio2);
	
	\draw[->] (audio1)     -- (add);
	\draw[->] (audio2)     -- (add);
	
	\draw[->] (add)        -- (audio);
	
	\end{tikzpicture}
	\caption{Overview of the data flow in the DDSP decoder. The input variables are blue, processing functions are green, temporary variables are yellow, variables stored to file are orange and unused variables are white.} \label{fig:ddsp_decoder_dataflow}
\end{figure}


\subsubsection{f0\_scaled, ld\_scaled and Z}


The two one-dimensional parameters \texttt{f0\_scaled} and \texttt{ld\_scaled} and the 16-dimensional parameter \texttt{Z} are concatenated together.
The resulting 18-dimensional tensor is used as input for the most essential part of the \ac{ddsp}'s decoder: the decoder \ac{rnn}.
The internals of this decoder are documented in appendix B.2 of the original \ac{ddsp} paper (see \cite{ddsp}, p. 15f).

The output of the decoder \ac{rnn} is spilt into three variables:
\texttt{amplitudes} with one dimension per timestep, \texttt{harmonic\_distribution} with 100 dimensions per timestep, and \texttt{noise\_magnitudes} with 65 dimensions per timestep.
These three variables are accessible during the decoding process within the \ac{ddsp}, just before being used by the synthesizers, and after the decoding is finished (by design for backtracking in training mode, but may be re-used as well).


\subsubsection{amplitudes, harmonic\_distribution and f0\_hz}
\label{sssec:harmonic_audio}

After being calculated by \ac{ddsp}'s decoder \ac{rnn}, the \texttt{amplitudes} and \texttt{harmonic\_distribution} are scaled using equation \ref{eqn:scale}.
The scaled values are used as input for the harmonic synthesizer along with \texttt{f0\_hz} as third input.
The \texttt{amplitudes} mainly contain information about the loudness of the audio to be synthesized,
while the \texttt{harmonic\_distribution} contains information about frequencies that harmonize with the fundamental frequency (\texttt{f0\_hz}).

\begin{equation}
\label{eqn:scale}
\text{scaled\_value} = \text{max\_value} * \frac{1}{1 + e^{-\text{unscaled\_value}}} ^{ \ln \left( \text{exp} \right) } + \text{threshold}
\end{equation}
with default values of max\_value $ = 2.0 $, exp $ = 10.0$, and threshold $ = 1*10^{-7}$

The output of the harmonic synthesizer is a \texttt{clean harmonic audio} which should be free from any noise.
This is an essential part of the \ac{ddsp} decoding.
How the harmonic synthesizer operates is briefly described in algorithm \ref{algo:synthesisCleanHarmonicAudio}.

\begin{algorithm}
	\caption{Synthesis of Clean Harmonic Audio}
	\label{algo:synthesisCleanHarmonicAudio}
	\begin{algorithmic}
		\REQUIRE amplitudes \COMMENT{ loudness information for the various frequencies }
		\REQUIRE f0\_hz \COMMENT{ The fundamental frequency }
		\REQUIRE harmonic\_distribution \COMMENT{ harmonic frequencies related to f0\_hz }
		\REQUIRE \#samples \COMMENT{ representing the length of the synthesized }
		\REQUIRE samplerate \COMMENT{ for the target audio }
		\REQUIRE tf (TensorFlow library)
		\STATE
		\STATE \#harmonics $\leftarrow$ dimensions ( \textbf{harmonic\_distribution} )
		\STATE harmonic\_frequencies $\leftarrow$ \textbf{tf}.linspace ( \\
		\qquad \qquad start = 1.0 , \\
		\qquad \qquad stop = \#harmonics , \\
		\qquad \qquad count = \#harmonics ) \\
		\qquad $*$ \textbf{f0\_hz}
		\STATE harmonic\_amplitudes $\leftarrow$ \textbf{amplitudes} $*$ \textbf{harmonic\_distribution}
		\STATE
		\STATE frequency\_envelope $\leftarrow$ resample ( harmonic\_frequencies , \textbf{\#samples} )
		\STATE amplitude\_envelope $\leftarrow$ resample ( harmonic\_amplitudes , \textbf{\#samples} )
		\STATE
		\STATE \textbf{clean\_harmonic\_audio} $\leftarrow$ oscillator\_bank ( \\
		\qquad \qquad frequency\_envelope , \\
		\qquad \qquad amplitude\_envelope , \\
		\qquad \qquad \textbf{samplerate} ) \\
		\COMMENT{ the \texttt{oscillator\_bank} is the part of this algorithm that produces the audio data. }
		\STATE
		\STATE \COMMENT{ normalize audio - the sum of all dimensions at a timestep shall be 1 }
		\STATE \textbf{clean\_harmonic\_audio} $\leftarrow$ \textbf{tf}.reduce\_sum( clean\_harmonic\_audio, axis=-1 )
		\STATE
		\RETURN clean\_harmonic\_audio
	\end{algorithmic}
\end{algorithm}


\subsubsection{noise\_magnitues}


Calculated by \ac{ddsp}'s decoder RNN the \texttt{noise\_magnitudes} are used for synthesizing the \texttt{filtered noise audio}.
At first the responsible synthesizer creates a random noise signal, which is filtered to match the expected noise levels and distribution using the \texttt{noise\_magnitudes}.
According to its authors \ac{ddsp} can be configured to run its decoder with different noise creation approaches.


\subsubsection{From Clean Harmonic Audio and Filtered Noise Audio to Final Audio}

The final audio created by the \ac{ddsp}'s decoder is calculated by adding up the audio signals of the \texttt{clean harmonic audio} and the \texttt{filtered noise audio}.
This final audio is returned to the script or program that executed \ac{ddsp}'s decoder and is ready to be used.
Once \ac{ddsp} has returned the audio my wrapper script simply saves it to a \ac{WAV} file on disk.


\section{Discussion}
\label{sec:discussion:data_flow}


I acknowledge that all the information in this whole chapter is already available publicly in the \ac{ddsp} paper and source code.
Nevertheless I consider this chapter relevant, as it presents the information - in particular the algorithms and equations used - in a much more readable way and thus the details of the processing are easier to understand.

Also, a good share of the source code inspections needed for the present chapter were mandatory for the experiments presented in chapters \ref{c:LatentRepresentationAnalysis} and \ref{c:AnalysisDDSP-SynthesizedAudio}, thus presenting this information has not been too much extra effort.

