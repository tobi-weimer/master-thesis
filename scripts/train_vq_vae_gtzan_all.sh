# call as: nohup bash scripts/train_vq_vae_gtzan_all.sh > log/train_vq_vae_gtzan_all.log &
save_dir="/project/wtp-music-embedding/models/vq_vae_gtzan_all"
mkdir $save_dir

root_dir="/project/wtp-music-embedding/code/wtp-music-embedding"
export PYTHONPATH=$root_dir  # Needed because we need modules of this project

ddsp_run \
  --mode=train \
  --alsologtostderr \
  --save_dir="$save_dir" \
  --gin_file="$root_dir/model_configs/vq_vae.gin" \
  --gin_file=datasets/tfrecord.gin \
  --gin_param="TFRecordProvider.file_pattern='/scratch/wtp-music-embedding/datasets/gtzan/ddsp/*/*'" \
  --gin_param="batch_size=16" \
  --gin_param="train_util.train.num_steps=150000" \
  --gin_param="train_util.train.steps_per_save=300" \
  --gin_param="trainers.Trainer.checkpoints_to_keep=10"