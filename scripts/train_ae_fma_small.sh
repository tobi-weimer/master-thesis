# call as: nohup bash scripts/train_ae_fma_small.sh > log/train_ae_fma_small.log &
save_dir="/project/wtp-music-embedding/models/ae_fma_small"
mkdir $save_dir

ddsp_run \
  --mode=train \
  --alsologtostderr \
  --save_dir="$save_dir" \
  --gin_file=models/ae.gin \
  --gin_file=datasets/tfrecord.gin \
  --gin_param="TFRecordProvider.file_pattern='/scratch/wtp-music-embedding/datasets/fma_small/ddsp/train/**/*'" \
  --gin_param="batch_size=16" \
  --gin_param="train_util.train.num_steps=150000" \
  --gin_param="train_util.train.steps_per_save=300" \
  --gin_param="trainers.Trainer.checkpoints_to_keep=10"