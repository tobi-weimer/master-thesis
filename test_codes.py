import unittest

import codes
import numpy as np
import numpy.testing as np_test


class TestCodes(unittest.TestCase):

    def test_unique_transitions_with_probabilities(self):
        #    0 1
        # 0| 1 2
        # 1| 3 4
        trans = np.array([[1, 1],
                          [0, 1],
                          [0, 0],
                          [0, 1],
                          [1, 1],
                          [1, 0],
                          [1, 0],
                          [1, 1],
                          [1, 0],
                          [1, 1]])

        unique_trans, probs = codes.unique_transitions_with_probabilities(trans)
        np_test.assert_array_equal(unique_trans, np.array([[0, 0], [0, 1], [1, 0], [1, 1]]))
        np_test.assert_array_equal(probs, np.array([1 / 3, 2 / 3, 3 / 7, 4 / 7]))

    def test_compute_transition_counts(self):
        transitions = np.array([[1, 2],
                                [2, 2],
                                [2, 2],
                                [2, 2],
                                [2, 3],
                                [3, 2],
                                [2, 3],
                                [3, 3]])

        graph_data = codes.compute_transition_counts(transitions)
        np_test.assert_array_equal(graph_data.all_transitions, [[1, 2], [2, 2], [2, 3], [3, 2], [3, 3]])
        np_test.assert_array_equal(graph_data.all_counts, [1, 3, 2, 1, 1])
        np_test.assert_array_equal(graph_data.non_self_transitions, [[1, 2], [2, 3], [3, 2]])
        np_test.assert_array_equal(graph_data.non_self_counts, [1, 2, 1])
        np_test.assert_array_equal(graph_data.self_transitions, [2, 3])
        np_test.assert_array_equal(graph_data.self_counts, [3, 1])
