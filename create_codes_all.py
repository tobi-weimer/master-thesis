from create_codes import create_codes
from os import path
from pathlib import Path


def codes_exist(tfrecord_output_file):
    return path.isfile(f'{tfrecord_output_file}.done')


def mark_codes_exist(tfrecord_output_file):
    Path(f'{tfrecord_output_file}.done').touch(exist_ok=True)


def create_all_codes():
    i = 0
    for dataset in ["fma_small", "gtzan"]:
        for model in ["fma_small_train", "fma_small_train_and_validation", "fma_small_all", "gtzan_train",
                      "gtzan_train_and_validation", "gtzan_all"]:
            for split in ["training", "validation", "test"]:
                i += 1
                print(f'Step {i}: Creating codes for dataset {dataset} using the DDSP model trained'
                      f' on {model} (split={split})')
                tfrecord_output_file = f"/scratch/wtp-music-embedding/datasets/{dataset}/codes_generated_using_model_trained_on/{model}/{split}.tfrecord"

                # Skip the step if the codes already exist
                if codes_exist(tfrecord_output_file):
                    print(f'Step {i}: Codes already exist. Skipping...')
                    continue

                create_codes(
                    tfrecord_dir=f"/scratch/wtp-music-embedding/datasets/{dataset}/ddsp_no_overlap/{split}",
                    tracks_metadata_file=f"/scratch/wtp-music-embedding/datasets/{dataset}/metadata/tracks.csv",
                    trained_model_dir=f"/project/wtp-music-embedding/models/vq_vae_{model}",
                    tfrecord_output_file=tfrecord_output_file,
                    dataset_name=dataset
                )
                mark_codes_exist(tfrecord_output_file)

                print(f'Step {i}: Finished creating codes for dataset {dataset} using the DDSP model trained on'
                      f' {model} (split={split})')
    print("Finished creating all codes")


if __name__ == '__main__':
    create_all_codes()
