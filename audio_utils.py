import ddsp
import numpy as np
import sounddevice as sd
from matplotlib import pyplot as plt
from scipy.io import wavfile


def show_specplot(audio,
                  vmin=-5,
                  vmax=1,
                  rotate=True,
                  size=512 + 256,
                  **matshow_kwargs):
    logmag = ddsp.spectral_ops.compute_logmag(ddsp.core.tf_float32(audio),
                                              size=size)
    if rotate:
        logmag = np.rot90(logmag)
    # Plotting.
    plt.matshow(logmag,
                vmin=vmin,
                vmax=vmax,
                cmap=plt.cm.magma,
                aspect='auto',
                **matshow_kwargs)
    plt.xticks([])
    plt.yticks([])
    plt.xlabel('Time')
    plt.ylabel('Frequency')
    plt.show()


def save_specplot(file, audio,
                  vmin=-5,
                  vmax=1,
                  rotate=True,
                  size=512 + 256,
                  **matshow_kwargs):
    logmag = ddsp.spectral_ops.compute_logmag(ddsp.core.tf_float32(audio),
                                              size=size)
    if rotate:
        logmag = np.rot90(logmag)
    # Plotting.
    plt.matshow(logmag,
                vmin=vmin,
                vmax=vmax,
                cmap=plt.cm.magma,
                aspect='auto',
                **matshow_kwargs)
    plt.xticks([])
    plt.yticks([])
    plt.xlabel('Time')
    plt.ylabel('Frequency')
    plt.savefig(file, bbox_inches='tight')
    plt.close()


def play_audio(audio):
    sd.play(audio, 16000, blocking=True)


def save_audio(file, audio):
    normalizer = float(np.iinfo(np.int16).max)
    audio = np.array(np.asarray(audio) * normalizer, dtype=np.int16)

    wavfile.write(file, 16000, audio)
