import os
from collections import namedtuple

import gin
import numpy as np
import tensorflow as tf
from scipy.cluster import hierarchy

import vq_vae

timesteps = 7000
codebook_size = 64
feature_description = {
    'codes': tf.io.FixedLenFeature([timesteps], tf.int64),
    'track_id': tf.io.FixedLenFeature([], tf.string),
    'genre_idx': tf.io.FixedLenFeature([], tf.int64),
}


def get_parse_codes_example_map_fn(codes_dtype=tf.int32):
    def parse_codes_example(example_proto):
        example = tf.io.parse_single_example(example_proto, feature_description)
        example['codes'] = tf.cast(example['codes'], codes_dtype)
        example['track_id'] = tf.cast(example['track_id'], tf.string)
        example['genre_idx'] = tf.cast(example['genre_idx'], tf.int32)
        return example

    return parse_codes_example


def get_calc_frequencies_map_fn(normalize=True):
    def calc_frequencies(parsed_example):
        code_frequencies = tf.math.bincount(parsed_example['codes'], minlength=codebook_size, maxlength=codebook_size)
        if normalize:
            code_frequencies = code_frequencies / tf.reduce_sum(code_frequencies)
        parsed_example['code_frequencies'] = code_frequencies
        return parsed_example

    return calc_frequencies


def get_calc_code_transitions_map_fn():
    def calc_code_transitions(parsed_example):
        codes = parsed_example['codes']
        codes_shifted = tf.roll(codes, shift=-1, axis=-1)
        transitions = tf.stack((codes, codes_shifted), axis=-1)
        # Drop the last transition that points from the last code to the first in the sequence
        transitions = transitions[:-1]
        parsed_example['transitions'] = transitions
        return parsed_example

    return calc_code_transitions


def get_as_tuple_map_fun(*features):
    def as_tuple(parsed_example):
        if len(features) == 1:
            return parsed_example[features[0]]
        else:
            return tuple(parsed_example[feature] for feature in features)

    return as_tuple


def get_codes_dataset(*splits, dataset_name, model_name=None):
    model_name = model_name if model_name else f'{dataset_name}_train'
    ds_paths = [f'D:/devel/datasets/{dataset_name}/codes_generated_using_model_trained_on/{model_name}/{split}.tfrecord'
                for split in splits]
    return tf.data.TFRecordDataset(ds_paths)


def unique_transitions_with_probabilities(trans):
    unique_start_states, start_state_counts = np.unique(trans[:, 0], return_counts=True)
    unique_trans, trans_counts = np.unique(trans, axis=0, return_counts=True)

    start_states_per_trans_idxs = np.searchsorted(unique_start_states, unique_trans[:, 0])
    start_states_per_trans_counts = start_state_counts[start_states_per_trans_idxs]
    trans_probs = trans_counts / start_states_per_trans_counts

    return unique_trans, trans_probs


def unique_transitions_with_counts(trans, norm_by_num_tracks=False):
    unique_trans, trans_counts = np.unique(trans, axis=0, return_counts=True)
    if norm_by_num_tracks:
        num_tracks = np.sum(trans_counts) / 6999  # 6999=number of codes per track
        trans_counts = trans_counts / num_tracks

    return unique_trans, trans_counts


TransitionCounts = namedtuple("TransitionCounts",
                              "all_transitions, all_counts, non_self_transitions non_self_counts self_transitions self_counts")


def compute_transition_counts(transitions, norm_by_num_tracks=False) -> TransitionCounts:
    all_transitions, all_counts = unique_transitions_with_counts(transitions, norm_by_num_tracks=norm_by_num_tracks)
    is_self_transition = all_transitions[:, 0] == all_transitions[:, 1]
    self_edge_idxs, non_self_edge_idxs = np.where(is_self_transition), np.where(~is_self_transition)
    self_transitions, self_counts = np.squeeze(all_transitions[self_edge_idxs, 0]), all_counts[self_edge_idxs]
    non_self_transitions, non_self_counts = all_transitions[non_self_edge_idxs], all_counts[non_self_edge_idxs]

    return TransitionCounts(
        all_transitions=all_transitions,
        all_counts=all_counts,
        non_self_transitions=non_self_transitions,
        non_self_counts=non_self_counts,
        self_transitions=self_transitions,
        self_counts=self_counts
    )


def load_model_codebook(trained_model_dir):
    gin_file = os.path.join(trained_model_dir, 'operative_config-0.gin')
    gin.parse_config_file(gin_file)

    model = vq_vae.QuantizingAutoencoder()
    model.restore(trained_model_dir)

    # Use the model on some dummy data, so that tf fills all model variables with the saved data
    model({'audio': tf.zeros((7, 64000), dtype=tf.float32),
           'loudness_db': tf.zeros((7, 1000), dtype=tf.float32),
           'f0_hz': tf.zeros((7, 1000), dtype=tf.float32),
           'f0_confidence': tf.zeros((7, 1000), dtype=tf.float32)
           })

    return model.quantizer.codebook


def get_optimal_codebook_ordering(codebook, clustering_method, return_linkage: bool = False):
    if not clustering_method and return_linkage:
        raise ValueError("Cannot return a linkage for clustering method=None")
    elif not clustering_method:
        return np.arange(codebook.shape[0])
    else:
        if not isinstance(codebook, np.ndarray):
            codebook = codebook.numpy()

        # Perform hierarchical clustering
        linkage = hierarchy.linkage(codebook, method=clustering_method, metric='euclidean', optimal_ordering=True)

        # Create the leaves list from the already optimally ordered clustering. This is the code ordering we can return.
        ordering = hierarchy.leaves_list(linkage)
        if return_linkage:
            return ordering, linkage
        else:
            return ordering
