import os
import subprocess
from builtins import print
from pathlib import Path

from absl import flags, app

FLAGS = flags.FLAGS
flags.DEFINE_string(
    'tfrecords_root_dir', None,
    'The directory containing the tfrecords files to process.')
flags.DEFINE_string(
    'tfrecords_path_and_name_pattern', None,
    'The pattern of relative path from tfrecords_root_dir/<SONG_DIR> and filename, ' +
        'e.g. "/**/*STEM*half1.tfrecord-00000-of-00001"'
)
flags.DEFINE_string(
    'models_root_dir', None,
    'The directory to which the resulting model files should be written.')
flags.DEFINE_integer(
    'train_steps', 150000,
    'How many training steps each model shall get.')


# noinspection PyUnusedLocal
def main(argv):
    Path(FLAGS.models_root_dir).mkdir(parents=True, exist_ok=True)

    tfrecords_root_dir = Path(FLAGS.tfrecords_root_dir)

    working_directory = os.getcwd()
    script_path = ""

    if not working_directory.__contains__("ddsp_scripts"):
        script_path = working_directory + "/ddsp_scripts"
    else:
        script_path = working_directory

    print("script_path: " + script_path)

    for song_dir in tfrecords_root_dir.iterdir():
        if not song_dir.is_dir():
            continue

        param_s_save_dir = FLAGS.models_root_dir + "/" + song_dir.stem
        param_t_train_steps = str(song_dir)
        param_p_file_pattern = FLAGS.tfrecords_path_and_name_pattern
        command = [
            "sh",
            script_path + "/train_ae.sh",
            "-s", param_s_save_dir,
            "-t", param_t_train_steps,
            "-p", param_p_file_pattern,
            "-e", str(FLAGS.train_steps),
            "-h", str("True")
        ]

        # print("these files fulfill the tfrecord files pattern:")
        # for file in song_dir.glob(pattern=param_p_file_pattern):
        #     print(str(file))

        print(f"Executing command: {' '.join(command)}")
        subprocess.call(command)

    print(f"Finished processing all tfrecord files")


if __name__ == '__main__':
    flags.mark_flags_as_required(['tfrecords_root_dir', 'tfrecords_path_and_name_pattern', 'models_root_dir'])
    app.run(main)
