from pathlib import Path
from tensorflow.python.ops.numpy_ops import np_config

import numpy as np

from absl import flags, app, logging

from ddsp_scripts import csv_util


FLAGS = flags.FLAGS

source_csv_folder_help: str = 'the source CSV folder'
flags.DEFINE_string('source_csv_folder', None, source_csv_folder_help)
source_file_pattern_help: str = 'the pattern of the source file names'
flags.DEFINE_string('source_file_pattern', None, source_file_pattern_help)
parameter_names_help: str = 'the name of the parameters to evaluate'
flags.DEFINE_string('parameter_names', None, parameter_names_help)


def main(argv):
    source_csv_folder_str: str = FLAGS.source_csv_folder
    if source_csv_folder_str is None:
        message = ('Missing mandatory parameter "--source_csv_folder <PATH>". Help message: %s '
                   % source_csv_folder_help)
        logging.error(message)
        raise ValueError(message)
    source_file_pattern = FLAGS.source_file_pattern
    if source_file_pattern is None:
        message = ('Missing mandatory parameter "--source_file_pattern <PATTERN>". Help message: %s '
                   % source_file_pattern_help)
        logging.error(message)
        raise ValueError(message)
    parameter_names_str: str = FLAGS.parameter_names
    if parameter_names_str is None:
        message = ('Missing mandatory parameter "--parameter_names <PARAMETER1[,PARAMETER2],...>". '
                   + 'Help message: ' + parameter_names_help)
        logging.error(message)
        raise ValueError(message)

    np_config.enable_numpy_behavior()
    source_csv_folder: Path = Path(source_csv_folder_str)
    parameter_names: [str] = parameter_names_str.split(',')

    max_values: {str, np.float32} = {}
    min_values: {str, np.float32} = {}
    for parameter_name in parameter_names:
        max_values[parameter_name] = np.finfo('d').min
        min_values[parameter_name] = np.finfo('d').max

    for source_file in source_csv_folder.glob(source_file_pattern):
        csv_content, _ = csv_util.load_csv_to_features(str(source_file))
        for parameter_name in parameter_names:
            parameter: np.ndarray = csv_content[parameter_name]
            max = parameter.max().numpy()
            min = parameter.min().numpy()
            if max > max_values[parameter_name]:
                max_values[parameter_name] = max
            if min < min_values[parameter_name]:
                min_values[parameter_name] = min

    message: str = 'Parameter %s has values in range [%f, %f]'
    for parameter_name in parameter_names:
        logging.info(message, parameter_name, min_values[parameter_name], max_values[parameter_name])


if __name__ == "__main__":
    app.run(main)
