from absl import flags, app
from average_spectrogram_calculation import calculate_average_spectrogram

# supported by this site:
# https://stackoverflow.com/questions/31584445/averaging-multiple-images-in-python


FLAGS = flags.FLAGS
flags.DEFINE_string(
    'IMAGES_BASE_DIR', None,
    'The directory path where to start searching the images',
    required=True)
flags.DEFINE_string(
    'IMAGE_FILES_PATTERN', None,
    'Pattern for source images, may include subdirectories',
    required=True)
flags.DEFINE_string(
    'TARGET_FILE_NAME', None,
    'The path and name of the created image',
    required=True)


def main(argv):
    # images_base_dir = Path(FLAGS.IMAGES_BASE_DIR)
    images_base_dir = FLAGS.IMAGES_BASE_DIR
    image_files_pattern = FLAGS.IMAGE_FILES_PATTERN
    target_file_name = FLAGS.TARGET_FILE_NAME

    print('running with parameters' +
          '\nIMAGES_BASE_DIR = ', images_base_dir,
          '\nIMAGE_FILES_PATTERN = ', image_files_pattern,
          '\nTARGET_FILE_NAME = ', target_file_name)
    calculate_average_spectrogram(images_base_dir, image_files_pattern, target_file_name)


if __name__ == '__main__':
    app.run(main)
