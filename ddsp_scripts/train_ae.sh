#!/bin/bash
# call as
# nohup bash ddsp_scripts/train_ae.sh -m <mode> -e <train_steps> -s <model_save_dir> -t <tf_files_dir> -p <tf_files pattern> > <logs_dir>/train_ae.sh.`date +"%Y-%m-%d_%H-%M"`.info.log 2>> log/train_ae.sh.`date +"%Y-%m-%d_%H-%M"`.error.log &

# read parameters
while getopts e:h:p:s:t: option
do
  # shellcheck disable=SC2220
  case "${option}"
  in
    e)train_steps=${OPTARG};;
    h)hypertune=${OPTARG};;
    p)file_pattern=${OPTARG};;
    s)save_dir=${OPTARG};;
    t)tf_files_dir=${OPTARG};;
  esac
done

# check parameters
run=true

if ${train_steps+"false"}
then
  echo "parameter -e <train_steps> must be given. Good value: 150000"
  run=false
fi
if ${hypertune+"false"}
then
  hypertune="False"
fi
if ${file_pattern+"false"}
then
  echo "parameter -p <tf_files pattern> must be given"
  run=false
fi
if ${save_dir+"false"}
then
  echo "parameter -s <model_save_dir> must be given"
  run=false
fi
if ${tf_files_dir+"false"}
then
  echo "parameter -t <tf_files_dir> must be given"
  run=false
fi

if ! $run
then
  echo "At least one parameter is wrong or  missing. Do not train DDSP."
  echo "Exit."
  exit 0
fi

tfrecord_file_pattern="$tf_files_dir$file_pattern"
echo "running with parameters"
echo "save_dir              : $save_dir"
echo "tf_files_dir          : $tf_files_dir"
echo "file_pattern          : $file_pattern"
echo "tfrecord_file_pattern : TFRecordProvider.file_pattern='$tfrecord_file_pattern'"

# create model save dir if not exists
if [ ! -d "$save_dir" ];
then
  mkdir "$save_dir"
fi

if [ ! -d "$save_dir" ];
then
  echo "ERROR: model_save_dir \"$save_dir\" does not exist and could not be created!"
  exit 1
fi

# get path where this script is located
script_path="${BASH_SOURCE}"

# resolve all links in path to get hard path
while [ -L "${script_path}" ]; do
  script_dir="$(cd -P "$(dirname "${script_path}")" >/dev/null 2>&1 && pwd)"
  script_path="$(readlink "${script_path}")"
  [[ ${script_path} != /* ]] && script_path="${script_dir}/${script_path}"
done
script_path="$(readlink -f "${script_path}")"
script_dir="$(cd -P "$(dirname -- "${script_path}")" >/dev/null 2>&1 && pwd)"

# ch one directory up
root_dir="$(cd -P "$(dirname -- "${script_dir}")" >/dev/null 2>&1 && pwd)"

cd -P "$(dirname -- "${script_path}")"

export PYTHONPATH=$root_dir,$script_dir

echo \
   "ddsp_run" \
      "--alsologtostderr" \
      "--save_dir=\"$save_dir\"" \
      "--hypertune=\"$hypertune\"" \
      "--early_stop_loss_value=4.5" \
      "--gin_file=models/ae.gin" \
      "--gin_file=datasets/tfrecord.gin" \
      "--gin_param=\"TFRecordProvider.file_pattern='$tfrecord_file_pattern'\"" \
      "--gin_param=\"batch_size=16\"" \
      "--gin_param=\"train_util.train.num_steps=$train_steps\"" \
      "--gin_param=\"train_util.train.steps_per_save=1000\"" \
      "--gin_param=\"trainers.Trainer.checkpoints_to_keep=10\""


ddsp_run \
  --alsologtostderr \
  --save_dir="$save_dir" \
  --hypertune="$hypertune" \
  --early_stop_loss_value=4.5 \
  --gin_file=models/ae.gin \
  --gin_file=datasets/tfrecord.gin \
  --gin_param="TFRecordProvider.file_pattern='$tfrecord_file_pattern'" \
  --gin_param="batch_size=16" \
  --gin_param="train_util.train.num_steps=$train_steps" \
  --gin_param="train_util.train.steps_per_save=1000" \
  --gin_param="trainers.Trainer.checkpoints_to_keep=10"
