import subprocess
import sys
import time

from pathlib import Path
from os.path import exists as file_exists


def perform_resynthesis(resynthesis_script_name: str,
                        model_dir: str,
                        source_audio_file: Path,
                        target_audio_file: str,
                        checkpoint_name: str,
                        trials: int,
                        max_trials: int):

    command = [
        'python', resynthesis_script_name,
        '--MODE', 'resynthesize',
        '--OVERWRITE', False,
        '--MODEL_DIR', model_dir,
        '--SOURCE_AUDIO_FILE', str(source_audio_file),
        '--TARGET_AUDIO_FILE', target_audio_file
    ]

    if checkpoint_name is not None:
        command.extend(['--CHECKPOINT_NAME', checkpoint_name])

    print(f"Executing command: {' '.join(command)}", flush=True)
    subprocess.call(command)
    time.sleep(5)

    msg = 'Audio synthesis of ' + target_audio_file
    if not file_exists(target_audio_file) and trials < max_trials:
        msg = msg + ' failed. Trials: ' + str(trials) + '. Retrying after 5sec.'
        print(msg, flush=True)
        print(msg, flush=True, file=sys.stderr)
        time.sleep(5)
    elif not file_exists(target_audio_file) and trials == max_trials:
        msg = msg + ' failed. Trials: ' + str(trials) + '. Giving up!'
        print(msg, flush=True)
        print(msg, flush=True, file=sys.stderr)
        time.sleep(1)
    else:
        msg = msg + ' successful. Trials: ' + str(trials)
        print(msg, flush=True)
        time.sleep(1)
