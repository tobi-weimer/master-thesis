import subprocess
from builtins import print
from pathlib import Path

# noinspection PyPackageRequirements
from absl import flags, app

FLAGS = flags.FLAGS
flags.DEFINE_string(
    'tfrecords_root_dir', None,
    'The directory containing the tfrecord files.')
flags.DEFINE_boolean(
    'create_tfrecords_if_necessary', None,
    'shall missing tfrecord files be created?.')
flags.DEFINE_string(
    'audio_root_dir', None,
    'The directory containing the source audio files.')
flags.DEFINE_string(
    'models_root_dir', None,
    'The directory to which the resulting model files should be written.')
flags.DEFINE_integer(
    'train_steps', 3000,
    'How many training steps each model shall get.')


# noinspection PyUnusedLocal
def main(argv):
    models_root_dir = FLAGS.models_root_dir
    tfrecords_root_dir = FLAGS.tfrecords_root_dir

    Path(tfrecords_root_dir).mkdir(parents=True, exist_ok=True)

    Path(models_root_dir).mkdir(parents=True, exist_ok=True)

    if FLAGS.create_tfrecords_if_necessary is True:
        print("creating tfrecords")
        tfrecords = Path(tfrecords_root_dir + "/all_stems")

        audio_root_dir = FLAGS.audio_root_dir
        input_audio_filepattern = f"{audio_root_dir}/*/*STEMS/*half1.wav"

        output_tfrecord_path = f"{str(tfrecords)}.tfrecord"

        command = [
            'ddsp_prepare_tfrecord',
            f"--input_audio_filepatterns={input_audio_filepattern}",
            f"--output_tfrecord_path={output_tfrecord_path}",
            '--num_shards=250',  # yields files of approx. 200mb
            '--alsologtostderr'
        ]
        print(f"Executing command: {' '.join(command)}")
        subprocess.call(command)

        print("starting training")

        working_directory = os.getcwd()
        script_path = ""

        if not working_directory.__contains__("ddsp_scripts"):
            script_path = working_directory + "/ddsp_scripts"
        else:
            script_path = working_directory

        print("script_path: " + script_path)

        param_s_save_dir = models_root_dir
        param_t_tf_files_dir = tfrecords_root_dir + "/"
        param_p_file_pattern = "all_stems.tfrecord*"
        command = [
            "sh",
            script_path + "/train_ae.sh",
            "-s", param_s_save_dir,
            "-t", param_t_tf_files_dir,
            "-p", param_p_file_pattern,
            "-e", str(FLAGS.train_steps),
            "-h", str("True")
        ]
        print(f"Executing command: {' '.join(command)}")
        subprocess.call(command)
        pass
    print(f"Finished processing all tfrecord files")


if __name__ == '__main__':
    flags.mark_flags_as_required([
        'tfrecords_root_dir',
        'models_root_dir'
    ])
    app.run(main)
