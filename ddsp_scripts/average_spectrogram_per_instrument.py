from absl import flags, app
from get_instrument_set import get_instrument_set
from average_spectrogram_calculation import calculate_average_spectrogram

FLAGS = flags.FLAGS
flags.DEFINE_string(
    'IMAGES_BASE_DIR', None,
    'The directory path where to start searching the images',
    required=True)
flags.DEFINE_string(
    'IMAGE_FILES_PATTERN', None,
    'Pattern for source images, may include subdirectories.' +
    ' <<INSTRUMENT>> will be replaced with the instrument names.',
    required=True)
flags.DEFINE_string(
    'TARGET_FILE_NAME_PREFIX', None,
    'The path and name prefix of the created image',
    required=True)
flags.DEFINE_string(
    'YAML_FILES_FOLDER', None,
    'The directory where the .yaml-files are stored.')


def main(argv):
    # images_base_dir = Path(FLAGS.IMAGES_BASE_DIR)
    images_base_dir = FLAGS.IMAGES_BASE_DIR
    image_files_pattern_base = FLAGS.IMAGE_FILES_PATTERN
    target_file_name_prefix = FLAGS.TARGET_FILE_NAME_PREFIX
    yaml_files_folder: str = FLAGS.YAML_FILES_FOLDER

    if target_file_name_prefix.endswith(".png"):
        target_file_name_prefix = target_file_name_prefix[:-4]

    print('running with parameters' +
          '\nIMAGES_BASE_DIR =', images_base_dir,
          '\nIMAGE_FILES_PATTERN =', image_files_pattern_base,
          '\nYAML_FILES_FOLDER', yaml_files_folder,
          '\nTARGET_FILE_NAME_PREFIX =', target_file_name_prefix)

    instrument_set = get_instrument_set(yaml_files_folder)

    if not target_file_name_prefix.endswith("_"):
        target_file_name_prefix = target_file_name_prefix + "_"

    for instrument in instrument_set:
        print('now processíng instrument', instrument)
        instrument = instrument.replace(' ', '_')
        target_file_name = target_file_name_prefix + instrument + ".png"
        image_files_pattern = image_files_pattern_base
        image_files_pattern = image_files_pattern.replace('<<INSTRUMENT>>', instrument)

        print('calling calculate_average_spectrogram with parameters' +
              '\nIMAGES_BASE_DIR =', images_base_dir,
              '\nIMAGE_FILES_PATTERN =', image_files_pattern,
              '\nTARGET_FILE_NAME =', target_file_name)

        calculate_average_spectrogram(images_base_dir, image_files_pattern, target_file_name)


if __name__ == '__main__':
    app.run(main)
