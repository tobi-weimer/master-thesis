import os

from absl import flags, app, logging
from pathlib import Path
from get_instrument_set import get_instrument_set
from perform_resynthesis import perform_resynthesis

"""
example call:
nohup python resynthesize_audio_single.py --RESYNTHESIZE_MULTIPLE=True \
 --AUDIO_BASE_DIR=/home/tweimer/scratch/MedleyDB_wav_halves --AUDIO_FILES_PATTERN=**/*_STEM_*half1* \
--MODELS_DIR=/home/tweimer/scratch/MedleyDB_single_models_2k_steps \
> /home/tweimer/logs/resynthesizeAudio.`date +"%Y-%m-%d_%H-%M"`.info.log \
2>/home/tweimer/logs/resynthesizeAudio.`date +"%Y-%m-%d_%H-%M"`.error.log &
"""


FLAGS = flags.FLAGS
flags.DEFINE_boolean(
    'OVERWRITE', False,
    'Skip files if target file already exists?')
flags.DEFINE_string(
    'SOURCE_AUDIO_BASE_DIR', None,
    'The base dir for all input audio files')
flags.DEFINE_string(
    'AUDIO_FILES_PATTERN', None,
    'Pattern for source audio files and relative path from base dir')
flags.DEFINE_string(
    'AUDIO_FILES_HALF', None,
    'use first or second half of audio file as source')
flags.DEFINE_string(
    'MODELS_BASE_DIR', None,
    'Directory where all models are stored in subfolders')
flags.DEFINE_string(
    'CHECKPOINT_NAME', None,
    'The name of the checkpoints to use')
flags.DEFINE_string(
    'YAML_FILES_FOLDER', None,
    'The directory where the .yaml-files are stored.')


def main(argv):
    logging.info('Running %s with flags', argv[0])
    my_flags = FLAGS.get_flags_for_module(argv[0])
    for flag in my_flags:
        if flag.value is not None:
            logging.info('--%s %s', flag.name, str(flag.value))

    overwrite = FLAGS.OVERWRITE
    models_base_dir = FLAGS.MODELS_BASE_DIR
    source_audio_base_dir = Path(FLAGS.SOURCE_AUDIO_BASE_DIR)
    audio_files_pattern = FLAGS.AUDIO_FILES_PATTERN
    audio_files_half = FLAGS.AUDIO_FILES_HALF
    checkpoint_name = FLAGS.CHECKPOINT_NAME
    yaml_files_folder = FLAGS.YAML_FILES_FOLDER

    trials: int
    max_trials: int = 3

    instrument_set = get_instrument_set(yaml_files_folder)

    for instrument in instrument_set:

        instrument = str(instrument).replace(" ", "_")

        model_dir = models_base_dir + instrument

        if audio_files_half is not None:
            current_audio_files_pattern = audio_files_pattern + instrument + "_half" + audio_files_half + ".wav"
        else:
            current_audio_files_pattern = audio_files_pattern + instrument + ".wav"

        print('now resynthesizing with\ninstrument: ', instrument,
              '\nmodel_dir: ', model_dir,
              '\ncurrent_audio_files_pattern: ', current_audio_files_pattern,
              sep=None, flush=True)

        for source_audio_file in source_audio_base_dir.glob(current_audio_files_pattern):
            trials = 0

            target_audio_file = (str(source_audio_file.parent) + '/' +
                                 source_audio_file.stem + '_generated_instrument_models.wav')

            if not overwrite and os.path.exists(target_audio_file):
                print('target audio file', target_audio_file, 'already exists. Skip re-creating.', flush=True)
                continue

            if overwrite and os.path.exists(target_audio_file):
                print('target audio file', target_audio_file, 'will be deleted and recreated.', flush=True)
                os.remove(target_audio_file)

            while trials < max_trials and not os.path.exists(target_audio_file):
                trials = trials + 1
                perform_resynthesis(str(argv[0]).replace('resynthesize_audio_per_instrument.py', 'audio_worker.py'),
                                    model_dir,
                                    source_audio_file,
                                    target_audio_file,
                                    checkpoint_name,
                                    trials, max_trials)


if __name__ == '__main__':
    app.run(main)
