# import os
# os.environ["CUDA_VISIBLE_DEVICES"] = '0'

import numpy as np
import pandas as pd
import tensorflow as tf

from absl import logging

csv_postfix: str = '.csv'


def load_csv_to_features(source_csv_file: str):
    if not source_csv_file.lower().endswith(csv_postfix):
        my_source_csv_file = source_csv_file + csv_postfix
    else:
        my_source_csv_file = source_csv_file

    csv_df = pd.read_csv(my_source_csv_file)

    time_steps = np.array(csv_df['f0_hz']).shape[0]
    start = 0
    end = time_steps
    logging.info('loaded features from index {s} to index {e}'.format(s=start, e=end))

    audio_features = dict({})
    z_columns_count: int = 0
    for data_series in csv_df:
        key = str(data_series)
        if key.startswith('Unnamed: 0'):
            csv_df.pop(key)
        elif not key.__contains__('_col_'):
            audio_features[key] = np.array(csv_df.pop(key))[start:end]
        elif key.startswith('audio'):
            continue
        elif key.startswith('z'):
            z_columns_count = z_columns_count + 1

    for key in audio_features.keys():
        audio_features[key] = tf.reshape(
            audio_features[key].astype(np.float32),
            shape=(1, audio_features[key].shape[0], 1)
        )

    get_connected_columns_from_dataframe(column_name='z',
                                         columns_count=z_columns_count,
                                         audio_features=audio_features,
                                         dataframe=csv_df,
                                         start=start,
                                         end=end)
    z_shape = audio_features['z'].shape
    audio_features['z'] = tf.reshape(
        audio_features['z'].astype(np.float32),
        shape=(1, z_shape[0], z_shape[1])
    )

    if logging.level_debug():
        logging.debug('custom audio feature shapes:')
        for key in audio_features.keys():
            logging.debug('audio_features[%s].shape: %s',
                          key, audio_features.get(key).shape)

    return audio_features, time_steps


def get_connected_columns_from_dataframe(column_name, columns_count, audio_features, dataframe, start, end):
    column_names = get_column_names(column_name_prefix=column_name, number_of_columns=columns_count)
    temp_dataframe = pd.DataFrame()
    index: int = 0
    for column_name_iter in column_names:
        temp_dataframe.insert(loc=index, column=column_name_iter, value=dataframe.pop(column_name_iter))
        index = index + 1
    audio_features[column_name] = np.array(temp_dataframe)[start:end]


def save_features_to_csv(features, target_csv_file: str):
    if logging.level_debug():
        logging.debug('feature shapes:')
        for key in features.keys():
            logging.debug('features[%s].shape: %s',
                          key, features.get(key).shape)

    timestamps = features['f0_hz'].size
    features['f0_hz'] = tf.reshape(
        features['f0_hz'].astype(np.float32), shape=timestamps)
    features['f0_scaled'] = tf.reshape(
        features['f0_scaled'].astype(np.float32), shape=timestamps)
    features['loudness_db'] = tf.reshape(
        features['loudness_db'].astype(np.float32), shape=timestamps)
    features['ld_scaled'] = tf.reshape(
        features['ld_scaled'].astype(np.float32), shape=timestamps)
    features['z'] = tf.transpose(
        tf.reshape(features['z'].astype(np.float32),
                   shape=(timestamps, 16)))

    if logging.level_debug():
        logging.debug('feature shapes after conversion:')
        for key in features.keys():
            logging.debug('features[%s].shape: %s',
                          key, features.get(key).shape)

    stacked = np.stack(
        (features['f0_hz'],
         features['f0_scaled'],
         features['loudness_db'],
         features['ld_scaled']),
        axis=1)
    csv_df = pd.DataFrame(stacked, columns=['f0_hz',
                                            'f0_scaled',
                                            'loudness_db',
                                            'ld_scaled'])

    add_connected_columns_to_dataframe(dataframe=csv_df, columns_data=features, column_name='z')

    if not target_csv_file.lower().endswith(csv_postfix):
        my_target_csv_file = target_csv_file + csv_postfix
    else:
        my_target_csv_file = target_csv_file

    csv_df.to_csv(my_target_csv_file)

    return


def add_connected_columns_to_dataframe(dataframe: pd.DataFrame, columns_data, column_name: str):
    columns_names = get_column_names(column_name_prefix=column_name,
                                     number_of_columns=columns_data[column_name].shape.dims[0])
    stacked = np.stack(
        (columns_data[column_name]),
        axis=1)
    df = pd.DataFrame(stacked, columns=columns_names)
    for column_name in columns_names:
        values = df[column_name]
        dataframe.insert(loc=len(dataframe.columns), column=column_name, value=values)


def get_column_names(column_name_prefix: str, number_of_columns: int):
    columns = list()
    for x in range(number_of_columns):
        cur_column_name = '{column_name}_col_{index}'.format(column_name=column_name_prefix, index=x)
        columns.append(cur_column_name)

    return columns
