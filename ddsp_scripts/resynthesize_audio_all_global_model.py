import os
import subprocess
import sys
import time

from absl import flags, app, logging
from pathlib import Path
from os.path import exists as file_exists

"""
example call:
nohup python resynthesize_audio_all_global_model.py \
 --SOURCE_AUDIO_BASE_DIR=/home/tweimer/scratch/MedleyDB_wav_halves --AUDIO_FILES_PATTERN=**/*_STEM_*half2.wav \
--MODEL_DIR=/home/tweimer/scratch/MedleyDB_single_models_2k_steps \
> /home/tweimer/logs/resynthesizeAudio.`date +"%Y-%m-%d_%H-%M"`.info.log \
2>/home/tweimer/logs/resynthesizeAudio.`date +"%Y-%m-%d_%H-%M"`.error.log &
"""


FLAGS = flags.FLAGS
flags.DEFINE_boolean(
    'OVERWRITE', False,
    'Skip files if target file already exists?')
flags.DEFINE_string(
    'SOURCE_AUDIO_BASE_DIR', None,
    'The base dir for all input audio files')
flags.DEFINE_string(
    'AUDIO_FILES_PATTERN', None,
    'Pattern for source audio files and relative path from base dir')
flags.DEFINE_string(
    'MODEL_DIR', None,
    'Directory where the model is stored')
flags.DEFINE_boolean(
    'TARGET_AUDIO_IN_SOURCE_FOLDER', False,
    'Save the target audio in the folder where the source audio is found?')
flags.DEFINE_string(
    'TARGET_AUDIO_DIR', None,
    'The directory to store the generated audio files.')
flags.DEFINE_string(
    'TARGET_FILENAME_POSTFIX', None,
    'Postfix for target audio file name, inserted before .wav ending')
flags.DEFINE_string(
    'CHECKPOINT_NAME', 'ckpt-1000',
    'The name of the checkpoints to use')


def main(argv):

    logging.info('Running %s with flags', argv[0])
    my_flags = FLAGS.get_flags_for_module(argv[0])
    for flag in my_flags:
        if flag.value is not None:
            logging.info('--%s %s', flag.name, str(flag.value))

    overwrite = FLAGS.OVERWRITE
    model_dir = FLAGS.MODEL_DIR
    source_audio_base_dir = Path(FLAGS.SOURCE_AUDIO_BASE_DIR)
    audio_files_pattern = FLAGS.AUDIO_FILES_PATTERN
    target_audio_in_source_folder = FLAGS.TARGET_AUDIO_IN_SOURCE_FOLDER
    target_audio_dir = FLAGS.TARGET_AUDIO_DIR
    target_filename_postfix = FLAGS.TARGET_FILENAME_POSTFIX
    checkpoint_name = FLAGS.CHECKPOINT_NAME

    trials: int
    max_trials: int = 3

    audio_files = source_audio_base_dir.glob(audio_files_pattern)
    for source_audio_file in audio_files:
        trials = 0

        if target_audio_in_source_folder:
            target_audio_file = str(source_audio_file.parent) + "/" + source_audio_file.stem + target_filename_postfix + '.wav'
        else:
            target_audio_file = target_audio_dir + "/" + source_audio_file.stem + target_filename_postfix + '.wav'

        if not overwrite and os.path.exists(target_audio_file):
            print('target audio file', target_audio_file, 'already exists. Skip re-creating.', flush=True)
            continue

        if overwrite and os.path.exists(target_audio_file):
            print('target audio file', target_audio_file, 'will be deleted and recreated.', flush=True)
            os.remove(target_audio_file)

        while trials < max_trials and not os.path.exists(target_audio_file):
            trials = trials + 1

            command = [
                'python', str(argv[0]).replace('all_global_model.py', 'single.py'),
                '--MODEL_DIR', model_dir,
                '--SOURCE_AUDIO_FILE', str(source_audio_file),
                '--TARGET_AUDIO_FILE', target_audio_file,
                '--CHECKPOINT_NAME', checkpoint_name
            ]
            print(f"Executing command: {' '.join(command)}", flush=True)
            subprocess.call(command)
            time.sleep(5)

            msg = 'Audio synthesis of ' + target_audio_file
            if not file_exists(target_audio_file) and trials < max_trials:
                msg = msg + ' failed. Trials: ' + str(trials) + '. Retrying after 5sec.'
                print(msg, flush=True)
                print(msg, flush=True, file=sys.stderr)
                time.sleep(5)
            elif not file_exists(target_audio_file) and trials == max_trials:
                msg = msg + ' failed. Trials: ' + str(trials) + '. Giving up!'
                print(msg, flush=True)
                print(msg, flush=True, file=sys.stderr)
                time.sleep(1)
            else:
                msg = msg + ' successful. Trials: ' + str(trials)
                print(msg, flush=True)
                time.sleep(1)


if __name__ == '__main__':
    app.run(main)
