import glob
import os
import numpy
from PIL import Image


def calculate_average_spectrogram(images_base_dir, image_files_pattern, target_file_name):
    # Access all PNG files in directory
    all_image_files = glob.glob(os.path.join(images_base_dir, image_files_pattern), recursive=True)

    if len(all_image_files) == 0:
        print('\nNo matches found for pattern in directory!\nDIRECTORY: ' + images_base_dir +
              '\nPATTERN: ' + image_files_pattern)
        return

    image_sizes = dict({})
    max_counter: int = 0
    most_important_size = tuple()

    print('List all matching image files:')
    for image_file in all_image_files:
        print(image_file)
        image = Image.open(image_file)
        image_size = image.size
        if image_sizes.__contains__(image_size):
            image_sizes[image_size] = image_sizes[image_size] + 1
        else:
            image_sizes[image_size] = 1

        if image_sizes[image_size] > max_counter:
            max_counter = image_sizes[image_size]
            most_important_size = image_size

    # Assuming all images are the same size, get dimensions of first image
    image_width = most_important_size[0]
    image_height = most_important_size[1]

    images_count: int = 0
    images_skipped_count: int = 0

    # Create a numpy array of floats to store the average in 3 channel RGB mode
    image_sum = numpy.zeros((image_height, image_width, 3), dtype=numpy.float128)

    # Build up average pixel intensities, casting each image as an aray of floats
    for image_file in all_image_files:
        print('now processing ', image_file)
        image = Image.open(image_file)

        # TODO handle different spectrogram sizes - or generate spectrograms with a fixed size
        if image_width != image.width or image_height != image.height:
            print('expected image format {exp_w:d}x{exp_h:d}, got {i_w:d}x{i_h:d}.'.format(
                exp_w=image_width, exp_h=image_height, i_w=image.width, i_h=image.height))
            images_skipped_count = images_skipped_count + 1
            continue
        if image.mode != 'RGB':
            image = image.convert('RGB')
        # noinspection PyTypeChecker
        image_array = numpy.array(image, dtype=numpy.float128)
        image_sum = image_sum + image_array
        images_count = images_count + 1
        print("processed image file " + image_file)

    print("processed " + str(images_count) + " images.")
    if images_skipped_count != 0:
        print("Attention: skipped " + str(images_skipped_count) + " images!")

    image_average = image_sum / images_count

    # Round values in array and cast as 8-bit integer
    image_average = numpy.array(numpy.round(image_average), dtype=numpy.uint8)

    # Generate, save and preview final image
    # noinspection PyTypeChecker
    out = Image.fromarray(image_average, mode='RGB')
    out.save(target_file_name)
