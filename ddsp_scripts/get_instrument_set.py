from pathlib import Path
import yaml


def get_instrument_set(yaml_files_folder: str):
    yaml_files_folder = Path(yaml_files_folder)

    instrument_set = set()

    for yaml_file in yaml_files_folder.iterdir():
        with yaml_file.open() as my_yaml_file:
            content = yaml.safe_load(my_yaml_file)

            for stem_entry in content["stems"]:
                instrument = content["stems"][stem_entry]["instrument"]
                if type(instrument) is list:
                    instrument = instrument[0]
                instrument_set.add(instrument)

    return instrument_set


def get_instrument_set_replaced(yaml_files_folder: str):
    instrument_set = get_instrument_set(yaml_files_folder)
    result_instrument_set = set()
    for instrument in instrument_set:
        instrument = str(instrument).replace(" ", "_")
        instrument = str(instrument).replace("/", "_")
        result_instrument_set.add(instrument)

    return result_instrument_set
