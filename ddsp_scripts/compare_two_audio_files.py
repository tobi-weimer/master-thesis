import librosa
import matplotlib.pyplot as plt
from dtw import accelerated_dtw

# source:
# https://stackoverflow.com/questions/58971042/how-do-i-compare-and-visualise-two-wav-files-in-python-3

# Loading audio files
y1, sr1 = librosa.load('/home/tweimer/scratch/Tests/STEM_04_clean_electric_guitar_half1_decoded.wav')
y2, sr2 = librosa.load('/home/tweimer/scratch/Tests/STEM_04_clean_electric_guitar_half1_resynthesized.wav')

# Showing multiple plots using subplot
plt.subplot(1, 2, 1)
mfcc1 = librosa.feature.mfcc(y=y1, sr=sr1)   # Computing MFCC values
librosa.display.specshow(mfcc1)

plt.subplot(1, 2, 2)
mfcc2 = librosa.feature.mfcc(y=y2, sr=sr2)
librosa.display.specshow(mfcc2)

min_dist, cost, cost_accumulated, path = accelerated_dtw(mfcc1.T, mfcc2.T, 'euclidean')
print("The normalized distance between the two : ", min_dist)   # 0 for similar audios

plt.imshow(cost.T, origin='lower', cmap=plt.get_cmap('gray'), interpolation='nearest')
plt.plot(path[0], path[1], 'w')   # creating plot for DTW

plt.show()
