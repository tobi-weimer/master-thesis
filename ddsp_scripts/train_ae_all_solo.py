import os
import subprocess
from builtins import print
from pathlib import Path

# noinspection PyPackageRequirements
from absl import flags, app

FLAGS = flags.FLAGS
flags.DEFINE_string(
    'tfrecords_root_dir', None,
    'The directory containing the tfrecords files to process.')
flags.DEFINE_string(
    'models_root_dir', None,
    'The directory to which the resulting model files should be written.')
flags.DEFINE_integer(
    'train_steps', 150000,
    'How many training steps each model shall get.')


# noinspection PyUnusedLocal
def main(argv):
    Path(FLAGS.models_root_dir).mkdir(parents=True, exist_ok=True)

    working_directory = os.getcwd()
    script_path = ""

    if not working_directory.__contains__("ddsp_scripts"):
        script_path = working_directory + "/ddsp_scripts"
    else:
        script_path = working_directory

    tfrecords_root_dir = Path(FLAGS.tfrecords_root_dir)

    for tfrecord_file in tfrecords_root_dir.glob("**/*STEM*half1.tfrecord-00000-of-00001"):
        print(tfrecord_file)

        print(tfrecord_file.stem)
        print(tfrecord_file.parent)

        param_s_save_dir = FLAGS.models_root_dir + "/" + tfrecord_file.stem
        param_t_tf_files_dir = str(tfrecord_file.parent)
        param_p_file_pattern = "/*" + str(tfrecord_file.name) + "*"
        command = [
            "sh",
            script_path + "/train_ae.sh",
            "-s", param_s_save_dir,
            "-t", param_t_tf_files_dir,
            "-p", param_p_file_pattern,
            "-e", str(FLAGS.train_steps),
            "-h", str("True")
        ]
        print(f"Executing command: {' '.join(command)}")
        subprocess.call(command)
        pass
    print(f"Finished processing all tfrecord files")


if __name__ == '__main__':
    flags.mark_flags_as_required(['tfrecords_root_dir', 'models_root_dir'])
    app.run(main)
