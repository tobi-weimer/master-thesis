import re

from absl import flags, app, logging
from pathlib import Path
from audio_worker import encode_audio
from audio_worker import ModelNotLoadedException

"""
example call:

nohup python encode_multiple_audio_to_csv.py \
--OVERWRITE False \
--MODEL_BASE_DIR /PATH/TO/INSTRUMENT_MODELS \
--SOURCE_AUDIO_BASE_DIR /PATH/TO/WAV_FILES \
--SOURCE_AUDIO_PATTERN AUDIO*FILES*PATTERN
--TARGET_CSV_DIR /PATH/TO/CSV_TARGET_DIR \
--CHECKPOINT_NAME ckpt-9000 
> /PATH/TO/LOGFILE_WITH_TIMESTAMP.`date +"%Y-%m-%d_%H-%M"`.info.log \
2>/PATH/TO/LOGFILE_WITH_TIMESTAMP.`date +"%Y-%m-%d_%H-%M"`.error.log &

"""


FLAGS = flags.FLAGS
flags.DEFINE_string(
    'MODEL_BASE_DIR', None,
    'Directory where the models are stored.')
flags.DEFINE_string(
    'SOURCE_AUDIO_BASE_DIR', None,
    'The (base) path of the audio files to be processed.')
flags.DEFINE_string(
    'SOURCE_AUDIO_PATTERN', None,
    'Pattern for source audio files including relative path ' +
    'from SOURCE_AUDIO_BASE_DIR')
flags.DEFINE_string(
    'AUDIO_FILES_EXCLUDE_PATTERN', None,
    'Pattern for source audio files to be excluded')
flags.DEFINE_string(
    'TARGET_CSV_DIR', None,
    'The path of the created csv files for storing. Name and extensions ' +
    'will be created, so the files will be ' +
    'TARGET_CSV_DIR/SOURCE_AUDIO_FILE.features.csv, ' +
    'TARGET_CSV_DIR/SOURCE_AUDIO_FILE.audio.csv and ' +
    'TARGET_CSV_DIR/SOURCE_AUDIO_FILE.z.csv.')


def main(argv):
    logging.info('Running %s with flags', argv[0])
    my_flags = FLAGS.get_flags_for_module(argv[0])
    for flag in my_flags:
        if flag.value is not None:
            logging.info('--%s %s', flag.name, str(flag.value))

    overwrite: bool = FLAGS.OVERWRITE
    model_base_dir: str = FLAGS.MODEL_BASE_DIR
    source_audio_base_dir: Path = Path(FLAGS.SOURCE_AUDIO_BASE_DIR)
    source_audio_pattern: str = FLAGS.SOURCE_AUDIO_PATTERN
    audio_files_exclude_pattern = FLAGS.AUDIO_FILES_EXCLUDE_PATTERN
    target_csv_dir: str = FLAGS.TARGET_CSV_DIR
    checkpoint_name: str = FLAGS.CHECKPOINT_NAME

    found_files_counter: int = 0
    excluded_files_counter: int = 0
    processed_files_counter: int = 0
    files_with_errors: int = 0

    if target_csv_dir is not None:
        target_csv_dir_current = target_csv_dir

    for source_audio_file in source_audio_base_dir.glob(source_audio_pattern):
        found_files_counter = found_files_counter + 1

        source_audio_file_str: str = str(source_audio_file)
        if (audio_files_exclude_pattern is not None and
                re.search(audio_files_exclude_pattern, source_audio_file_str)):
            excluded_files_counter = excluded_files_counter + 1
            logging.info('Skipping file %s due to exclude pattern.',  source_audio_file_str)
            continue

        if 'STEM' in source_audio_file.stem:
            instrument_source = source_audio_file.stem
        else:
            instrument_source = source_audio_file.parent.stem

        try:
            instrument = re.search('STEM_(.+?)_half', str(instrument_source)).group(1)
        except AttributeError:
            logging.exception('instrument not defined in %s', instrument_source)
            continue

        if target_csv_dir is None:
            target_csv_dir_current = str(source_audio_file.parent)

        instrument = instrument[3:]
        model_dir = model_base_dir + '/' + instrument
        target_csv_file = target_csv_dir_current + '/' + source_audio_file.stem

        logging.info('Now processing %s with model %s', source_audio_file_str, model_dir)
        try:
            encode_audio(model_dir=model_dir,
                         source_audio_file=source_audio_file_str,
                         target_csv_file=target_csv_file,
                         overwrite=overwrite,
                         checkpoint_name=checkpoint_name)
            logging.info('Finished processing %s', str(source_audio_file))
            processed_files_counter = processed_files_counter + 1
        except ModelNotLoadedException as ex:
            logging.exception('Aborted processing %s due to ModelNotLoadedException.\n' +
                              'Error Message:\n%s',
                              str(source_audio_file_str), ex)
            files_with_errors = files_with_errors + 1

    logging.info('Processed %d files from %d found files. ' +
                 '%d files have had errors during processing.' +
                 '%d files have been excluded.',
                 processed_files_counter,
                 found_files_counter,
                 files_with_errors,
                 excluded_files_counter)


if __name__ == '__main__':
    app.run(main)
