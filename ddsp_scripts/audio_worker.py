import os
# os.environ["CUDA_VISIBLE_DEVICES"] = '0'

import ddsp.training
import gin
import librosa
import numpy as np
import time

from absl import flags, app, logging
from pathlib import Path
from os.path import exists as file_exists
from scipy.io import wavfile
from tensorflow.python.ops.numpy_ops import np_config

if __name__ == '__main__':
    import csv_util
else:
    from ddsp_scripts import csv_util

"""
example call:

nohup python audio_worker.py \
--MODE resynthesize
--OVERWRITE False \
--MODEL_DIR /PATH/TO/SPECIFIC/INSTRUMENT_MODEL \
--SOURCE_AUDIO_FILE /PATH/TO/SPECIFIC/WAV_FILE_INCLUDING_POSTFIX \
--TARGET_AUDIO_FILE /PATH/TO/SPECIFIC/WAV_FILE_INCLUDING_POSTFIX \
--CHECKPOINT_NAME ckpt-9000 
> /PATH/TO/LOGFILE_WITH_TIMESTAMP.`date +"%Y-%m-%d_%H-%M"`.info.log \
2>/PATH/TO/LOGFILE_WITH_TIMESTAMP.`date +"%Y-%m-%d_%H-%M"`.error.log &

"""


FLAGS = flags.FLAGS
flags.DEFINE_string(
    'MODE', 'resynthesize',
    'Available modes: "resynthesize", "encode" and "decode".')
flags.DEFINE_boolean(
    'OVERWRITE', False,
    'Skip files if target file already exists? - True or False.')
flags.DEFINE_string(
    'MODEL_DIR', None,
    'Directory where the models is stored.')
flags.DEFINE_string(
    'SOURCE_AUDIO_FILE', None,
    'The path and name of the audio file to be processed.')
flags.DEFINE_string(
    'SOURCE_CSV_FILE', None,
    'The path and name of the csv file to be decoded to audio, without ' +
    'postfix. Will expect .csv files SOURCE_CSV_FILE.audio.csv, ' +
    'SOURCE_CSV_FILE.features.csv and SOURCE_CSV_FILE.z.csv')
flags.DEFINE_string(
    'TARGET_AUDIO_FILE', None,
    'The path and name of the created audio file for storing.')
flags.DEFINE_string(
    'TARGET_CSV_FILE', None,
    'The path and name of the created csv files for storing. Extensions ' +
    'will be created, so the files will be TARGET_CSV_FILE.features.csv, ' +
    'TARGET_CSV_FILE.audio.csv and TARGET_CSV_FILE.z.csv. ' +
    'Default: MODEL_DIR/encode.')
flags.DEFINE_integer(
    'SAMPLERATE', 16000,
    'The samplerate of the generated audio')
flags.DEFINE_string(
    'CHECKPOINT_NAME', '',
    'The name of the checkpoints to use.')


def resynthesize_audio(model_dir: str,
                       source_audio_file: str,
                       target_audio_file: str = None,
                       samplerate: int = None,
                       overwrite: bool = False,
                       checkpoint_name: str = None):
    """
    :param checkpoint_name:
    :param model_dir: directory path to the model.
    :param source_audio_file: full path to the source audio file
    :param target_audio_file: full path to of target audio file.
        Default: model_dir + '/synthesized.wav'
    :param samplerate: the sample rate of the generated audio.
    :param overwrite: True if existing files shall be overwritten. Default: False
    """

    if target_audio_file is None:
        target_audio_file = model_dir + '/synthesized.wav'

    if not Path(target_audio_file).parent.exists():
        logging.error('target folder %s to save file does not exist!', str(Path(target_audio_file).parent))
        return

    if not overwrite and file_exists(target_audio_file):
        logging.info('target audio file %s already exists. Skip re-creating.',
                     target_audio_file)
        return

    ddsp.spectral_ops.reset_crepe()

    audio_features = load_and_prepare_audio_features(model_dir, source_audio_file)
    model = load_model_with_checkpoint(checkpoint_name, model_dir)

    # Run a batch of predictions.
    start_time = time.time()

    outputs = model(audio_features, training=False)
    audio_gen = model.get_audio_from_outputs(outputs)

    logging.info('Prediction took %.1f seconds' % (time.time() - start_time))

    save_audio_to_file(audio_gen, target_audio_file, sample_rate=samplerate)

    return


def encode_audio(model_dir: str,
                 source_audio_file: str,
                 target_csv_file: str = None,
                 overwrite: bool = False,
                 checkpoint_name: str = '') -> object:
    """
    :param checkpoint_name:
    :param model_dir: directory path to the model.
    :param source_audio_file: full path to the source audio file
    :param target_csv_file: full path to of target csv file.
        Default: model_dir + '/encoded.csv'
    :param overwrite: True if existing files shall be overwritten. Default: False
    """

    if target_csv_file is None:
        target_csv_file = model_dir + '/encoded'

    if not Path(target_csv_file).parent.exists():
        logging.error('target folder %s to save file does not exist!', str(Path(target_csv_file).parent))
        return

    if not overwrite and file_exists(target_csv_file):
        logging.info('target csv file %s already exists. Skip re-creating.', target_csv_file,)
        return

    # enable numpy related methods for tensors
    np_config.enable_numpy_behavior()
    ddsp.spectral_ops.reset_crepe()

    audio_features = load_and_prepare_audio_features(model_dir, source_audio_file)
    model = load_model_with_checkpoint(checkpoint_name, model_dir)

    # Run a batch of predictions.
    start_time = time.time()

    features = model.encode(audio_features, training=False)

    logging.info('Prediction took %.1f seconds', (time.time() - start_time))

    if logging.level_debug():
        logging.debug('feature shapes:')
        for key in features.keys():
            logging.debug('features[%s].shape: %s',
                          key, features.get(key).shape)

    features_to_save = {
        'f0_hz':       features['f0_hz'],
        'f0_scaled':   features['f0_scaled'],
        'loudness_db': features['loudness_db'],
        'ld_scaled':   features['ld_scaled'],
        'z':           features['z']
    }

    csv_util.save_features_to_csv(features_to_save, target_csv_file)

    return


def decode_audio(model_dir: str,
                 source_csv_file: str,
                 target_audio_file: str = None,
                 samplerate: int = 16000,
                 overwrite: bool = False,
                 checkpoint_name: str = '') -> object:
    """
    :param model_dir: directory path to the model.
    :param source_csv_file: full path to the source csv file
    :param target_audio_file: full path to of target audio file. Format is always WAV.
        Default: model_dir + '/decoded.wav'
    :param samplerate: samplerate for the generated audio.
        Default: 16,000
    :param overwrite: True if existing files shall be overwritten. Default: False
    :param checkpoint_name:
    """

    if target_audio_file is None:
        target_audio_file = model_dir + '/decoded.wav'

    if not Path(target_audio_file).parent.exists():
        logging.error('target folder %s to save file does not exist!', str(Path(target_audio_file).parent))
        return

    if not overwrite and file_exists(target_audio_file):
        logging.info('target audio file %s already exists. Skip re-creating.', target_audio_file)
        return

    # enable numpy related methods for tensors
    np_config.enable_numpy_behavior()
    ddsp.spectral_ops.reset_crepe()

    audio_features, time_steps = csv_util.load_csv_to_features(source_csv_file)

    load_and_adjust_gin_configuration(model_dir, source_csv_file, time_steps=time_steps)

    ddsp.spectral_ops.reset_crepe()

    model = load_model_with_checkpoint(checkpoint_name, model_dir)
    ddsp.spectral_ops.reset_crepe()

    # Run a batch of predictions.
    start_time = time.time()

    audio = model.decode(audio_features, training=False)

    logging.info('Prediction took %.1f seconds' % (time.time() - start_time))

    save_audio_to_file(audio, target_audio_file, samplerate)

    return


def load_and_prepare_audio_features(model_dir: str, source_audio_file: str):
    audio, audio_features = load_and_prepare_audio(source_audio_file)
    n_samples, time_steps = load_and_adjust_gin_configuration(
        model_dir, source_audio_file, audio=audio)
    # Trim all input vectors to correct lengths
    for key in ['f0_hz', 'f0_confidence', 'loudness_db']:
        audio_features[key] = audio_features[key][:time_steps]
    audio_features['audio'] = audio_features['audio'][:, :n_samples]
    return audio_features


def load_and_prepare_audio(source_audio_file: str):
    audio, sample_rate = librosa.load(source_audio_file, sr=16000)
    if len(audio.shape) == 1:
        audio = audio[np.newaxis, :]

    logging.info('Extracting audio features ...')
    audio_features = ddsp.training.metrics.compute_audio_features(
        audio, frame_rate=250)
    logging.info('Extracting audio features is done.')
    return audio, audio_features


class ModelNotLoadedException(Exception):
    pass


def load_and_adjust_gin_configuration(model_dir: str,
                                      current_file: str = None,
                                      time_steps: int = None,
                                      audio=None):
    # Ensure dimensions and sampling rates are equal
    logging.info('loading and adjusting gin configuration ...')

    if time_steps is None and audio is None:
        message: str = 'Skipping ' + current_file + ' because neither time_steps nor audio were provided!'
        logging.exception(message)
        raise ModelNotLoadedException(message)

    # Parse gin config,
    try:
        gin_file = os.path.join(model_dir, 'operative_config-0.gin')
        with gin.unlock_config():
            gin.parse_config_file(gin_file, skip_unknown=True)
    except IOError as io_error_msg:
        logging.exception(io_error_msg)
        message: str = 'Skipping ' + current_file + ' because model was not found!'
        logging.exception(message)
        raise ModelNotLoadedException(message)

    time_steps_train = gin.query_parameter('F0LoudnessPreprocessor.time_steps')
    n_samples_train = gin.query_parameter('Harmonic.n_samples')
    hop_size = int(n_samples_train / time_steps_train)
    if time_steps is None:
        time_steps = int(audio.shape[1] / hop_size)
    n_samples = time_steps * hop_size
    # adjust gin config to audio file
    gin_params = [
        'Harmonic.n_samples = {}'.format(n_samples),
        'FilteredNoise.n_samples = {}'.format(n_samples),
        'F0LoudnessPreprocessor.time_steps = {}'.format(time_steps),
        'oscillator_bank.use_angular_cumsum = True',  # Avoids cumsum accumulation errors.
    ]
    with gin.unlock_config():
        gin.parse_config(gin_params)

    logging.info('loading and adjusting gin configuration is done')
    return n_samples, time_steps


def load_model_with_checkpoint(checkpoint_name: str, model_dir: str):
    # load checkpoint
    # Assumes only one checkpoint in the folder, "ckpt-[iter]".
    if checkpoint_name is None:
        checkpoint_name = ''

    logging.info('loading model from checkpoint %s', checkpoint_name)
    start_time = time.time()

    ckpt = os.path.join(model_dir, checkpoint_name)
    # Set up the model just to predict audio given new conditioning
    model = ddsp.training.models.Autoencoder()
    # verbose=False : expect incomplete checkpoint, do not log missing variables
    model.restore(ckpt, verbose=False)

    logging.info('checkpoint %s loaded. Restoring model took %.1f seconds',
                 checkpoint_name,
                 (time.time() - start_time))

    return model


def save_audio_to_file(audio_gen,
                       target_audio_file: str,
                       sample_rate: int = ddsp.spectral_ops.CREPE_SAMPLE_RATE):
    # save audio to wave file
    array_of_floats = audio_gen
    # If batched, take first element.
    if len(array_of_floats.shape) == 2:
        array_of_floats = array_of_floats[0]
    normalizer = float(np.iinfo(np.int16).max)
    array_of_ints = np.array(
        np.asarray(array_of_floats) * normalizer, dtype=np.int16)
    wavfile.write(target_audio_file, sample_rate, array_of_ints)


def main(argv):
    logging.info('Running %s with flags', argv[0])
    my_flags = FLAGS.get_flags_for_module(argv[0])
    for flag in my_flags:
        if flag.value is not None:
            logging.info('--%s %s', flag.name, str(flag.value))

    mode: str = FLAGS.MODE
    overwrite: bool = FLAGS.OVERWRITE
    model_dir: str = FLAGS.MODEL_DIR
    source_audio_file: str = FLAGS.SOURCE_AUDIO_FILE
    source_csv_file: str = FLAGS.SOURCE_CSV_FILE
    target_audio_file: str = FLAGS.TARGET_AUDIO_FILE
    target_csv_file: str = FLAGS.TARGET_CSV_FILE
    samplerate: int = FLAGS.SAMPLERATE
    checkpoint_name: str = FLAGS.CHECKPOINT_NAME

    mode = mode.lower()
    if mode == 'resynthesize':
        resynthesize_audio(model_dir,
                           source_audio_file,
                           target_audio_file,
                           samplerate,
                           overwrite,
                           checkpoint_name)
    elif mode == 'encode':
        encode_audio(model_dir,
                     source_audio_file,
                     target_csv_file,
                     overwrite,
                     checkpoint_name)
    elif mode == 'decode':
        decode_audio(model_dir=model_dir,
                     source_csv_file=source_csv_file,
                     target_audio_file=target_audio_file,
                     samplerate=samplerate,
                     overwrite=overwrite,
                     checkpoint_name=checkpoint_name)
    else:
        logging.error('--MODE %s has no match! Did not do anything.', mode)
        exit(0)


if __name__ == '__main__':
    app.run(main)
