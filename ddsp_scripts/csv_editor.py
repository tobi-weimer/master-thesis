import math

import numpy as np
from absl import flags, app, logging
from math import sin
from tensorflow.python.ops.numpy_ops import np_config


from ddsp_scripts import csv_util


FLAGS = flags.FLAGS
source_csv_file_help: str = 'the source CSV file'
flags.DEFINE_string('source_csv_file', None, source_csv_file_help)
target_csv_file_help: str = 'the target CSV file'
flags.DEFINE_string('target_csv_file', None, target_csv_file_help)
edit_function_help: str = 'the function to use to edit the target CSV file'
flags.DEFINE_string('edit_function', None, edit_function_help)
parameter_name_help: str = 'the name of the parameter to edit'
flags.DEFINE_string('parameter_name', None, parameter_name_help)

value_help: str = 'the value to set'
flags.DEFINE_float('value', None, value_help)
jump_length_help: str = 'how many step shall be set to high/low before switching to the other back and forth?'
flags.DEFINE_integer('jump_length', None, jump_length_help)
low_value_help: str = 'the lower (limit) value to set'
flags.DEFINE_float('low_value', None, low_value_help)
high_value_help: str = 'the upper (limit) value to set'
flags.DEFINE_integer('high_value', None, high_value_help)
wave_length_help: str = 'after how many times of Pi shall the wave repeat?'
flags.DEFINE_integer('wave_length', None, wave_length_help)

edit_both_f0_help: str = 'edit both f0 parameters to keep their relation intact?'
flags.DEFINE_boolean('edit_both_f0', False, edit_both_f0_help)

max_values = {
    'f0_hz': 10_000,
    'f0_scaled': 1.0,
    'ld_scaled': 1.0,
    'z': 3.0
}
min_values = {
    'f0_hz': 0,
    'f0_scaled': 0.0,
    'ld_scaled': 0.0,
    'z': -3.0
}

parameters = [
    'f0_hz'
    'f0_scaled',
    'ld_scaled',
    'z'
]


function_names = [
    'fixed_value',
    'max_value',
    'min_value',
    'zero_value',
    'null_value',
    'jump_min_max',
    'jump_low_high',
    'sin_min_max',
    'sin_low_high',
    'invert_value'
]


def set_to_fixed_value(audio_features: dict,
                       parameter_name: str,
                       value,
                       edit_both_f0: bool = False):
    parameter_value = audio_features[parameter_name]
    shape = parameter_value.shape

    parameter_value = np.full(shape, value, dtype=float)

    audio_features[parameter_name] = parameter_value

    if edit_both_f0 and parameter_name in ('f0_hz','f0_scaled'):
        if parameter_name == 'f0_hz':
            other_value = convert_f0_hz_to_f0_scaled(value)
            audio_features = set_to_fixed_value(audio_features, 'f0_scaled', other_value, False)
        else:
            other_value = convert_f0_scaled_to_f0_hz(value)
            audio_features = set_to_fixed_value(audio_features, 'f0_hz', other_value, False)

    return audio_features


def set_max_value(audio_features: dict,
                  parameter_name: str,
                  edit_both_f0: bool = False):

    max_value = max_values[parameter_name]
    audio_features = set_to_fixed_value(audio_features, parameter_name, max_value, edit_both_f0)
    return audio_features


def set_min_value(audio_features: dict,
                  parameter_name: str,
                  edit_both_f0: bool = False):
    min_value = min_values[parameter_name]
    audio_features = set_to_fixed_value(audio_features, parameter_name, min_value, edit_both_f0)
    return audio_features


def set_zero_value(audio_features: dict,
                   parameter_name: str,
                   edit_both_f0: bool = False):
    audio_features = set_to_fixed_value(audio_features, parameter_name, 0, edit_both_f0)
    return audio_features


def set_null_value(audio_features: dict,
                   parameter_name: str,
                   edit_both_f0: bool = False):
    audio_features = set_to_fixed_value(audio_features, parameter_name, None, edit_both_f0)
    return audio_features


def jump_min_max(audio_features: dict,
                 parameter_name: str,
                 jump_length,
                 edit_both_f0: bool = False):
    audio_features = jump_two_values(audio_features,
                                     parameter_name,
                                     jump_length,
                                     min_values[parameter_name],
                                     max_values[parameter_name],
                                     edit_both_f0)
    return audio_features


def jump_two_values(audio_features: dict,
                    parameter_name: str,
                    jump_length,
                    low,
                    high,
                    edit_both_f0: bool = False):
    parameter_value = audio_features[parameter_name]
    shape = parameter_value.shape
    total_values: int = 1
    for dim in shape.dims:
        total_values *= dim.value

    new_parameter_value = np.empty(total_values, dtype=float)
    for step in range(total_values):
        if (int(step / jump_length) % 2) == 1:
            new_parameter_value[step] = low
        else:
            new_parameter_value[step] = high

    new_parameter_value = new_parameter_value.reshape(shape)
    audio_features[parameter_name] = new_parameter_value

    if edit_both_f0 and parameter_name in ('f0_hz','f0_scaled'):
        if parameter_name == 'f0_hz':
            other_low = convert_f0_hz_to_f0_scaled(low)
            other_high = convert_f0_hz_to_f0_scaled(high)
            audio_features = jump_two_values(audio_features, 'f0_scaled', jump_length, other_low, other_high, False)
        else:
            other_low = convert_f0_scaled_to_f0_hz(low)
            other_high = convert_f0_scaled_to_f0_hz(high)
            audio_features = jump_two_values(audio_features, 'f0_hz', jump_length, other_low, other_high, False)

    return audio_features


def sinusoid_min_max_values(audio_features: dict,
                            parameter_name: str,
                            wave_length,
                            edit_both_f0: bool = False):
    audio_features = sinusoid_wave_two_values(audio_features,
                                              parameter_name,
                                              wave_length,
                                              min_values[parameter_name],
                                              max_values[parameter_name],
                                              edit_both_f0)
    return audio_features


def sinusoid_wave_two_values(audio_features: dict,
                             parameter_name: str,
                             wave_length,
                             low,
                             high,
                             edit_both_f0: bool = False):
    """
    Parameter wave_length: after how many steps shall the wave repeat?
    """
    parameter_value = audio_features[parameter_name]
    shape = parameter_value.shape
    total_values: int = 1
    for dim in shape.dims:
        total_values *= dim.value

    half_difference = (high - low) / 2

    new_parameter_value = np.empty(total_values, dtype=float)
    for step in range(total_values):
        value = half_difference * sin(2 / wave_length * math.pi * step) + low + half_difference
        new_parameter_value[step] = value

    new_parameter_value = new_parameter_value.reshape(shape)
    audio_features[parameter_name] = new_parameter_value

    if edit_both_f0 and parameter_name in ('f0_hz', 'f0_scaled'):
        if parameter_name == 'f0_hz':
            other_low = convert_f0_hz_to_f0_scaled(low)
            other_high = convert_f0_hz_to_f0_scaled(high)
            audio_features = sinusoid_wave_two_values(audio_features, 'f0_scaled', wave_length, other_low, other_high, False)
        else:
            other_low = convert_f0_scaled_to_f0_hz(low)
            other_high = convert_f0_scaled_to_f0_hz(high)
            audio_features = sinusoid_wave_two_values(audio_features, 'f0_hz', wave_length, other_low, other_high, False)

    return audio_features


def invert_value(audio_features: dict,
                 parameter_name: str,
                 edit_both_f0: bool = False):
    max_value = max_values[parameter_name]
    min_value = min_values[parameter_name]
    distance = max_value - min_value

    parameter_value = audio_features[parameter_name]
    shape = parameter_value.shape
    total_values: int = 1
    for dim in shape.dims:
        total_values *= dim.value

    parameter_value = parameter_value.reshape(total_values)

    new_parameter_value = np.empty(total_values, dtype=float)
    for step in range(total_values):
        value = parameter_value[step]
        value = min_value + distance - value
        new_parameter_value[step] = value

    new_parameter_value = new_parameter_value.reshape(shape)
    audio_features[parameter_name] = new_parameter_value

    if edit_both_f0 and parameter_name in ('f0_hz','f0_scaled'):
        if parameter_name == 'f0_hz':
            audio_features = invert_value(audio_features, 'f0_scaled', False)
        else:
            audio_features = invert_value(audio_features, 'f0_hz', False)

    return audio_features


def convert_f0_hz_to_f0_scaled(f0_hz: float) -> float:
    if f0_hz is None:
        return None
    if f0_hz == 0.0:
        return 0.0
    midi_code: float = 12.0 * math.log2( f0_hz / 440.0 ) + 69.0
    f0_scaled: float = max( midi_code, 0) / 127.0
    return f0_scaled


def convert_f0_scaled_to_f0_hz(f0_scaled: float) -> float:
    if f0_scaled is None:
        return None
    if f0_scaled == 0.0:
        return 0.0
    midi_code: float = f0_scaled * 127.0
    f0_hz: float = (( midi_code - 69.0 ) / 12.0 ) ** 2 * 440.0
    return f0_hz


def main(argv):
    logging.info('Running %s with flags', argv[0])
    my_flags = FLAGS.get_flags_for_module(argv[0])
    for flag in my_flags:
        if flag.value is not None:
            logging.info('--%s %s', flag.name, str(flag.value))

    source_csv_file = FLAGS.source_csv_file
    if source_csv_file is None:
        message: str = 'Missing parameter "--source_csv_file <VALUE>".'
        logging.error(message)
        raise TypeError(message)
    target_csv_file = FLAGS.target_csv_file
    if target_csv_file is None:
        message: str = 'Missing parameter "--target_csv_file <VALUE>".'
        logging.error(message)
        raise TypeError(message)

    parameter_name: str = FLAGS.parameter_name
    if parameter_name is None:
        message: str = 'Missing parameter "--parameter_name <VALUE>".'
        logging.error(message)
        raise TypeError(message)

    edit_function_name: str = FLAGS.edit_function
    if edit_function_name is None:
        message: str = 'Missing parameter "--edit_function <VALUE>".'
        logging.error(message)
        raise TypeError(message)
    if edit_function_name not in function_names:
        message: str = ('Parameter "--edit_function <VALUE>" must have a value of %s.'
                        % ', '.join(function_names))
        logging.error(message)
        raise ValueError(message)

    edit_both_f0: bool = FLAGS.edit_both_f0

    np_config.enable_numpy_behavior()

    audio_features, time_steps = csv_util.load_csv_to_features(source_csv_file)

    audio_features = select_and_apply_edit_function(audio_features, edit_function_name, parameter_name, edit_both_f0)

    csv_util.save_features_to_csv(audio_features, target_csv_file)
    logging.info('Editing and saving of file %s successful.', target_csv_file)


def select_and_apply_edit_function(audio_features, edit_function_name, parameter_name, edit_both_f0: False):
    if edit_function_name == 'fixed_value':
        value = FLAGS.value
        if value is None:
            message: str = ('Missing parameter "--value <VALUE>" for edit function %s!'
                            % edit_function_name)
            logging.error(message)
            raise TypeError(message)

        audio_features = set_to_fixed_value(audio_features, parameter_name, value, edit_both_f0)
    elif edit_function_name == 'max_value':
        audio_features = set_max_value(audio_features, parameter_name, edit_both_f0)

    elif edit_function_name == 'min_value':
        audio_features = set_min_value(audio_features, parameter_name, edit_both_f0)

    elif edit_function_name == 'zero_value':
        audio_features = set_zero_value(audio_features, parameter_name, edit_both_f0)

    elif edit_function_name == 'null_value':
        audio_features = set_null_value(audio_features, parameter_name, edit_both_f0)

    elif edit_function_name == 'jump_min_max':
        jump_length = FLAGS.jump_length
        if jump_length is None:
            message: str = ('Missing parameter "--jump_length <VALUE>" for edit function %s!'
                            % edit_function_name)
            logging.error(message)
            raise TypeError(message)
        audio_features = jump_min_max(audio_features, parameter_name, jump_length, edit_both_f0)

    elif edit_function_name == 'jump_low_high':
        jump_length = FLAGS.jump_length
        if jump_length is None:
            message: str = ('Missing parameter "--jump_length <VALUE>" for edit function %s!'
                            % edit_function_name)
            logging.error(message)
            raise TypeError(message)

        low_value = FLAGS.low_value
        if low_value is None:
            message: str = ('Missing parameter "--low_value <VALUE>" for edit function %s!'
                            % edit_function_name)
            logging.error(message)
            raise TypeError(message)

        high_value = FLAGS.high_value
        if high_value is None:
            message: str = ('Missing parameter "--high_value <VALUE>" for edit function %s!'
                            % edit_function_name)
            logging.error(message)
            raise TypeError(message)

        audio_features = jump_two_values(audio_features, parameter_name,
                                         jump_length, low_value, high_value,
                                         edit_both_f0)

    elif edit_function_name == 'sin_min_max':
        wave_length = FLAGS.wave_length
        if wave_length is None:
            message: str = ('Missing parameter "--wave_length <VALUE>" for edit function %s!'
                            % edit_function_name)
            logging.error(message)
            raise TypeError(message)

        audio_features = sinusoid_min_max_values(audio_features, parameter_name,
                                                 wave_length, edit_both_f0)

    elif edit_function_name == 'sin_low_high':
        wave_length = FLAGS.wave_length
        if wave_length is None:
            message: str = ('Missing parameter "--wave_length <VALUE>" for edit function %s!'
                            % edit_function_name)
            logging.error(message)
            raise TypeError(message)

        low_value = float(FLAGS.low_value)
        if low_value is None:
            message: str = ('Missing parameter "--low_value <VALUE>" for edit function %s!'
                            % edit_function_name)
            logging.error(message)
            raise TypeError(message)

        high_value = float(FLAGS.high_value)
        if high_value is None:
            message: str = ('Missing parameter "--high_value <VALUE>" for edit function %s!'
                            % edit_function_name)
            logging.error(message)
            raise TypeError(message)

        audio_features = sinusoid_wave_two_values(audio_features, parameter_name,
                                                  wave_length, low_value, high_value,
                                                  edit_both_f0)

    elif edit_function_name == 'invert_value':
        audio_features = invert_value(audio_features, parameter_name, edit_both_f0)

    else:
        message: str = ('Parameter "--edit_function <VALUE>" must have a value of %s.'
                        % ', '.join(function_names))
        logging.error(message)
        raise ValueError(message)
    return audio_features


if __name__ == "__main__":
    app.run(main)
