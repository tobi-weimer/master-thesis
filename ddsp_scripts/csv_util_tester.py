import csv_util
import numpy as np
import tensorflow as tf
import unittest

from tensorflow.python.ops.numpy_ops import np_config


class MyTestCase(unittest.TestCase):
    def test_csv_util(self):
        np_config.enable_numpy_behavior()
        csv_file: str = "/tmp/csv_util_tester"

        end = 5
        audio_features = dict({
            "f0_hz": tf.random.uniform(shape=(1, end, 1), dtype=np.float32),
            "f0_scaled": tf.random.uniform(shape=(1, end, 1), dtype=np.float32),
            "loudness_db": tf.random.uniform(shape=(1, end, 1), dtype=np.float32),
            "ld_scaled": tf.random.uniform(shape=(1, end, 1), dtype=np.float32),
            "z": tf.random.uniform(shape=(1, end, 16), dtype=np.float32)
        })

        audio_features_working = dict({})

        for key in audio_features.keys():
            audio_features_working[key] = tf.identity(audio_features[key])

        csv_util.save_features_to_csv(audio_features_working, csv_file)

        loaded_csv_file, _ = csv_util.load_csv_to_features(csv_file)

        self.assertEqual(audio_features.keys(), loaded_csv_file.keys())  # add assertion here
        for key in audio_features.keys():
            audio_features_key = audio_features[key]
            loaded_csv_key = loaded_csv_file[key]

            try:
                equal = tf.reduce_all(tf.equal(audio_features_key, loaded_csv_key))
            except Exception as ex:
                print('failed to compare audio_features["' + key + '"]')
                print('audio_features["' + key + '"]')
                print(audio_features_key)

                print('loaded_csv_file["' + key + '"]')
                print(loaded_csv_key)
                raise ex

            equal = tf.keras.backend.eval(equal)

            if not equal:
                print('audio_features["' + key + '"]')
                print(audio_features_key)

                print('loaded_csv_file["' + key + '"]')
                print(loaded_csv_key)

            self.assertEqual(equal, True)


if __name__ == '__main__':
    unittest.main()
