import librosa
import sys
import traceback
from absl import flags, app
from pathlib import Path
from os.path import exists

from site import addsitedir
addsitedir(str(Path(__file__).parent.parent))

from audio_utils import save_specplot

"""
example call:
nohup python generate_spectograms.py \
--BASE_DIR=/home/tobi/MA-Thesis/audios_generated/AimeeNorwich_Child \
--AUDIO_FILE_PATTERN=**/*.wav --FILE_STEM_EXTENSION=_specplot \
> /home/tweimer/logs/resynthesizeAudio.`date +"%Y-%m-%d_%H-%M"`.info.log \
2>/home/tweimer/logs/resynthesizeAudio.`date +"%Y-%m-%d_%H-%M"`.error.log &
"""

FLAGS = flags.FLAGS
flags.DEFINE_string(
    'AUDIO_FILE_PATTERN', None,
    'A pattern to filter which the files found in BASE_DIR.')
flags.DEFINE_string(
    'BASE_DIR', None,
    'The directory where audio files are searched to create spectograms - including all subdirectories.')
flags.DEFINE_string(
    'FILE_STEM_EXTENSION', None,
    'Optional extension of the file name stem.')


def process_audio_file(audio_file, file_stem_extension):
    """
    :param audio_file:
    :param file_stem_extension:
    """

    audio, sample_rate = librosa.load(str(audio_file), sr=16000)

    if len(audio.shape) > 1:
        raise ValueError("variable audio must be 1D array! Unbatch it if necessary!")

    specplot_file_name = str(audio_file.parent) + '/' + audio_file.stem
    if file_stem_extension is not None:
        specplot_file_name = specplot_file_name + file_stem_extension

    if not exists(specplot_file_name + '.png'):
        save_specplot(specplot_file_name, audio)

    return


def main(argv):
    audio_file_pattern = FLAGS.AUDIO_FILE_PATTERN
    base_dir = FLAGS.BASE_DIR
    file_stem_extension = FLAGS.FILE_STEM_EXTENSION

    print(argv[0] + ' running with flags:',
          '\n--AUDIO_FILE_PATTERN', audio_file_pattern,
          '\n--BASE_DIR', base_dir,
          '\n--FILE_STEM_EXTENSION', file_stem_extension, '\n',
          sep=None, flush=True)

    if audio_file_pattern is None:
        print('parameter AUDIO_FILE_PATTERN is required!', file=sys.stderr)

    if base_dir is None:
        print('parameter BASE_DIR is required!', file=sys.stderr)

    base_dir_path = Path(base_dir)
    for audio_file in base_dir_path.glob(audio_file_pattern):
        print('processing file ' + str(audio_file) + '...', end='')
        try:
            process_audio_file(audio_file, file_stem_extension)
            print(' finished.')
        except:
            print(' failed!')
            print('processing file ' + str(audio_file) + ' failed!', file=sys.stderr)
            traceback.print_exc()


if __name__ == '__main__':
    app.run(main)
