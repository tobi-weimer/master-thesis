import subprocess
from builtins import print
from pathlib import Path

from absl import flags, app

FLAGS = flags.FLAGS
flags.DEFINE_string(
    'tfrecords_root_dir', None,
    'The directory containing the tfrecords files to process.')
flags.DEFINE_string(
    'models_root_dir', None,
    'The directory to which the resulting model files should be written.')


# noinspection PyUnusedLocal
def main(argv):
    tfrecords_root_dir = Path(FLAGS.tfrecords_root_dir)

    for tfrecord_file in tfrecords_root_dir.glob("**/*STEM*half1.tfrecord-00000-of-00001"):
        print(tfrecord_file)

        print(tfrecord_file.stem)
        print(tfrecord_file.parent)

        save_dir = "--save_dir=" + FLAGS.models_root_dir + "/" + tfrecord_file.stem + "/"
        file_pattern = "--gin_param=TFRecordProvider.file_pattern=\"" + str(tfrecord_file) + "\""
        command = [
            "ddsp_run",
            "--mode=eval",
            "--alsologtostderr",
            "--run_once",
            save_dir,
            "--gin_file=eval/basic_f0_ld.gin",
            "--gin_file=models/ae.gin",
            "--gin_file=datasets/tfrecord.gin",
            file_pattern
        ]
        print(f"Executing command: {' '.join(command)}")
        subprocess.call(command)
        pass
    print(f"Finished processing all tfrecord files")


if __name__ == '__main__':
    flags.mark_flags_as_required(['tfrecords_root_dir', 'models_root_dir'])
    app.run(main)
